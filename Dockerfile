#FROM docker-prod.registry.kroger.com/library/java-openjre:8-latest
FROM openjdk:8-jre-alpine

EXPOSE 10072

# add the application
COPY build/libs/*.jar /app.jar

ENV JAVA_OPTS="-Xms256m -Xmx512m"

CMD java ${JAVA_OPTS} -jar /app.jar
