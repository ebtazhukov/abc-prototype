# abc-prototype

To start a cassandra database in a docker container, run:

    cd monolith-main/src/main/docker/cassandra/
        
    docker build -t mycassandra .
        
    docker run -d -p 9042:9042 mycassandra
            

##keycloak

to run pre-configured keycloak:

    cd monolith-main/src/main/docker/keycloak/
    
    docker build -t mykeycloak .
    
    docker run -d -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e DB_VENDOR=H2 mykeycloak
        
        
##Build Services (All except Album)
To build the Java services

    ./gradlew clean build
    ./gradlew bootrun
    
    Services run on 10072
    
     in intellij spring run monlith-main/main/jave/com.kroger.abc.prototype/ABCPrototypeApplication
     
     ABCPrototypeApplication
     
##Run Album service (Nodejs)
    will run with npm but...
    
    run from yarn (if not installed run --  npm install -g yarn)
    
    from the abc-prototype/app-album directory
    run
    yarn 
    then
    yarn start
    
##Run the client port 10080

    Again using yarn see above to install
    
    from abc-prototype/ui  Directory
    
    run
    yarn
    
    Then
    yarn start