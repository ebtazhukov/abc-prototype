package com.kroger.abc.prototype.app.account.rest;

import com.kroger.abc.prototype.app.account.service.AccountAppService;
import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.account.AccountDTO;
import com.kroger.abc.prototype.common.rest.util.SecurityContextHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Account.
 */
@RestController
@RequestMapping("/app")
@Api(
        value = "app-account",
        tags = "app-account",
        description = "Application service responding for account information."
)
@Profile({"default", "app-accounts"})
public class AccountAppController {

    private static final String ENTITY_NAME = "accounts";
    private final Logger log = LoggerFactory.getLogger(AccountAppController.class);
    private final AccountAppService accountAppService;

    public AccountAppController(AccountAppService accountAppService) {
        this.accountAppService = accountAppService;
    }

    /**
     * GET  /accounts : get account for currently logged in user
     *
     * @return the ResponseEntity with status 200 (OK) and with body the accountDTO
     */
    @GetMapping("/accounts")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get account",
            notes = "This method is for obtaining account for currently logged in user"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = AccountDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<AccountDTO>> getAccount() {

        log.debug("REST request to get all the Account: {}", SecurityContextHelper.getUserName());
        return ResponseEntity.ok(ApiResponseBodyDeprecated.of(accountAppService.findOne()));
    }

    /**
     * DELETE  /accounts : Deletes an existing account for currently logged in user.
     *
     * @return the ResponseEntity with status 204 (NO_CONTENT), or with status 404 (Not Found)
     */
    @DeleteMapping("/accounts")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(
            value = "Delete the account",
            notes = "This method is for deleting account"
    )
    @ApiResponses({
            @ApiResponse(code = 204, message = "NO_CONTENT")
    })
    public ResponseEntity<Void> deleteAccount() {
        log.debug("REST request to delete Account : {}", SecurityContextHelper.getUserName());
        accountAppService.delete();
        return ResponseEntity.noContent().build();
    }
}
