package com.kroger.abc.prototype.app.account.service;

import com.kroger.abc.prototype.common.rest.dto.account.AccountDTO;

/**
 * Service Interface for managing Accounts.
 */
public interface AccountAppService {

    /**
     * Get account for currently logged in user.
     *
     * @return the entity
     */
    AccountDTO findOne();

    /**
     * Delete account for currently logged in user.
     */
    void delete();
}
