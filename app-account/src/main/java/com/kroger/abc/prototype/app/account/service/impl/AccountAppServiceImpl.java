package com.kroger.abc.prototype.app.account.service.impl;

import com.kroger.abc.prototype.app.account.service.AccountAppService;
import com.kroger.abc.prototype.common.client.account.core.AccountClient;
import com.kroger.abc.prototype.common.rest.dto.account.AccountDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing Account.
 */
@Service
public class AccountAppServiceImpl implements AccountAppService {

    private final Logger log = LoggerFactory.getLogger(AccountAppServiceImpl.class);

    private final AccountClient accountClient;

    public AccountAppServiceImpl(AccountClient accountClient) {
        this.accountClient = accountClient;
    }

    /**
     * Get account for currently logged in user.
     *
     * @return the entity
     */
    @Override
    public AccountDTO findOne() {
        log.debug("Request to get Account by username");
        return accountClient.findOne();
    }

    /**
     * Delete account for currently logged in user.
     */
    @Override
    public void delete() {
        log.debug("Request to delete Account by username");
        accountClient.delete();
    }
}
