const Koa = require('koa')
const cors = require('@koa/cors')
const router = require('koa-router')()
const logger = require('koa-logger')
const koaBody = require('koa-body')()
const axios = require('axios')
const https = require('https')

const { attachToCTX, checkPrecondition, respOnError } = require('./koa-utils')

const constructURL = ((useLocalhost = true) => {
  return useLocalhost !== 'false'
    ? (tier, domain) => `http://localhost:10072/${tier}/${domain}`
    : (tier, domain) => `https://krandorify-${tier}-${domain}.cfcdcinternaltest.kroger.com/${tier}/${domain}`
})(process.env.USE_LOCALHOST)

const songsURL = process.env.API_BUSINESS_SONGS || constructURL('business', 'songs')
const albumsURL = process.env.API_BUSINESS_ALBUMS || constructURL('business', 'albums')
const artistsURL = process.env.API_CORE_ARTISTS || constructURL('core', 'artists')

axios.defaults.headers.post['Content-Type'] = 'application/json'
axios.defaults.headers.put['Content-Type'] = 'application/json'
axios.defaults.headers.common['Accept'] = 'application/json'

const app = new Koa()

const headerWhiteList = ['authorization']

const includeRE = /(\w+)\[((\w+_\w+,?)+),?\]/gm

function parseProjections(projections) {
  return projections.split(',').reduce(
    (acc, projection) => {
      const parts = projection.split('_')
      acc[parts[0]] = parts[1]

      return acc
    },
    { rawProjection: projections }
  )
}

function parseIncludes(str) {
  let m
  const parsed = {}

  while ((m = includeRE.exec(str)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === includeRE.lastIndex) {
      includeRE.lastIndex++
    }

    parsed[m[1]] = parseProjections(m[2])
  }

  return parsed
}

const getProjection = (ctx, domain) => (ctx.state.include[domain] && ctx.state.include[domain].rawProjection) || ''

const setupAxiosClient = headers =>
  axios.create({
    headers: {
      common: headerWhiteList.reduce((acc, hdrName) => {
        if (headers[hdrName]) {
          acc[hdrName] = headers[hdrName]
        }

        return acc
      }, {})
    },
    httpsAgent: new https.Agent({
      rejectUnauthorized: false
    })
  })

const attachinclude = respOnError(attachToCTX(parseIncludes, 'query.include', 'include'), 500)
const attachAxios = respOnError(attachToCTX(setupAxiosClient, 'headers', 'axios'), 500)

const ensureJSONBody = checkPrecondition(ctx => ctx.request.is('json'), 415)
const ensureAcceptsJSON = checkPrecondition(ctx => ctx.request.accepts('json'), 406)

const getSongs = (ctx, songIds) =>
  ctx.state.axios
    .get(songsURL, {
      params: { 'filter.song.id': songIds.join(',') },
      projection: getProjection(ctx, 'songs')
    })
    .then(resp => resp.data.data)

const getArtists = (ctx, artistIds) =>
  ctx.state.axios
    .get(artistsURL, {
      params: { 'filter.id': artistIds.join(',') },
      projection: getProjection(ctx, 'artists')
    })
    .then(resp => resp.data.data)

function concatSets(set, ...iterables) {
  for (const iterable of iterables) {
    for (const item of iterable) {
      set.add(item)
    }
  }

  return set
}

function addinclude(ctx) {
  return async albums => {
    const include = ctx.state.include || {}
    const albumsReturned = Array.isArray(albums) ? albums : [albums]
    const body = {
      data: albums,
      included: {},
      meta: {
        projections: ctx.state.include
      }
    }

    if (include.songs) {
      const albumSongs = concatSets(
        new Set(),
        albumsReturned.map(album => album.relationships.songs.map(song => song.id))
      )
      body.included.songs = await getSongs(ctx, [...albumSongs.values()] || [])
    }

    if (include.artists) {
      const artistIds = concatSets(
        new Set(),
        body.included.songs.map(song => song.relationships.artist.id),
        albumsReturned.map(album => album.relationships.artist.id)
      )

      body.included.artists = await getArtists(ctx, [...artistIds.values()])
    }

    return body
  }
}

async function getAlbum(ctx, next) {
  await ctx.state.axios
    .get(`${albumsURL}/${ctx.params.id}`, {
      params: { projection: getProjection(ctx, 'albums') }
    })
    .then(resp => resp.data.data)
    .then(addinclude(ctx))
    .then(body => {
      ctx.body = body
    })
  await next()
}

async function getAllAlbums(ctx, next) {
  await ctx.state.axios
    .get(albumsURL, {
      params: { projection: getProjection(ctx, 'albums') }
    })
    .then(resp => resp.data.data)
    .then(addinclude(ctx))
    .then(body => {
      ctx.body = body
    })
  await next()
}

async function createAlbum(ctx, next) {
  await ctx.state.axios
    .post(albumsURL, {
      data: ctx.request.body
    })
    .then(resp => resp.data.data)
    .then(addinclude(ctx))
    .then(body => {
      ctx.body = body
    })
  await next()
}

// async function createAlbum(ctx, next) {
//   ctx.body = ctx.request.body
//   await next()
// }

// async function updateAlbum(ctx, next) {
//   ctx.body = ctx.request.body
//   await next()
// }

/**
 * @swagger
 * tags:
 *    name: "app-album"
 *    description: "Application service responding for album domain."
 * definitions:
 *    AlbumResponseBody:
 *      type: "object"
 *      properties:
 *        data:
 *          type: "array"
 *          description: "The album requested"
 *          allowEmptyValue: false
 *          items:
 *            $ref: "#/definitions/AlbumDTO"
 *        included:
 *          $ref: "#/definitions/albumIncluded"
 *        meta:
 *          $ref: "#/definitions/MetadataDTO"
 *    albumIncluded:
 *      type: "object"
 *      properties:
 *        songs:
 *          type: "array"
 *          description: "Songs on the album if they were 'included'"
 *          allowEmptyValue: true
 *          items:
 *            $ref: "#/definitions/SongDTO"
 *        artists:
 *          type: "array"
 *          description: "artist for the album as well as for the songs (if they were 'included') if artists were 'included'"
 *          allowEmptyValue: true
 *          items:
 *            $ref: "#/definitions/ArtistDTO"
 *    AlbumRelationships:
 *      type: "object"
 *      properties:
 *        artist:
 *          description: "The artist who created this album."
 *          allowEmptyValue: false
 *          $ref: "#/definitions/ArtistReference"
 *        songs:
 *          type: "array"
 *          description: "The songs on this album, in order."
 *          allowEmptyValue: false
 *          items:
 *            $ref: "#/definitions/SongReference"
 *
 */

/**
 * @swagger
 * /app/albums/{id}:
 *    get:
 *      tags: ["app-album"]
 *      summary: "returns a single album"
 *      operationId: "getAlbumById"
 *      produces:
 *        - "application/json"
 *      parameters:
 *        - name: "id"
 *          in: "path"
 *          description: "Id of an album to retrieve"
 *          required: true
 *          type: "string"
 *          format: "uuid"
 *        - name: "include"
 *          in: "query"
 *          description: "**Domains to be included in response body along with their projections.** \n\nSupported domains are following: \n\n**albums** with available projections: details, notes \n\n**songs** with available projections: details, lyrics and digitalFiles \n\n**artists** with available projections: artists"
 *          required: false
 *          type: "string"
 *          format: "domainSpecifier"
 *      responses:
 *        200:
 *          description: "successful operation"
 *          schema:
 *            $ref: "#/definitions/AlbumResponseBody"
 *        406:
 *          description: "Requested unsupported content type. Only 'application/json' is currently supported."
 *        415:
 *          description: "Body is not in JSON format. Only JSON is currently supported."
 */
router.get('/app/albums/:id', attachinclude, getAlbum)

/**
 * @swagger
 * /app/albums:
 *    get:
 *      tags: ["app-album"]
 *      summary: "returns one or more albums"
 *      operationId: "getAlbums"
 *      produces:
 *        - "application/json"
 *      parameters:
 *        - name: "include"
 *          in: "query"
 *          description: "**Domains to be included in response body along with their projections.** \n\nSupported domains are following: \n\n**albums** with available projections: details, notes \n\n**songs** with available projections: details, lyrics and digitalFiles \n\n**artists** with available projections: artists"
 *          required: false
 *          type: "string"
 *          format: "domainSpecifier"
 *      responses:
 *        200:
 *          description: "successful operation"
 *          schema:
 *            $ref: "#/definitions/AlbumResponseBody"
 *        406:
 *          description: "Requested unsupported content type. Only 'application/json' is currently supported."
 *        415:
 *          description: "Body is not in JSON format. Only JSON is currently supported."
 */
router.get('/app/albums', attachinclude, getAllAlbums)

/**
 * @swagger
 * /app/albums:
 *    post:
 *      tags: ["app-album"]
 *      summary: "Adds album"
 *      operationId: "createAlbums"
 *      produces:
 *        - "application/json"
 *      parameters:
 *        - name: "include"
 *          in: "query"
 *          description: "**Domains to be included in response body along with their projections.** \n\nSupported domains are following: \n\n**albums** with available projections: details, notes \n\n**songs** with available projections: details, lyrics and digitalFiles \n\n**artists** with available projections: artists"
 *          required: false
 *          type: "string"
 *          format: "domainSpecifier"
 *      responses:
 *        200:
 *          description: "successful operation"
 *          schema:
 *            $ref: "#/definitions/AlbumResponseBody"
 *        406:
 *          description: "Requested unsupported content type. Only 'application/json' is currently supported."
 *        415:
 *          description: "Body is not in JSON format. Only JSON is currently supported."
 */
router.post('/app/albums', ensureJSONBody, koaBody, createAlbum)


// router.put('/albums', ensureJSONBody, koaBody, updateAlbum)

/**
 * @swagger
 * /health:
 *    get:
 *      tags: ["app-album"]
 *      summary: "returns OK"
 *      operationId: "checkHealth"
 *      produces:
 *        - "text"
 *      responses:
 *        200:
 *          description: "successful operation"
 */
router.get('/health', resp => (resp.body = 'OK'))

app.use(
  cors({
    origin: '*'
  })
)

app
  .use(logger())
  .use(ensureAcceptsJSON)
  .use(attachAxios)
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(9090)
