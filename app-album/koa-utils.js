function getByPath(obj, path) {
  return path.split('.').reduce((acc, attr) => acc && acc[attr], obj)
}

function attachToCTX(fn, paramPath, destination, error) {
  return async (ctx, next) => {
    ctx.state[destination] = fn(getByPath(ctx, paramPath))

    await next()
  }
}

function checkPrecondition(predicate, error) {
  return async (ctx, next) => {
    if (predicate(ctx)) {
      await next()
    } else {
      ctx.throw(error)
    }
  }
}

function respOnError(fn, error) {
  return async (ctx, next) => {
    try {
      await fn(ctx, next)
    } catch (e) {
      console.log('Got error: ', e, ' throwing with ', error)
      ctx.throw(error)
    }
  }
}

module.exports = {
  attachToCTX,
  checkPrecondition,
  respOnError
}
