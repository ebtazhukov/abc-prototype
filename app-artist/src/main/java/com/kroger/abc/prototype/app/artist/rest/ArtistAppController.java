package com.kroger.abc.prototype.app.artist.rest;

import com.kroger.abc.prototype.app.artist.service.ArtistAppService;
import com.kroger.abc.prototype.common.rest.dto.artist.app.ArtistListResponseBody;
import com.kroger.abc.prototype.common.rest.dto.artist.app.ArtistResponseBody;
import com.kroger.abc.prototype.common.rest.param.filter.ArtistFilters;
import com.kroger.abc.prototype.common.rest.param.inclusion.ArtistInclusions;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Artist.
 */
@RestController
@RequestMapping("/app")
@Api(
        value = "app-artist",
        tags = "app-artist",
        description = "Application service responding for artist domain."
)
@Profile({"default", "app-artists"})
public class ArtistAppController {

    private static final String ENTITY_NAME = "artists";
    private final Logger log = LoggerFactory.getLogger(ArtistAppController.class);
    private final ArtistAppService artistAppService;

    public ArtistAppController(ArtistAppService artistAppService) {
        this.artistAppService = artistAppService;
    }

    /**
     * GET  /artists/:id : get artist for specific id.
     *
     * @param id the id of the artist for which artistDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the artistDTO, or with status 404 (Not Found)
     */
    @GetMapping("/artists/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get artist for specific id",
            notes = "This method is for obtaining artist for specific id"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = ArtistResponseBody.class)
    })
    public ResponseEntity<ArtistResponseBody> getArtist(
            @ApiParam(value = "**Artist ID**.", required = true) @PathVariable String id,
            @Valid ArtistInclusions inclusions) {

        log.debug("REST request to get Artist : {}", id, QueryParametersHelper.toDebugString(inclusions));
        return ResponseEntity.ok(artistAppService.findOne(id, inclusions));
    }

    /**
     * GET  /artists : get all the artists
     *
     * @return the ResponseEntity with status 200 (OK) and with body the artistDTO list
     */
    @GetMapping("/artists")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get all the artists",
            notes = "This method is for obtaining all of the artists"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = ArtistListResponseBody.class)
    })
    public ResponseEntity<ArtistListResponseBody> getArtists(
            @Valid ArtistFilters artistFilters,
            @Valid ArtistInclusions inclusions) {

        log.debug("REST request to get all the Artists, query: {}",
                QueryParametersHelper.toDebugString(artistFilters, inclusions));
        return ResponseEntity.ok(artistAppService.findAll(artistFilters, inclusions));
    }
}
