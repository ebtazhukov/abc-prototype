package com.kroger.abc.prototype.app.artist.service;

import com.kroger.abc.prototype.common.rest.dto.artist.app.ArtistListResponseBody;
import com.kroger.abc.prototype.common.rest.dto.artist.app.ArtistResponseBody;
import com.kroger.abc.prototype.common.rest.param.filter.ArtistFilters;
import com.kroger.abc.prototype.common.rest.param.inclusion.Inclusions;

/**
 * Service Interface for managing Artists.
 */
public interface ArtistAppService {

    /**
     * Get all the artists.
     *
     * @return the list of entities
     */
    ArtistListResponseBody findAll(ArtistFilters artistFilters, Inclusions inclusions);

    /**
     * Get the "id" artist.
     *
     * @param id         the id of the entity
     * @param inclusions
     * @return the entity
     */
    ArtistResponseBody findOne(String id, Inclusions inclusions);
}
