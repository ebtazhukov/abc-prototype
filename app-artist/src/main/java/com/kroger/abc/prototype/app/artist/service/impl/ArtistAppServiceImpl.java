package com.kroger.abc.prototype.app.artist.service.impl;

import com.kroger.abc.prototype.app.artist.service.ArtistAppService;
import com.kroger.abc.prototype.common.client.artist.core.ArtistClient;
import com.kroger.abc.prototype.common.rest.dto.MetadataDTO;
import com.kroger.abc.prototype.common.rest.dto.artist.app.ArtistListResponseBody;
import com.kroger.abc.prototype.common.rest.dto.artist.app.ArtistResponseBody;
import com.kroger.abc.prototype.common.rest.param.filter.ArtistFilters;
import com.kroger.abc.prototype.common.rest.param.inclusion.Inclusions;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.util.projection.Inclusion;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.kroger.abc.prototype.common.rest.util.projection.Inclusion.INCLUSION_ARTIST_DOMAIN;

/**
 * Service Implementation for managing Artist.
 */
@Service
public class ArtistAppServiceImpl implements ArtistAppService {

    private final Logger log = LoggerFactory.getLogger(ArtistAppServiceImpl.class);

    private final ArtistClient artistClient;

    public ArtistAppServiceImpl(ArtistClient artistClient) {
        this.artistClient = artistClient;
    }

    /**
     * Get all the artists.
     *
     * @return the list of entities
     */
    public ArtistListResponseBody findAll(ArtistFilters artistFilters, Inclusions inclusions) {
        log.debug("Request to get all Artists");
        Inclusion inclusion = inclusions.extract();
        Projections artistProjections = inclusion.getByKey(INCLUSION_ARTIST_DOMAIN);

        return ArtistListResponseBody.of(
                artistClient.findAll(artistProjections, artistFilters), MetadataDTO.fromInclusion(inclusion));
    }

    /**
     * Get the "id" artist.
     *
     * @param id         the id of the entity
     * @param inclusions
     * @return the entity
     */
    public ArtistResponseBody findOne(String id, Inclusions inclusions) {
        log.debug("Request to get Artist by id");
        Inclusion inclusion = inclusions.extract();
        Projections artistProjections = inclusion.getByKey(INCLUSION_ARTIST_DOMAIN);

        return ArtistResponseBody.of(
                artistClient.findOne(UUID.fromString(id), artistProjections), MetadataDTO.fromInclusion(inclusion));
    }

}
