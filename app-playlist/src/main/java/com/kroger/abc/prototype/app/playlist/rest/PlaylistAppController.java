package com.kroger.abc.prototype.app.playlist.rest;

import com.kroger.abc.prototype.app.playlist.service.PlaylistAppService;
import com.kroger.abc.prototype.common.rest.ApiRequestBody;
import com.kroger.abc.prototype.common.rest.dto.playlist.app.PlaylistListResponseBody;
import com.kroger.abc.prototype.common.rest.dto.playlist.app.PlaylistResponseBody;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistUpdateDTO;
import com.kroger.abc.prototype.common.rest.param.filter.PlaylistFilters;
import com.kroger.abc.prototype.common.rest.param.inclusion.PlaylistInclusions;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Playlist.
 */
@RestController
@RequestMapping("/app")
@Api(
        value = "app-playlist",
        tags = "app-playlist",
        description = "Application service responding for playlist domain."
)
@Profile({"default", "app-playlists"})
public class PlaylistAppController {

    private static final String ENTITY_NAME = "playlists";
    private final Logger log = LoggerFactory.getLogger(PlaylistAppController.class);
    private final PlaylistAppService playlistAppService;

    public PlaylistAppController(PlaylistAppService playlistAppService) {
        this.playlistAppService = playlistAppService;
    }

    /**
     * GET  /playlists/:id : get playlist for specific id.
     *
     * @param id the id of the playlist for which playlistDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the playlistDTO, or with status 404 (Not Found)
     */
    @GetMapping("/playlists/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get playlist for specific id",
            notes = "This method is for obtaining playlist for specific id"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = PlaylistResponseBody.class)
    })
    public ResponseEntity<PlaylistResponseBody> getPlaylist(
            @ApiParam(value = "**Playlist ID**.", required = true) @PathVariable String id,
            @Valid PlaylistInclusions inclusions) {

        log.debug("REST request to get Playlist : {}, query: {}", id, QueryParametersHelper.toDebugString(inclusions));
        return ResponseEntity.ok(playlistAppService.findOne(id, inclusions));
    }

    /**
     * GET  /playlists : get all the playlists
     *
     * @return the ResponseEntity with status 200 (OK) and with body the playlistDTO list
     */
    @GetMapping("/playlists")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get all the playlists",
            notes = "This method is for obtaining all of the playlists"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = PlaylistListResponseBody.class)
    })
    public ResponseEntity<PlaylistListResponseBody> getPlaylists(
            @Valid PlaylistInclusions inclusions,
            @Valid PlaylistFilters playlistFilters) {

        log.debug("REST request to get all the Playlists, query: {}",
                QueryParametersHelper.toDebugString(inclusions, playlistFilters));
        return ResponseEntity.ok(playlistAppService.findAll(inclusions, playlistFilters));
    }

    /**
     * POST  /playlists : Creates a new playlist.
     *
     * @param request playlistDTO to create
     * @return the ResponseEntity with status 201 (CREATED) and with body the playlistDTO
     */
    @PostMapping("/playlists")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(
            value = "Create the playlist",
            notes = "This method is for creating playlist"
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "CREATED", response = PlaylistResponseBody.class)
    })
    public ResponseEntity<PlaylistResponseBody> createPlaylist(
            @Valid @RequestBody ApiRequestBody<PlaylistCreateDTO> request) {

        PlaylistCreateDTO playlistCreateDTO = request.getData();
        log.debug("REST request to create Playlist : {}", playlistCreateDTO);
        return ResponseEntity.created(URI.create("")).body(playlistAppService.create(playlistCreateDTO));
    }

    /**
     * PUT  /playlists : Updates an existing playlist.
     *
     * @param request playlistDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the playlistDTO, or with status 404 (Not Found)
     */
    @PutMapping("/playlists")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Update the playlist",
            notes = "This method is for updating playlist"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = PlaylistResponseBody.class)
    })
    public ResponseEntity<PlaylistResponseBody> updatePlaylist(
            @Valid @RequestBody ApiRequestBody<PlaylistUpdateDTO> request) {

        PlaylistUpdateDTO playlistUpdateDTO = request.getData();
        log.debug("REST request to update Playlist : {}", playlistUpdateDTO);
        return ResponseEntity.ok(playlistAppService.save(playlistUpdateDTO));
    }

    /**
     * DELETE  /playlists/:id : Deletes an existing playlist.
     *
     * @param id the id of the playlist to delete
     * @return the ResponseEntity with status 204 (NO_CONTENT), or with status 404 (Not Found)
     */
    @DeleteMapping("/playlists/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(
            value = "Delete the playlist",
            notes = "This method is for deleting playlist"
    )
    @ApiResponses({
            @ApiResponse(code = 204, message = "NO_CONTENT")
    })
    public ResponseEntity<Void> deletePlaylist(
            @ApiParam(value = "**Playlist ID**.", required = true) @PathVariable String id) {
        log.debug("REST request to delete Playlist : {}", id);
        playlistAppService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
