package com.kroger.abc.prototype.app.playlist.service;

import com.kroger.abc.prototype.common.rest.dto.playlist.app.PlaylistListResponseBody;
import com.kroger.abc.prototype.common.rest.dto.playlist.app.PlaylistResponseBody;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistUpdateDTO;
import com.kroger.abc.prototype.common.rest.param.filter.PlaylistFilters;
import com.kroger.abc.prototype.common.rest.param.inclusion.Inclusions;

/**
 * Service Interface for managing Playlists.
 */
public interface PlaylistAppService {

    /**
     * Create the playlist.
     *
     * @param playlistDTO the entity to save
     * @return saved entity
     */
    PlaylistResponseBody create(PlaylistCreateDTO playlistDTO);

    /**
     * Save an existing playlist.
     *
     * @param playlistDTO the entity to save
     * @return saved entity
     */
    PlaylistResponseBody save(PlaylistUpdateDTO playlistDTO);

    /**
     * Get all the playlists.
     *
     * @return the list of entities
     */
    PlaylistListResponseBody findAll(Inclusions inclusions, PlaylistFilters playlistFilters);

    /**
     * Get the "id" playlist.
     *
     * @param id         the id of the entity
     * @param inclusions
     * @return the entity
     */
    PlaylistResponseBody findOne(String id, Inclusions inclusions);

    /**
     * Delete the playlist.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
