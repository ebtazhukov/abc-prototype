package com.kroger.abc.prototype.app.playlist.service.impl;

import com.kroger.abc.prototype.app.playlist.service.PlaylistAppService;
import com.kroger.abc.prototype.common.client.artist.core.ArtistClient;
import com.kroger.abc.prototype.common.client.playlist.core.playlist.PlaylistClient;
import com.kroger.abc.prototype.common.client.song.business.song.SongClient;
import com.kroger.abc.prototype.common.rest.dto.MetadataDTO;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.app.PlaylistIncludeDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.app.PlaylistListResponseBody;
import com.kroger.abc.prototype.common.rest.dto.playlist.app.PlaylistResponseBody;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistRelationships;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistUpdateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongRelationships;
import com.kroger.abc.prototype.common.rest.param.filter.ArtistFilters;
import com.kroger.abc.prototype.common.rest.param.filter.PlaylistFilters;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.inclusion.Inclusions;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.util.projection.Inclusion;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.kroger.abc.prototype.common.rest.util.projection.Inclusion.INCLUSION_ARTIST_DOMAIN;
import static com.kroger.abc.prototype.common.rest.util.projection.Inclusion.INCLUSION_PLAYLIST_DOMAIN;
import static com.kroger.abc.prototype.common.rest.util.projection.Inclusion.INCLUSION_SONG_DOMAIN;

/**
 * Service Implementation for managing Playlist.
 */
@Service
public class PlaylistAppServiceImpl implements PlaylistAppService {

    private final Logger log = LoggerFactory.getLogger(PlaylistAppServiceImpl.class);

    private final PlaylistClient playlistClient;
    private final SongClient songClient;
    private final ArtistClient artistClient;

    public PlaylistAppServiceImpl(PlaylistClient playlistClient, SongClient songClient, ArtistClient artistClient) {
        this.playlistClient = playlistClient;
        this.songClient = songClient;
        this.artistClient = artistClient;
    }

    /**
     * Create the playlist.
     *
     * @param playlistDTO the entity to save
     * @return saved entity
     */
    public PlaylistResponseBody create(PlaylistCreateDTO playlistDTO) {
        log.debug("Request to create Playlist");
        return PlaylistResponseBody.of(playlistClient.create(playlistDTO), null, null);
    }

    /**
     * Save an existing playlist.
     *
     * @param playlistDTO the entity to save
     * @return saved entity
     */
    public PlaylistResponseBody save(PlaylistUpdateDTO playlistDTO) {
        log.debug("Request to update Playlist");
        return PlaylistResponseBody.of(playlistClient.save(playlistDTO), null, null);
    }

    /**
     * Get all the playlists.
     *
     * @return the list of entities
     */
    public PlaylistListResponseBody findAll(Inclusions inclusions, PlaylistFilters playlistFilters) {
        log.debug("Request to get all Playlists");
        Inclusion inclusion = inclusions.extract();
        Projections playlistProjections = inclusion.getByKey(INCLUSION_PLAYLIST_DOMAIN);

        List<PlaylistDTO> playlistDTOs = playlistClient.findAll(playlistProjections, playlistFilters);
        return PlaylistListResponseBody.of(playlistDTOs, getPlaylistInclusion(inclusion, playlistDTOs), MetadataDTO.fromInclusion(inclusion));
    }

    /**
     * Get the "id" playlist.
     *
     * @param id         the id of the entity
     * @param inclusions
     * @return the entity
     */
    public PlaylistResponseBody findOne(String id, Inclusions inclusions) {
        log.debug("Request to get Playlist by id");
        Inclusion inclusion = inclusions.extract();
        Projections playlistProjections = inclusion.getByKey(INCLUSION_PLAYLIST_DOMAIN);

        PlaylistDTO playlistDTO = playlistClient.findOne(UUID.fromString(id), playlistProjections);
        return PlaylistResponseBody.of(playlistDTO, getPlaylistInclusion(inclusion, playlistDTO), MetadataDTO.fromInclusion(inclusion));
    }

    /**
     * Delete the playlist.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Playlist by id");
        playlistClient.delete(UUID.fromString(id));
    }

    private PlaylistIncludeDTO getPlaylistInclusion(Inclusion inclusion, PlaylistDTO playlistDTO) {
        return getPlaylistInclusion(inclusion, Collections.singletonList(playlistDTO));
    }

    private PlaylistIncludeDTO getPlaylistInclusion(Inclusion inclusion, List<PlaylistDTO> playlistDTOs) {
        PlaylistIncludeDTO includeDTO = new PlaylistIncludeDTO();

        if (CollectionUtils.isNotEmpty(playlistDTOs) && inclusion.containsKey(INCLUSION_SONG_DOMAIN)) {

            Set<UUID> songIds = playlistDTOs.stream()
                    .flatMap(dto -> PlaylistRelationships.extractSongs(dto.getRelationships()).stream())
                    .collect(Collectors.toSet());

            SongFilters songIdFilter = new SongFilters();
            songIdFilter.setIds(new ArrayList<>(songIds));
            List<SongDTO> songDTOs = songClient.findAll(inclusion.getByKey(INCLUSION_SONG_DOMAIN), songIdFilter);
            includeDTO.setSongs(songDTOs);

            if (CollectionUtils.isNotEmpty(songDTOs) && inclusion.containsKey(INCLUSION_ARTIST_DOMAIN)) {
                Set<UUID> artistIds = songDTOs.stream()
                        .map(dto -> SongRelationships.extractArtist(dto.getRelationships()))
                        .collect(Collectors.toSet());

                ArtistFilters artistFilters = new ArtistFilters();
                artistFilters.setIds(new ArrayList<>(artistIds));
                Projections artistProjections = inclusion.getByKey(INCLUSION_ARTIST_DOMAIN);
                List<ArtistDTO> artistDTOs = artistClient.findAll(artistProjections, artistFilters);
                includeDTO.setArtists(artistDTOs);
            }
        }
        return includeDTO;
    }
}
