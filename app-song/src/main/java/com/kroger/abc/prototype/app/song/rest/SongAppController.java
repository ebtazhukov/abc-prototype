package com.kroger.abc.prototype.app.song.rest;

import com.kroger.abc.prototype.app.song.service.SongAppService;
import com.kroger.abc.prototype.common.rest.ApiRequestBody;
import com.kroger.abc.prototype.common.rest.dto.song.app.SongListResponseBody;
import com.kroger.abc.prototype.common.rest.dto.song.app.SongResponseBody;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.inclusion.SongInclusions;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Song.
 */
@RestController
@RequestMapping("/app")
@Api(
        value = "app-song",
        tags = "app-song",
        description = "Application service responding for song domain."
)
@Profile({"default", "app-songs"})
public class SongAppController {

    private static final String ENTITY_NAME = "songs";
    private final Logger log = LoggerFactory.getLogger(SongAppController.class);
    private final SongAppService songAppService;

    public SongAppController(SongAppService songAppService) {
        this.songAppService = songAppService;
    }

    /**
     * GET  /songs/:id : get song for specific id.
     *
     * @param id the id of the song for which songDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the songDTO, or with status 404 (Not Found)
     */
    @GetMapping("/songs/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get song for specific id",
            notes = "This method is for obtaining song for specific id"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = SongResponseBody.class)
    })
    public ResponseEntity<SongResponseBody> getSong(
            @ApiParam(value = "**Song ID**.", required = true) @PathVariable String id,
            @Valid SongInclusions inclusions) {

        log.debug("REST request to get Song : {}, query: {}", id, QueryParametersHelper.toDebugString(inclusions));
        return ResponseEntity.ok(songAppService.findOne(id, inclusions));
    }

    /**
     * GET  /songs : get all the songs
     *
     * @return the ResponseEntity with status 200 (OK) and with body the songDTO list, or with status 404 (Not Found)
     */
    @GetMapping("/songs")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get all the songs",
            notes = "This method is for obtaining all of the songs"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = SongListResponseBody.class)
    })
    public ResponseEntity<SongListResponseBody> getSongs(
            @Valid SongInclusions inclusions,
            @Valid SongFilters songFilters) {

        log.debug("REST request to get all the Songs, query: {}",
                QueryParametersHelper.toDebugString(inclusions, songFilters));
        return ResponseEntity.ok(songAppService.findAll(inclusions, songFilters));
    }

    /**
     * PUT  /songs : Updates an existing song.
     *
     * @param request the songDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated songDTO, or with status 404 (Not Found)
     */
    @PutMapping("/songs")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Update the song",
            notes = "This method is for updating song"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = SongResponseBody.class)
    })
    public ResponseEntity<SongResponseBody> updateSong(@Valid @RequestBody ApiRequestBody<SongDTO> request) {
        SongDTO songDTO = request.getData();
        log.debug("REST request to update Song : {}", songDTO);
        return ResponseEntity.ok(songAppService.update(songDTO));
    }
}
