package com.kroger.abc.prototype.app.song.service;

import com.kroger.abc.prototype.common.rest.dto.song.app.SongListResponseBody;
import com.kroger.abc.prototype.common.rest.dto.song.app.SongResponseBody;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.inclusion.Inclusions;

/**
 * Service Interface for managing SongDetails.
 */
public interface SongAppService {


    /**
     * Get all the songs.
     *
     * @return the list of entities
     */
    SongListResponseBody findAll(Inclusions inclusions, SongFilters songFilters);

    /**
     * Get the "id" song.
     *
     * @param id         the id of the entity
     * @param inclusions
     * @return the entity
     */
    SongResponseBody findOne(String id, Inclusions inclusions);

    /**
     * Update the song.
     *
     * @param songDTO the entity with updated fields
     * @return the entity
     */
    SongResponseBody update(SongDTO songDTO);

}
