package com.kroger.abc.prototype.app.song.service.impl;

import com.kroger.abc.prototype.app.song.service.SongAppService;
import com.kroger.abc.prototype.common.client.artist.core.ArtistClient;
import com.kroger.abc.prototype.common.client.song.business.song.SongClient;
import com.kroger.abc.prototype.common.rest.dto.MetadataDTO;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;
import com.kroger.abc.prototype.common.rest.dto.song.app.SongIncludeDTO;
import com.kroger.abc.prototype.common.rest.dto.song.app.SongListResponseBody;
import com.kroger.abc.prototype.common.rest.dto.song.app.SongResponseBody;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongRelationships;
import com.kroger.abc.prototype.common.rest.param.filter.ArtistFilters;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.inclusion.Inclusions;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.util.projection.Inclusion;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.kroger.abc.prototype.common.rest.util.projection.Inclusion.INCLUSION_ARTIST_DOMAIN;
import static com.kroger.abc.prototype.common.rest.util.projection.Inclusion.INCLUSION_SONG_DOMAIN;

/**
 * Service Implementation for managing Song.
 */
@Service
public class SongAppServiceImpl implements SongAppService {

    private final Logger log = LoggerFactory.getLogger(SongAppServiceImpl.class);

    private final SongClient songClient;
    private final ArtistClient artistClient;

    public SongAppServiceImpl(SongClient songClient, ArtistClient artistClient) {
        this.songClient = songClient;
        this.artistClient = artistClient;
    }

    /**
     * Get all the songDetails.
     *
     * @return the list of entities
     */
    @Override
    public SongListResponseBody findAll(Inclusions inclusions, SongFilters songFilters) {
        log.debug("Request to get all Songs");
        Inclusion inclusion = inclusions.extract();
        Projections songProjections = inclusion.getByKey(INCLUSION_SONG_DOMAIN);

        List<SongDTO> songDTOs = songClient.findAll(songProjections, songFilters);
        return SongListResponseBody.of(songDTOs, getSongInclusion(inclusion, songDTOs), MetadataDTO.fromInclusion(inclusion));
    }

    /**
     * Get one songDetails by id.
     *
     * @param id         the id of the entity
     * @param inclusions
     * @return the entity
     */
    @Override
    public SongResponseBody findOne(String id, Inclusions inclusions) {
        log.debug("Request to get Song : {}", id);
        Inclusion inclusion = inclusions.extract();
        Projections songProjections = inclusion.getByKey(INCLUSION_SONG_DOMAIN);

        SongDTO songDTO = songClient.findOne(UUID.fromString(id), songProjections);
        return SongResponseBody.of(songDTO, getSongInclusion(inclusion, songDTO), MetadataDTO.fromInclusion(inclusion));
    }

    /**
     * Update the song.
     *
     * @param songDTO the entity with updated fields
     * @return the entity
     */
    @Override
    public SongResponseBody update(SongDTO songDTO) {
        log.debug("Request to update the Song : {}", songDTO);
        return SongResponseBody.of(songClient.update(songDTO), null, null);
    }

    private SongIncludeDTO getSongInclusion(Inclusion inclusion, SongDTO songDTO) {
        return getSongInclusion(inclusion, Collections.singletonList(songDTO));
    }

    private SongIncludeDTO getSongInclusion(Inclusion inclusion, List<SongDTO> songDTOs) {
        SongIncludeDTO includeDTO = new SongIncludeDTO();

        if (CollectionUtils.isNotEmpty(songDTOs) && inclusion.containsKey(INCLUSION_ARTIST_DOMAIN)) {

            Set<UUID> artistIds = songDTOs.stream()
                    .map(dto -> SongRelationships.extractArtist(dto.getRelationships()))
                    .collect(Collectors.toSet());

            ArtistFilters artistFilters = new ArtistFilters();
            artistFilters.setIds(new ArrayList<>(artistIds));
            Projections artistProjections = inclusion.getByKey(INCLUSION_ARTIST_DOMAIN);
            List<ArtistDTO> artistDTOs = artistClient.findAll(artistProjections, artistFilters);

            includeDTO.setArtists(artistDTOs);
        }
        return includeDTO;
    }
}
