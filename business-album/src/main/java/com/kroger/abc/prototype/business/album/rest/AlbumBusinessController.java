package com.kroger.abc.prototype.business.album.rest;

import com.kroger.abc.prototype.business.album.service.AlbumBusinessService;
import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.album.business.AlbumCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.business.AlbumDTO;
import com.kroger.abc.prototype.common.rest.param.filter.AlbumFilters;
import com.kroger.abc.prototype.common.rest.param.projection.AlbumProjections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Albums.
 */
@RestController
@RequestMapping("/business")
@Api(
        value = "business-album",
        tags = "business-album",
        description = "Business service responding for album domain."
)
@Profile({"default", "business-albums"})
public class AlbumBusinessController {

    private static final String ENTITY_NAME = "albums";
    private final Logger log = LoggerFactory.getLogger(AlbumBusinessController.class);
    private final AlbumBusinessService albumBusinessService;

    public AlbumBusinessController(AlbumBusinessService albumBusinessService) {
        this.albumBusinessService = albumBusinessService;
    }

    /**
     * GET  /albums/:id : get album for specific id.
     *
     * @param id the id of the album for which albumDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the albumDTO, or with status 404 (Not Found)
     */
    @GetMapping("/albums/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get album for specific id",
            notes = "This method is for obtaining album for specific id"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = AlbumDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<AlbumDTO>> getAlbum(
            @ApiParam(value = "**Album ID**.", required = true) @PathVariable String id,
            @Valid AlbumProjections projections) {

        log.debug("REST request to get Album : {}, query: {}", QueryParametersHelper.toDebugString(projections));
        AlbumDTO albumDTO = albumBusinessService.findOne(id, projections);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(albumDTO));
    }

    /**
     * GET  /albums : get all the albums
     *
     * @return the ResponseEntity with status 200 (OK) and with body the albumDTO list, or with status 404 (Not Found)
     */
    @GetMapping("/albums")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get all the albums",
            notes = "This method is for obtaining all of the albums"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = AlbumDTO[].class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<List<AlbumDTO>>> getAlbums(
            @Valid AlbumFilters albumFilters,
            @Valid AlbumProjections projections) {

        log.debug("REST request to get all the Albums, query: {}",
                QueryParametersHelper.toDebugString(projections, albumFilters));
        List<AlbumDTO> albumDTOs = albumBusinessService.findAll(projections, albumFilters);
        return ResponseEntity.ok(ApiResponseBodyDeprecated.of(albumDTOs));
    }

    /**
     * POST  /albums : Creates a new album.
     *
     * @param albumCreateDTO the data for album creation
     * @return the ResponseEntity with status 201 (CREATED) and with body the albumDTO
     */
    @PostMapping("/albums")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(
            value = "Create a new album",
            notes = "This method is for creating the album. \n\n" +
                    "It also creates missing content (songs & artists) by calling corresponding C-layer services."
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "OK", response = AlbumDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<AlbumDTO>> updateSong(
            @Valid @RequestBody AlbumCreateDTO albumCreateDTO) {
        log.debug("REST request to create the Album : {}", albumCreateDTO);
        AlbumDTO result = albumBusinessService.create(albumCreateDTO);
        return ResponseEntity.created(URI.create("")).body(ApiResponseBodyDeprecated.of(result));
    }

}
