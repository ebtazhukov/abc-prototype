package com.kroger.abc.prototype.business.album.service;

import com.kroger.abc.prototype.common.rest.dto.album.business.AlbumCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.business.AlbumDTO;
import com.kroger.abc.prototype.common.rest.param.filter.AlbumFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;

/**
 * Service Interface for managing Albums.
 */
public interface AlbumBusinessService {


    /**
     * Get all the albums.
     *
     * @param projections
     * @param albumFilters
     * @return the list of entities
     */
    List<AlbumDTO> findAll(Projections projections, AlbumFilters albumFilters);

    /**
     * Get the "id" album.
     *
     * @param id          the id of the entity
     * @param projections
     * @return the entity
     */
    AlbumDTO findOne(String id, Projections projections);

    AlbumDTO create(AlbumCreateDTO albumCreateDTO);
}
