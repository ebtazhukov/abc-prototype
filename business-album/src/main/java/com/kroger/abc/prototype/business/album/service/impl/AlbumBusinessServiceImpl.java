package com.kroger.abc.prototype.business.album.service.impl;

import com.kroger.abc.prototype.business.album.service.AlbumBusinessService;
import com.kroger.abc.prototype.business.album.service.mapper.AlbumDTOMapper;
import com.kroger.abc.prototype.common.client.album.core.albumdetails.AlbumDetailsClient;
import com.kroger.abc.prototype.common.client.album.core.albumnotes.AlbumNotesClient;
import com.kroger.abc.prototype.common.client.artist.core.ArtistClient;
import com.kroger.abc.prototype.common.client.song.business.song.SongClient;
import com.kroger.abc.prototype.common.rest.dto.album.business.AlbumCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.business.AlbumDTO;
import com.kroger.abc.prototype.common.rest.dto.album.business.AlbumSongCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.business.NotesDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumRelationships;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongRelationships;
import com.kroger.abc.prototype.common.rest.param.filter.AlbumFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.kroger.abc.prototype.common.rest.util.projection.Projection.PROJECTION_ALBUM_NOTES_KEY;

/**
 * Service Implementation for managing Album.
 */
@Service
public class AlbumBusinessServiceImpl implements AlbumBusinessService {

    private final Logger log = LoggerFactory.getLogger(AlbumBusinessServiceImpl.class);

    private final AlbumDTOMapper albumDTOMapper;

    private final AlbumDetailsClient albumDetailsClient;

    private final AlbumNotesClient albumNotesClient;

    private final ArtistClient artistClient;

    private final SongClient songClient;


    public AlbumBusinessServiceImpl(AlbumDTOMapper albumDTOMapper, AlbumDetailsClient albumDetailsClient,
                                    AlbumNotesClient albumNotesClient, ArtistClient artistClient,
                                    SongClient songClient) {
        this.albumDTOMapper = albumDTOMapper;
        this.albumDetailsClient = albumDetailsClient;
        this.albumNotesClient = albumNotesClient;
        this.artistClient = artistClient;
        this.songClient = songClient;
    }

    /**
     * Get all the albumDetails.
     *
     * @param projections
     * @param albumFilters
     * @return the list of entities
     */
    @Override
    public List<AlbumDTO> findAll(Projections projections, AlbumFilters albumFilters) {
        log.debug("Request to get all Albums");
        LinkedList<AlbumDTO> albumDTOs = albumDetailsClient
                .findAll(projections, albumFilters)
                .stream()
                .map(albumDTOMapper::toDestination)
                .collect(Collectors.toCollection(LinkedList::new));

        enrichAlbumDTOWithNotes(projections, albumDTOs);
        return albumDTOs;
    }

    /**
     * Get one albumDetails by id.
     *
     * @param id          the id of the entity
     * @param projections
     * @return the entity
     */
    @Override
    public AlbumDTO findOne(String id, Projections projections) {
        log.debug("Request to get Album : {}", id);
        AlbumDetailsDTO albumDetails = albumDetailsClient.findOne(UUID.fromString(id), projections);
        AlbumDTO albumDTO = null;
        if (albumDetails != null) {
            albumDTO = albumDTOMapper.toDestination(albumDetails);
            enrichAlbumDTOWithNotes(projections, albumDTO);
        }
        return albumDTO;
    }

    @Override
    public AlbumDTO create(AlbumCreateDTO albumCreateDTO) {
        Map<String, UUID> artistMap = getOrCreateArtists(albumCreateDTO);
        List<UUID> songs = getOrCreateSongs(albumCreateDTO.getSongs(), artistMap);
        AlbumDTO result = createAlbum(albumCreateDTO, artistMap, songs);
        createAlbumNotes(result, albumCreateDTO.getNotes());

        return result;
    }

    private void createAlbumNotes(AlbumDTO album, String notes) {
        AlbumNotesCreateDTO createDTO = new AlbumNotesCreateDTO(album.getId(), notes);
        AlbumNotesDTO created = albumNotesClient.create(createDTO);
        album.setNotes(new NotesDTO(created.getNotes()));
    }

    private AlbumDTO createAlbum(AlbumCreateDTO albumCreateDTO, Map<String, UUID> artistMap, List<UUID> songs) {
        AlbumDetailsCreateDTO createDTO = new AlbumDetailsCreateDTO(
                albumCreateDTO.getTitle(),
                AlbumRelationships.build(artistMap.get(albumCreateDTO.getArtist()), songs));
        return albumDTOMapper.toDestination(albumDetailsClient.create(createDTO));
    }

    private List<UUID> getOrCreateSongs(List<AlbumSongCreateDTO> songs, Map<String, UUID> artistMap) {
        List<SongCreateDTO> songsCreateDTO = CollectionUtils.emptyIfNull(songs).stream().map(song ->
                new SongCreateDTO(
                        song.getTitle(),
                        SongRelationships.build(artistMap.get(song.getArtist())),
                        song.getDuration(),
                        song.getBitrate(),
                        song.getSubscribeOnly(),
                        song.getLyrics(),
                        song.getFileURL()))
                .collect(Collectors.toList());

        List<SongDTO> result = songClient.getOrCreateInBulk(songsCreateDTO);
        return CollectionUtils.emptyIfNull(result).stream().map(SongDTO::getId).collect(Collectors.toList());
    }


    private Map<String, UUID> getOrCreateArtists(AlbumCreateDTO albumCreateDTO) {
        Map<String, UUID> resultArtistNameMapping = new HashMap<>();

        //collect a set of all artists from album and all of its songs
        Set<String> artistNames = new HashSet<>();
        artistNames.add(albumCreateDTO.getArtist());
        albumCreateDTO.getSongs().forEach(song -> artistNames.add(song.getArtist()));

        //get or create artists and put their ids to the map (with name as a key)
        List<ArtistCreateDTO> artistsCreateDTO = CollectionUtils.emptyIfNull(artistNames).stream()
                .map(ArtistCreateDTO::new)
                .collect(Collectors.toList());

        List<ArtistDTO> result = artistClient.getOrCreateInBulk(artistsCreateDTO);
        CollectionUtils.emptyIfNull(result).forEach(
                artist -> resultArtistNameMapping.put(artist.getName(), artist.getId()));

        return resultArtistNameMapping;
    }

    private void enrichAlbumDTOWithNotes(Projections projections, AlbumDTO albumDTOs) {
        enrichAlbumDTOWithNotes(projections, Collections.singletonList(albumDTOs));
    }

    private void enrichAlbumDTOWithNotes(Projections projections, List<AlbumDTO> albumDTOs) {
        if (CollectionUtils.isNotEmpty(albumDTOs) && projections != null
                && projections.extract().containsKey(PROJECTION_ALBUM_NOTES_KEY)) {

            List<UUID> albumIds = albumDTOs.stream().map(AlbumDTO::getId).collect(Collectors.toList());
            List<AlbumNotesDTO> albumNotesDTOs = albumNotesClient.findByAlbumIds(albumIds, projections);

            if (CollectionUtils.isNotEmpty(albumNotesDTOs)) {
                Map<UUID, List<AlbumNotesDTO>> mappedNotesDTOs =
                        albumNotesDTOs.stream().collect(Collectors.groupingBy(AlbumNotesDTO::getAlbumId));

                for (AlbumDTO albumDTO : albumDTOs) {
                    List<AlbumNotesDTO> notesByAlbum = mappedNotesDTOs.get(albumDTO.getId());
                    if (CollectionUtils.isNotEmpty(notesByAlbum)) {
                        albumDTO.setNotes(new NotesDTO(notesByAlbum.get(0).getNotes()));
                    }
                }
            }
        }
    }
}
