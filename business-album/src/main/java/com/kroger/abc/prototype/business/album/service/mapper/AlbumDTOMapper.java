package com.kroger.abc.prototype.business.album.service.mapper;

import com.kroger.abc.prototype.common.rest.dto.album.business.AlbumDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsDTO;
import com.kroger.abc.prototype.common.rest.mapper.DTOMapper;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * Mapper for the AlbumDTO and AlbumDetailsDTO cross-mapping.
 */
@Component
public class AlbumDTOMapper implements DTOMapper<AlbumDetailsDTO, AlbumDTO> {

    @Override
    public AlbumDetailsDTO toSource(AlbumDTO dto) {
        return dto == null ? null :
                new AlbumDetailsDTO(dto.getId(), dto.getCreatedAt(), dto.getModifiedAt(),
                        dto.getTitle(), dto.getRelationships());
    }

    @Override
    public AlbumDTO toDestination(AlbumDetailsDTO dto) {
        return dto == null ? null :
                new AlbumDTO(dto.getId(), dto.getCreatedAt(), dto.getModifiedAt(),
                        dto.getTitle(), dto.getRelationships(), null);
    }

    @Override
    public List<AlbumDetailsDTO> toSource(List<AlbumDTO> dtoList) {
        return dtoList == null ? null :
                dtoList.stream().map(this::toSource).collect(Collectors.toList());
    }

    @Override
    public List<AlbumDTO> toDestination(List<AlbumDetailsDTO> dtoList) {
        return dtoList == null ? null :
                dtoList.stream().map(this::toDestination).collect(Collectors.toList());
    }
}
