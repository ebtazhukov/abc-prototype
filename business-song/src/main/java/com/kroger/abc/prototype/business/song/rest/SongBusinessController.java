package com.kroger.abc.prototype.business.song.rest;

import com.kroger.abc.prototype.business.song.service.SongBusinessService;
import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.projection.SongProjections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing SongDetails.
 */
@RestController
@RequestMapping("/business")
@Api(
        value = "business-song",
        tags = "business-song",
        description = "Business service responding for song domain."
)
@Profile({"default", "business-songs"})
public class SongBusinessController {

    private static final String ENTITY_NAME = "songs";
    private final Logger log = LoggerFactory.getLogger(SongBusinessController.class);
    private final SongBusinessService songBusinessService;

    public SongBusinessController(SongBusinessService songBusinessService) {
        this.songBusinessService = songBusinessService;
    }

    /**
     * GET  /songs/:id : get song for specific id.
     *
     * @param id the id of the song for which songDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the songDTO, or with status 404 (Not Found)
     */
    @GetMapping("/songs/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get song for specific id",
            notes = "This method is for obtaining song for specific id"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = SongDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<SongDTO>> getSong(
            @ApiParam(value = "**Song ID**.", required = true) @PathVariable String id,
            @Valid SongProjections projections) {

        log.debug("REST request to get Song : {}, query: {}", QueryParametersHelper.toDebugString(projections));
        SongDTO songDTO = songBusinessService.findOne(id, projections);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(songDTO));
    }

    /**
     * GET  /songs : get all the songs
     *
     * @return the ResponseEntity with status 200 (OK) and with body the songDTO list, or with status 404 (Not Found)
     */
    @GetMapping("/songs")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get all the songs",
            notes = "This method is for obtaining all of the songs"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = SongDTO[].class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<List<SongDTO>>> getSongs(
            @Valid SongProjections projections,
            @Valid SongFilters songFilters) {

        log.debug("REST request to get all the Songs, query: {}",
                QueryParametersHelper.toDebugString(projections, songFilters));
        List<SongDTO> songDTOs = songBusinessService.findAll(projections, songFilters);
        return ResponseEntity.ok(ApiResponseBodyDeprecated.of(songDTOs));
    }

    /**
     * PUT  /songs : Updates an existing song.
     *
     * @param songDTO the songDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated songDTO, or with status 404 (Not Found)
     */
    @PutMapping("/songs")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Update the song",
            notes = "This method is for updating song"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = SongDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<SongDTO>> updateSong(@Valid @RequestBody SongDTO songDTO) {
        log.debug("REST request to update Song : {}", songDTO);
        SongDTO result = songBusinessService.update(songDTO);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(result));
    }

    /**
     * POST  /songs/get-or-create : Return all songs, creates any non-existed.
     *
     * @param createDTO List<SongCreateDTO> list of songs to return (create if doesn't exist)
     * @return the ResponseEntity with status 200 (OK) and with body the List<SongDTO>
     */
    @PostMapping("/songs/get-or-create")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get or create songs in bulk",
            notes = "This method is for returning all requested songs, matching by title, artistId, duration and bitrate." +
                    " Any non-existed will be created."
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = SongDTO[].class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<List<SongDTO>>> getOrCreateSongsInBulk(
            @Valid @RequestBody List<SongCreateDTO> createDTO) {

        log.debug("REST request to get or create Songs : {}", createDTO);
        List<SongDTO> songDTOS = songBusinessService.getOrCreateInBulk(createDTO);
        return ResponseEntity.ok().body(ApiResponseBodyDeprecated.of(songDTOS));
    }

}
