package com.kroger.abc.prototype.business.song.service;

import com.kroger.abc.prototype.common.rest.dto.song.business.SongCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;

/**
 * Service Interface for managing SongDetails.
 */
public interface SongBusinessService {


    /**
     * Get all the songs.
     *
     * @param projections
     * @param songFilters
     * @return the list of entities
     */
    List<SongDTO> findAll(Projections projections, SongFilters songFilters);

    /**
     * Get the "id" song.
     *
     * @param id          the id of the entity
     * @param projections
     * @return the entity
     */
    SongDTO findOne(String id, Projections projections);

    /**
     * Update the song.
     *
     * @param songDTO the song entity to update
     * @return the entity
     */
    SongDTO update(SongDTO songDTO);

    List<SongDTO> getOrCreateInBulk(List<SongCreateDTO> createDTO);
}
