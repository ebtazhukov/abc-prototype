package com.kroger.abc.prototype.business.song.service.impl;

import com.kroger.abc.prototype.business.song.service.SongBusinessService;
import com.kroger.abc.prototype.business.song.service.mapper.SongDTOMapper;
import com.kroger.abc.prototype.common.client.song.core.songdetails.SongDetailsClient;
import com.kroger.abc.prototype.common.client.song.core.songdigitalfile.SongDigitalFileClient;
import com.kroger.abc.prototype.common.client.song.core.songlyrics.SongLyricsClient;
import com.kroger.abc.prototype.common.rest.dto.song.business.DigitalFileDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.LyricsDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongRelationships;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.param.projection.SongDetailsProjections;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.kroger.abc.prototype.common.rest.util.projection.Projection.PROJECTION_SONG_DIGITAL_FILE_KEY;
import static com.kroger.abc.prototype.common.rest.util.projection.Projection.PROJECTION_SONG_LYRICS_KEY;

/**
 * Service Implementation for managing Song.
 */
@Service
public class SongBusinessServiceImpl implements SongBusinessService {

    private final Logger log = LoggerFactory.getLogger(SongBusinessServiceImpl.class);

    private final SongDTOMapper songDTOMapper;

    private final SongDetailsClient songDetailsClient;

    private final SongLyricsClient songLyricsClient;

    private final SongDigitalFileClient songDigitalFileClient;

    public SongBusinessServiceImpl(SongDTOMapper songDTOMapper, SongDetailsClient songDetailsClient,
                                   SongLyricsClient songLyricsClient, SongDigitalFileClient songDigitalFileClient) {
        this.songDTOMapper = songDTOMapper;
        this.songDetailsClient = songDetailsClient;
        this.songLyricsClient = songLyricsClient;
        this.songDigitalFileClient = songDigitalFileClient;
    }

    /**
     * Get all the songDetails.
     *
     * @param projections
     * @param songFilters
     * @return the list of entities
     */
    @Override
    public List<SongDTO> findAll(Projections projections, SongFilters songFilters) {
        log.debug("Request to get all Songs");
        LinkedList<SongDTO> songDTOs = songDetailsClient
                .findAll(projections, songFilters)
                .stream()
                .map(songDTOMapper::toDestination)
                .collect(Collectors.toCollection(LinkedList::new));

        enrichSongDTOWithLyrics(projections, songDTOs);
        enrichSongDTOWithDigitalFiles(projections, songDTOs);
        return songDTOs;
    }

    /**
     * Get one songDetails by id.
     *
     * @param id          the id of the entity
     * @param projections
     * @return the entity
     */
    @Override
    public SongDTO findOne(String id, Projections projections) {
        log.debug("Request to get Song : {}", id);
        SongDetailsDTO songDetails = songDetailsClient.findOne(UUID.fromString(id), projections);
        SongDTO songDTO = null;
        if (songDetails != null) {
            songDTO = songDTOMapper.toDestination(songDetails);
            enrichSongDTOWithLyrics(projections, songDTO);
            enrichSongDTOWithDigitalFiles(projections, songDTO);
        }
        return songDTO;
    }

    /**
     * Update the song.
     *
     * @param songDTO the song entity to update
     * @return the entity
     */
    @Override
    public SongDTO update(SongDTO songDTO) {
        log.debug("Request to update Song : {}", songDTO);
        SongDetailsDTO songDetails = songDetailsClient.update(songDTOMapper.toSource(songDTO));
        SongDTO result = null;
        if (songDetails != null) {
            result = songDTOMapper.toDestination(songDetails);
        }
        return result;
    }

    @Override
    public List<SongDTO> getOrCreateInBulk(List<SongCreateDTO> createDTO) {
        List<SongDetailsDTO> result = new ArrayList<>();

        //find all existed songs among requested
        List<SongDetailsDTO> existed = songDetailsClient.findAll(new SongDetailsProjections("details_full"), new SongFilters());
        List<SongCreateDTO> nonExisted = new ArrayList<>();
        createDTO.forEach(requested -> {
            Optional<SongDetailsDTO> existedSong = existed.stream()
                    .filter(song -> Objects.equals(song.getTitle(), requested.getTitle())
                            && Objects.equals(SongRelationships.extractArtist(song.getRelationships()), SongRelationships.extractArtist(requested.getRelationships()))
                            && Objects.equals(song.getBitrate(), requested.getBitrate())
                            && Objects.equals(song.getDuration(), requested.getDuration()))
                    .findFirst();
            if (existedSong.isPresent()) {
                result.add(existedSong.get());
            } else {
                nonExisted.add(requested);
            }
        });

        //create all non-existed songs and their lyrics and digitalFiles
        nonExisted.forEach(song -> {
            SongDetailsCreateDTO songDetailsCreateDTO =
                    new SongDetailsCreateDTO(song.getTitle(), song.getRelationships(),
                            song.getDuration(), song.getBitrate(), song.getSubscribeOnly());

            SongDetailsDTO songDetails = songDetailsClient.create(songDetailsCreateDTO);

            SongLyricsCreateDTO lyricsCreateDTO = new SongLyricsCreateDTO(songDetails.getId(), song.getLyrics());
            songLyricsClient.create(lyricsCreateDTO);

            SongDigitalFileCreateDTO digitalFileCreateDTO = new SongDigitalFileCreateDTO(songDetails.getId(), song.getFileURL());
            songDigitalFileClient.create(digitalFileCreateDTO);

            result.add(songDetails);
        });

        return songDTOMapper.toDestination(result);
    }

    private void enrichSongDTOWithLyrics(Projections projections, SongDTO songDTOs) {
        enrichSongDTOWithLyrics(projections, Collections.singletonList(songDTOs));
    }

    private void enrichSongDTOWithLyrics(Projections projections, List<SongDTO> songDTOs) {
        if (CollectionUtils.isNotEmpty(songDTOs) && projections != null
                && projections.extract().containsKey(PROJECTION_SONG_LYRICS_KEY)) {

            List<UUID> songIds = songDTOs.stream().map(SongDTO::getId).collect(Collectors.toList());
            List<SongLyricsDTO> songLyricsDTOs = songLyricsClient.findBySongIds(songIds, projections);

            if (CollectionUtils.isNotEmpty(songLyricsDTOs)) {
                Map<UUID, List<SongLyricsDTO>> mappedLyricsDTOs =
                        songLyricsDTOs.stream().collect(Collectors.groupingBy(SongLyricsDTO::getSongId));

                for (SongDTO songDTO : songDTOs) {
                    List<SongLyricsDTO> lyricsBySong = mappedLyricsDTOs.get(songDTO.getId());
                    if (CollectionUtils.isNotEmpty(lyricsBySong)) {
                        songDTO.setLyrics(new LyricsDTO(lyricsBySong.get(0).getLyrics()));
                    }
                }
            }
        }
    }

    private void enrichSongDTOWithDigitalFiles(Projections projections, SongDTO songDTOs) {
        enrichSongDTOWithDigitalFiles(projections, Collections.singletonList(songDTOs));
    }

    private void enrichSongDTOWithDigitalFiles(Projections projections, List<SongDTO> songDTOs) {
        if (CollectionUtils.isNotEmpty(songDTOs) && projections != null
                && projections.extract().containsKey(PROJECTION_SONG_DIGITAL_FILE_KEY)) {

            List<UUID> songIds = songDTOs.stream().map(SongDTO::getId).collect(Collectors.toList());
            List<SongDigitalFileDTO> songDigitalFileDTOs = songDigitalFileClient.findBySongIds(songIds, projections);

            if (CollectionUtils.isNotEmpty(songDigitalFileDTOs)) {
                Map<UUID, List<SongDigitalFileDTO>> mappedDigitalFilesDTOs =
                        songDigitalFileDTOs.stream().collect(Collectors.groupingBy(SongDigitalFileDTO::getSongId));

                for (SongDTO songDTO : songDTOs) {
                    List<SongDigitalFileDTO> digitalFilesBySong = mappedDigitalFilesDTOs.get(songDTO.getId());
                    if (CollectionUtils.isNotEmpty(digitalFilesBySong)) {
                        songDTO.setDigitalFile(new DigitalFileDTO(digitalFilesBySong.get(0).getUrl()));
                    }
                }
            }
        }
    }
}
