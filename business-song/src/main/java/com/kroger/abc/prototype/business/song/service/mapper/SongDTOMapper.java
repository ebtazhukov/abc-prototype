package com.kroger.abc.prototype.business.song.service.mapper;

import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsDTO;
import com.kroger.abc.prototype.common.rest.mapper.DTOMapper;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * Mapper for the AlbumDTO and SongDetailsDTO cross-mapping.
 */
@Component
public class SongDTOMapper implements DTOMapper<SongDetailsDTO, SongDTO> {

    @Override
    public SongDetailsDTO toSource(SongDTO dto) {
        return dto == null ? null :
                new SongDetailsDTO(dto.getId(), dto.getCreatedAt(), dto.getModifiedAt(),
                        dto.getTitle(), dto.getRelationships(), dto.getDuration(), dto.getBitrate(), dto.getSubscribeOnly());
    }

    @Override
    public SongDTO toDestination(SongDetailsDTO dto) {
        return dto == null ? null :
                new SongDTO(dto.getId(), dto.getCreatedAt(), dto.getModifiedAt(), dto.getTitle(), dto.getRelationships(),
                        dto.getDuration(), dto.getBitrate(), dto.getSubscribeOnly(), null, null);
    }

    @Override
    public List<SongDetailsDTO> toSource(List<SongDTO> dtoList) {
        return dtoList == null ? null :
                dtoList.stream().map(this::toSource).collect(Collectors.toList());
    }

    @Override
    public List<SongDTO> toDestination(List<SongDetailsDTO> dtoList) {
        return dtoList == null ? null :
                dtoList.stream().map(this::toDestination).collect(Collectors.toList());
    }
}
