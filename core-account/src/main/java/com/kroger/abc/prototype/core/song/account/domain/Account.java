package com.kroger.abc.prototype.core.song.account.domain;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import java.io.Serializable;
import java.time.Instant;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "account")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @PartitionKey
    private String username;

    private Instant createdAt;

    private Instant modifiedAt;

    private String firstName;

    private String lastName;

    private Boolean isSubscribed;
}
