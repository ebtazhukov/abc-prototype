package com.kroger.abc.prototype.core.song.account.repository;

import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.kroger.abc.prototype.core.song.account.domain.Account;
import javax.validation.Validator;
import org.springframework.stereotype.Repository;

/**
 * Cassandra repository for the Account entity.
 */
@Repository
public class AccountRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Account> mapper;

    public AccountRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(Account.class);
    }

    public Account findOne(String username) {
        return mapper.get(username);
    }

    public void delete(String username) {
        mapper.delete(username);
    }
}
