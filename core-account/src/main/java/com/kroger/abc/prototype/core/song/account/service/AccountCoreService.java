package com.kroger.abc.prototype.core.song.account.service;

import com.kroger.abc.prototype.common.rest.dto.account.AccountDTO;

/**
 * Service Interface for managing Account.
 */
public interface AccountCoreService {


    /**
     * Get Account for logged in user.
     *
     * @return the entity
     */
    AccountDTO findOne();

    void delete();
}