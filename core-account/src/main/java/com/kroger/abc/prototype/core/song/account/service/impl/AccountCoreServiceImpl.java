package com.kroger.abc.prototype.core.song.account.service.impl;

import com.kroger.abc.prototype.common.client.message.AccountMessagesClient;
import com.kroger.abc.prototype.common.rest.dto.account.AccountDTO;
import com.kroger.abc.prototype.common.rest.util.SecurityContextHelper;
import com.kroger.abc.prototype.core.song.account.repository.AccountRepository;
import com.kroger.abc.prototype.core.song.account.service.AccountCoreService;
import com.kroger.abc.prototype.core.song.account.service.mapper.AccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing Account.
 */
@Service
public class AccountCoreServiceImpl implements AccountCoreService {

    private final Logger log = LoggerFactory.getLogger(AccountCoreServiceImpl.class);

    private final AccountRepository accountRepository;

    private final AccountMapper accountMapper;

    private final AccountMessagesClient messagesClient;

    public AccountCoreServiceImpl(AccountRepository accountRepository, AccountMapper accountMapper, AccountMessagesClient messagesClient) {
        this.accountRepository = accountRepository;
        this.accountMapper = accountMapper;
        this.messagesClient = messagesClient;
    }

    /**
     * Get Account for logged in user.
     *
     * @return the entity
     */
    @Override
    public AccountDTO findOne() {
        String userName = SecurityContextHelper.getUserName();
        log.debug("Request to get Account: {}", userName);
        return accountMapper.toDto(accountRepository.findOne(userName));
    }

    /**
     * Delete Account for logged in user.
     */
    @Override
    public void delete() {
        String userName = SecurityContextHelper.getUserName();
        log.debug("Request to delete Account: {}", userName);
        accountRepository.delete(userName);
        messagesClient.create(userName);
    }
}
