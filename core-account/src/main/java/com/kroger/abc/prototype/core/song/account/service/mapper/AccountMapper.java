package com.kroger.abc.prototype.core.song.account.service.mapper;

import com.kroger.abc.prototype.common.rest.dto.account.AccountDTO;
import com.kroger.abc.prototype.common.rest.mapper.EntityMapper;
import com.kroger.abc.prototype.core.song.account.domain.Account;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * Mapper for the entity Account and its DTO AccountDTO.
 */
@Component
public class AccountMapper implements EntityMapper<AccountDTO, Account> {


    @Override
    public Account toEntity(AccountDTO dto) {
        return dto == null ? null :
                new Account(dto.getUsername(), dto.getCreatedAt(), dto.getModifiedAt(),
                        dto.getFirstName(), dto.getLastName(), dto.getIsSubscribed());
    }

    @Override
    public AccountDTO toDto(Account entity) {
        return entity == null ? null :
                new AccountDTO(entity.getUsername(), entity.getCreatedAt(), entity.getModifiedAt(),
                        entity.getFirstName(), entity.getLastName(), entity.getIsSubscribed());
    }

    @Override
    public List<Account> toEntity(List<AccountDTO> dtoList) {
        return dtoList == null ? null :
                dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<AccountDTO> toDto(List<Account> entityList) {
        return entityList == null ? null :
                entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}
