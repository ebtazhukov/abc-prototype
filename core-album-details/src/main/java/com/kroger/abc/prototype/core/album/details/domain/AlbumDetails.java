package com.kroger.abc.prototype.core.album.details.domain;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "albumDetails")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @PartitionKey
    private UUID id;

    private Instant createdAt;

    private Instant modifiedAt;

    private String title;

    private UUID artistId;

    private List<UUID> songIds;
}
