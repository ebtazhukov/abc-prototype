package com.kroger.abc.prototype.core.album.details.repository;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumRelationships;
import com.kroger.abc.prototype.core.album.details.domain.AlbumDetails;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.springframework.stereotype.Repository;

/**
 * Cassandra repository for the AlbumDetails entity.
 */
@Repository
public class AlbumDetailsRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<AlbumDetails> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public AlbumDetailsRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(AlbumDetails.class);
        this.findAllStmt = session.prepare("SELECT * FROM albumDetails");
        this.truncateStmt = session.prepare("TRUNCATE albumDetails");
    }

    public List<AlbumDetails> findAll() {
        List<AlbumDetails> albumDetailsList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
                row -> {
                    AlbumDetails albumDetails = new AlbumDetails();
                    albumDetails.setId(row.getUUID("id"));
                    albumDetails.setCreatedAt(row.get("createdAt", Instant.class));
                    albumDetails.setModifiedAt(row.get("modifiedAt", Instant.class));
                    albumDetails.setTitle(row.getString("title"));
                    albumDetails.setArtistId(row.getUUID("artistId"));
                    albumDetails.setSongIds(row.getList("songIds", UUID.class));
                    return albumDetails;
                }
        ).forEach(albumDetailsList::add);
        return albumDetailsList;
    }

    public AlbumDetails findOne(UUID id) {
        return mapper.get(id);
    }

    public AlbumDetails create(AlbumDetailsCreateDTO createDTO) {
        AlbumDetails albumDetails = new AlbumDetails();
        albumDetails.setId(UUID.randomUUID());
        albumDetails.setCreatedAt(Instant.now());
        albumDetails.setModifiedAt(Instant.now());
        albumDetails.setTitle(createDTO.getTitle());
        albumDetails.setArtistId(AlbumRelationships.extractArtist(createDTO.getRelationships()));
        albumDetails.setSongIds(AlbumRelationships.extractSongs(createDTO.getRelationships()));

        Set<ConstraintViolation<AlbumDetails>> violations = validator.validate(albumDetails);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(albumDetails);
        return albumDetails;
    }
}
