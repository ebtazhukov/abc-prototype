package com.kroger.abc.prototype.core.album.details.rest;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.AlbumFilters;
import com.kroger.abc.prototype.common.rest.param.projection.AlbumDetailsProjections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.ResponseUtil;
import com.kroger.abc.prototype.core.album.details.service.AlbumDetailsCoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing AlbumDetails.
 */
@RestController
@RequestMapping("/core")
@Api(
        value = "core-album-details",
        tags = "core-album-details",
        description = "Core service for providing album details."
)
@Profile({"default", "core-album-details"})
public class AlbumDetailsCoreController {

    private static final String ENTITY_NAME = "albumDetails";
    private final Logger log = LoggerFactory.getLogger(AlbumDetailsCoreController.class);
    private final AlbumDetailsCoreService albumDetailsCoreService;

    public AlbumDetailsCoreController(AlbumDetailsCoreService albumDetailsCoreService) {
        this.albumDetailsCoreService = albumDetailsCoreService;
    }

    /**
     * GET  /album-details/:id : get albumDetails for specific album.
     *
     * @param id the id of the album for which albumDetailsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the albumDetailsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/album-details/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get albumDetails for specific album",
            notes = "This method is for obtaining album details for specific album"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = AlbumDetailsDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<AlbumDetailsDTO>> getAlbumDetails(
            @ApiParam(value = "**Album ID**.", required = true) @PathVariable String id,
            @Valid AlbumDetailsProjections projections) {

        log.debug("REST request to get AlbumDetails : {}, query: {}", QueryParametersHelper.toDebugString(projections));
        AlbumDetailsDTO albumDetailsDTO = albumDetailsCoreService.findOne(id, projections);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(albumDetailsDTO));
    }

    /**
     * GET  /album-details : get albumDetails for all albums.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of albumDetails in body
     */
    @GetMapping("/album-details")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get albumDetails for all albums",
            notes = "This method is for obtaining album details for all albums"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = AlbumDetailsDTO[].class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<List<AlbumDetailsDTO>>> getAllAlbumDetails(
            @Valid AlbumFilters albumFilters,
            @Valid AlbumDetailsProjections projections) {

        log.debug("REST request to get all AlbumDetails, query: {}",
                QueryParametersHelper.toDebugString(projections, albumFilters));
        List<AlbumDetailsDTO> albumDetailsDTOS = albumDetailsCoreService.findAll(projections, albumFilters);
        return ResponseEntity.ok().body(ApiResponseBodyDeprecated.of(albumDetailsDTOS));
    }

    /**
     * POST  /album-details : Creates a new album details.
     *
     * @param createDTO AlbumDetailsCreateDTO to create
     * @return the ResponseEntity with status 201 (CREATED) and with body the AlbumDetailsDTO
     */
    @PostMapping("/album-details")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(
            value = "Create a new album details",
            notes = "This method is for creating an album details"
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "CREATED", response = AlbumDetailsDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<AlbumDetailsDTO>> createAlbumDetails(
            @Valid @RequestBody AlbumDetailsCreateDTO createDTO) {

        log.debug("REST request to create Album details : {}", createDTO);
        AlbumDetailsDTO result = albumDetailsCoreService.create(createDTO);
        return ResponseEntity.created(URI.create("")).body(ApiResponseBodyDeprecated.of(result));
    }
}
