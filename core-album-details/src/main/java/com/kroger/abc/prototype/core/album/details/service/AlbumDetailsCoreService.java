package com.kroger.abc.prototype.core.album.details.service;

import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.AlbumFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;

/**
 * Service Interface for managing AlbumDetails.
 */
public interface AlbumDetailsCoreService {

    /**
     * Get all the songDetails.
     *
     * @param projections
     * @param albumFilters
     * @return the list of entities
     */
    List<AlbumDetailsDTO> findAll(Projections projections, AlbumFilters albumFilters);

    /**
     * Get the "id" songDetails.
     *
     * @param id          the id of the entity
     * @param projections
     * @return the entity
     */
    AlbumDetailsDTO findOne(String id, Projections projections);

    AlbumDetailsDTO create(AlbumDetailsCreateDTO createDTO);
}
