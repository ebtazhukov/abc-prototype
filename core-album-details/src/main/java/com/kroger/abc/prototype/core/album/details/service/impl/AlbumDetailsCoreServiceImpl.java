package com.kroger.abc.prototype.core.album.details.service.impl;

import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.AlbumFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.util.projection.Projection;
import com.kroger.abc.prototype.core.album.details.domain.AlbumDetails;
import com.kroger.abc.prototype.core.album.details.repository.AlbumDetailsRepository;
import com.kroger.abc.prototype.core.album.details.service.AlbumDetailsCoreService;
import com.kroger.abc.prototype.core.album.details.service.mapper.AlbumDetailsMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.kroger.abc.prototype.common.rest.util.projection.Projection.PROJECTION_ALBUM_DETAILS_KEY;
import static com.kroger.abc.prototype.common.rest.util.projection.Projection.Shape.FULL;

/**
 * Service Implementation for managing AlbumDetails.
 */
@Service
public class AlbumDetailsCoreServiceImpl implements AlbumDetailsCoreService {

    private final Logger log = LoggerFactory.getLogger(AlbumDetailsCoreServiceImpl.class);

    private final AlbumDetailsRepository albumDetailsRepository;

    private final AlbumDetailsMapper albumDetailsMapper;

    public AlbumDetailsCoreServiceImpl(AlbumDetailsRepository albumDetailsRepository, AlbumDetailsMapper albumDetailsMapper) {
        this.albumDetailsRepository = albumDetailsRepository;
        this.albumDetailsMapper = albumDetailsMapper;
    }

    /**
     * Get all the songDetails.
     *
     * @param projections
     * @param albumFilters
     * @return the list of entities
     */
    @Override
    public List<AlbumDetailsDTO> findAll(Projections projections, AlbumFilters albumFilters) {
        log.debug("Request to get all AlbumDetails");
        Projection.Shape shape = projections.extract().getByKey(PROJECTION_ALBUM_DETAILS_KEY);

        List<AlbumDetails> albumDetails = albumDetailsRepository.findAll();

        //emulate real DB queries for simplicity
        String titleFilter = albumFilters.getTitle();
        List<UUID> filterIds = albumFilters.getIds();

        if (CollectionUtils.isNotEmpty(filterIds)) {
            albumDetails = albumDetails.stream()
                    .filter(albumDetail -> filterIds.contains(albumDetail.getId()))
                    .collect(Collectors.toList());
        } else {
            albumDetails = albumDetails.stream()
                    .filter(albumDetail -> StringUtils.isBlank(titleFilter) ||
                            StringUtils.containsIgnoreCase(albumDetail.getTitle(), titleFilter))
                    .collect(Collectors.toList());
        }

        return albumDetails.stream()
                .map(FULL.equals(shape) ? albumDetailsMapper::toDto : albumDetailsMapper::toDtoCompact)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one songDetails by id.
     *
     * @param id          the id of the entity
     * @param projections
     * @return the entity
     */
    @Override
    public AlbumDetailsDTO findOne(String id, Projections projections) {
        log.debug("Request to get AlbumDetails : {}", id);
        Projection.Shape shape = projections.extract().getByKey(PROJECTION_ALBUM_DETAILS_KEY);

        AlbumDetails albumDetails = albumDetailsRepository.findOne(UUID.fromString(id));
        return FULL.equals(shape) ? albumDetailsMapper.toDto(albumDetails) : albumDetailsMapper.toDtoCompact(albumDetails);
    }

    @Override
    public AlbumDetailsDTO create(AlbumDetailsCreateDTO createDTO) {
        log.debug("Request to create AlbumDetails : {}", createDTO);
        AlbumDetails albumDetails = albumDetailsRepository.create(createDTO);
        return albumDetailsMapper.toDto(albumDetails);
    }
}
