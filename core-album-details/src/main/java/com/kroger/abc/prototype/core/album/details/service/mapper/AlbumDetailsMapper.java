package com.kroger.abc.prototype.core.album.details.service.mapper;

import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumRelationships;
import com.kroger.abc.prototype.common.rest.mapper.EntityMapper;
import com.kroger.abc.prototype.core.album.details.domain.AlbumDetails;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * Mapper for the entity AlbumDetails and its DTO AlbumDetailsDTO.
 */
@Component
public class AlbumDetailsMapper implements EntityMapper<AlbumDetailsDTO, AlbumDetails> {


    @Override
    public AlbumDetails toEntity(AlbumDetailsDTO dto) {
        return dto == null ? null :
                new AlbumDetails(dto.getId(), dto.getCreatedAt(), dto.getModifiedAt(), dto.getTitle(),
                        AlbumRelationships.extractArtist(dto.getRelationships()),
                        AlbumRelationships.extractSongs(dto.getRelationships()));
    }

    @Override
    public AlbumDetailsDTO toDto(AlbumDetails entity) {
        return entity == null ? null :
                new AlbumDetailsDTO(entity.getId(), entity.getCreatedAt(), entity.getModifiedAt(), entity.getTitle(),
                        AlbumRelationships.build(entity.getArtistId(), entity.getSongIds()));
    }

    @Override
    public List<AlbumDetails> toEntity(List<AlbumDetailsDTO> dtoList) {
        return dtoList == null ? null :
                dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<AlbumDetailsDTO> toDto(List<AlbumDetails> entityList) {
        return entityList == null ? null :
                entityList.stream().map(this::toDto).collect(Collectors.toList());
    }

    public AlbumDetailsDTO toDtoCompact(AlbumDetails entity) {
        return entity == null ? null :
                new AlbumDetailsDTO(entity.getId(), null, null, entity.getTitle(), null);
    }

    public List<AlbumDetailsDTO> toDtoCompact(List<AlbumDetails> entityList) {
        return entityList == null ? null :
                entityList.stream().map(this::toDtoCompact).collect(Collectors.toList());
    }
}
