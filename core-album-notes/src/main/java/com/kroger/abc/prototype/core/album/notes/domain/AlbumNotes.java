package com.kroger.abc.prototype.core.album.notes.domain;

import com.datastax.driver.mapping.annotations.Table;
import java.io.Serializable;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "AlbumNotes")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumNotes implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private UUID id;

    @NotNull
    private UUID albumId;

    private String notes;

}
