package com.kroger.abc.prototype.core.album.notes.repository;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesCreateDTO;
import com.kroger.abc.prototype.core.album.notes.domain.AlbumNotes;
import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.springframework.stereotype.Repository;

/**
 * Cassandra repository for the AlbumNotes entity.
 */
@Repository
public class AlbumNotesRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<AlbumNotes> mapper;

    private PreparedStatement findByAlbumIdStmt;

    public AlbumNotesRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(AlbumNotes.class);
        this.findByAlbumIdStmt = session.prepare("SELECT * FROM albumNotes WHERE albumId = :albumId");
    }

    public AlbumNotes findByAlbumId(UUID albumId) {
        BoundStatement stmt = findByAlbumIdStmt.bind()
                .setUUID("albumId", albumId);
        return session.execute(stmt).all().stream().map(
                row -> {
                    AlbumNotes albumNotes = new AlbumNotes();
                    albumNotes.setId(row.getUUID("id"));
                    albumNotes.setAlbumId(row.getUUID("albumId"));
                    albumNotes.setNotes(row.getString("notes"));
                    return albumNotes;
                }
        ).findFirst().orElse(null);
    }

    public AlbumNotes create(AlbumNotesCreateDTO createDTO) {
        AlbumNotes albumNotes = new AlbumNotes();
        albumNotes.setId(UUID.randomUUID());
        albumNotes.setNotes(createDTO.getNotes());
        albumNotes.setAlbumId(createDTO.getAlbumId());

        Set<ConstraintViolation<AlbumNotes>> violations = validator.validate(albumNotes);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(albumNotes);
        return albumNotes;
    }
}
