package com.kroger.abc.prototype.core.album.notes.rest;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesDTO;
import com.kroger.abc.prototype.common.rest.param.filter.AlbumIdFilter;
import com.kroger.abc.prototype.common.rest.param.projection.AlbumNotesProjections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.ResponseUtil;
import com.kroger.abc.prototype.core.album.notes.service.AlbumNotesCoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing AlbumNotes.
 */
@RestController
@RequestMapping("/core")
@Api(
        value = "core-album-notes",
        tags = "core-album-notes",
        description = "Core service for providing album notes."
)
@Profile({"default", "core-album-notes"})
public class AlbumNotesCoreController {

    private static final String ENTITY_NAME = "albumNotes";
    private final Logger log = LoggerFactory.getLogger(AlbumNotesCoreController.class);
    private final AlbumNotesCoreService albumNotesCoreService;

    public AlbumNotesCoreController(AlbumNotesCoreService albumNotesCoreService) {
        this.albumNotesCoreService = albumNotesCoreService;
    }

    /**
     * GET  /album-notes : get albumNotes entities.
     *
     * @return the ResponseEntity with status 200 (OK) and with body the list of albumNotesDTO
     */
    @GetMapping("/album-notes")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get albumNotes",
            notes = "This method is for obtaining album notes"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = AlbumNotesDTO[].class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<List<AlbumNotesDTO>>> getAlbumNotes(
            @Valid AlbumNotesProjections projections,
            @Valid AlbumIdFilter albumIdFilter) {

        log.debug("REST request to get AlbumNotes, query: {}",
                QueryParametersHelper.toDebugString(projections, albumIdFilter));
        List<AlbumNotesDTO> albumNotesDTOs = albumNotesCoreService.findAll(albumIdFilter, projections);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(albumNotesDTOs));
    }

    /**
     * POST  /album-notes : Creates a new album notes.
     *
     * @param createDTO AlbumNotesCreateDTO to create
     * @return the ResponseEntity with status 201 (CREATED) and with body the AlbumNotesDTO
     */
    @PostMapping("/album-notes")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(
            value = "Create a new album notes",
            notes = "This method is for creating an album notes"
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "CREATED", response = AlbumNotesDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<AlbumNotesDTO>> createAlbumNotes(
            @Valid @RequestBody AlbumNotesCreateDTO createDTO) {

        log.debug("REST request to create Album notes : {}", createDTO);
        AlbumNotesDTO result = albumNotesCoreService.create(createDTO);
        return ResponseEntity.created(URI.create("")).body(ApiResponseBodyDeprecated.of(result));
    }
}
