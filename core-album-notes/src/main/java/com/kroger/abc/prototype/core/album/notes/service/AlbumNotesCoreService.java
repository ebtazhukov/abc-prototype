package com.kroger.abc.prototype.core.album.notes.service;

import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesDTO;
import com.kroger.abc.prototype.common.rest.param.filter.AlbumIdFilter;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;

/**
 * Service Interface for managing AlbumNotes.
 */
public interface AlbumNotesCoreService {


    /**
     * Get the albumNotes
     *
     * @param albumIdFilter
     * @param projections
     * @return the entity
     */
    List<AlbumNotesDTO> findAll(AlbumIdFilter albumIdFilter, Projections projections);

    AlbumNotesDTO create(AlbumNotesCreateDTO createDTO);
}