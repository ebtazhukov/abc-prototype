package com.kroger.abc.prototype.core.album.notes.service.impl;

import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesDTO;
import com.kroger.abc.prototype.common.rest.param.filter.AlbumIdFilter;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.util.projection.Projection;
import com.kroger.abc.prototype.core.album.notes.domain.AlbumNotes;
import com.kroger.abc.prototype.core.album.notes.repository.AlbumNotesRepository;
import com.kroger.abc.prototype.core.album.notes.service.AlbumNotesCoreService;
import com.kroger.abc.prototype.core.album.notes.service.mapper.AlbumNotesMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.kroger.abc.prototype.common.rest.util.projection.Projection.PROJECTION_ALBUM_NOTES_KEY;
import static com.kroger.abc.prototype.common.rest.util.projection.Projection.Shape.FULL;

/**
 * Service Implementation for managing AlbumNotes.
 */
@Service
public class AlbumNotesCoreServiceImpl implements AlbumNotesCoreService {

    private final Logger log = LoggerFactory.getLogger(AlbumNotesCoreServiceImpl.class);

    private final AlbumNotesRepository albumNotesRepository;

    private final AlbumNotesMapper albumNotesMapper;

    public AlbumNotesCoreServiceImpl(AlbumNotesRepository albumNotesRepository, AlbumNotesMapper albumNotesMapper) {
        this.albumNotesRepository = albumNotesRepository;
        this.albumNotesMapper = albumNotesMapper;
    }

    /**
     * Get one albumNotes.
     *
     * @param albumIdFilter
     * @param projections
     * @return the entity
     */
    @Override
    public List<AlbumNotesDTO> findAll(AlbumIdFilter albumIdFilter, Projections projections) {
        log.debug("Request to get AlbumNotes : {}", albumIdFilter.getIds());
        Projection.Shape shape = projections.extract().getByKey(PROJECTION_ALBUM_NOTES_KEY);

        List<AlbumNotes> albumNotes = new ArrayList<>();
        if (albumIdFilter.getIds() != null) {
            albumNotes = albumIdFilter.getIds().stream()
                    .map(albumNotesRepository::findByAlbumId)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
        return FULL.equals(shape) ? albumNotesMapper.toDto(albumNotes) : albumNotesMapper.toDtoCompact(albumNotes);
    }

    @Override
    public AlbumNotesDTO create(AlbumNotesCreateDTO createDTO) {
        log.debug("Request to create AlbumNotes : {}", createDTO);
        AlbumNotes albumNotes = albumNotesRepository.create(createDTO);
        return albumNotesMapper.toDto(albumNotes);
    }
}
