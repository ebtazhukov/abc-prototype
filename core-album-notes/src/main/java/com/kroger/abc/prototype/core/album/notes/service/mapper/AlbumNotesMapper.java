package com.kroger.abc.prototype.core.album.notes.service.mapper;

import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesDTO;
import com.kroger.abc.prototype.common.rest.mapper.EntityMapper;
import com.kroger.abc.prototype.core.album.notes.domain.AlbumNotes;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * Mapper for the entity AlbumNotes and its DTO AlbumNotesDTO.
 */
@Component
public class AlbumNotesMapper implements EntityMapper<AlbumNotesDTO, AlbumNotes> {


    @Override
    public AlbumNotes toEntity(AlbumNotesDTO dto) {
        return dto == null ? null :
                new AlbumNotes(dto.getId(), dto.getAlbumId(), dto.getNotes());
    }

    @Override
    public AlbumNotesDTO toDto(AlbumNotes entity) {
        return entity == null ? null :
                new AlbumNotesDTO(entity.getId(), entity.getAlbumId(), entity.getNotes());
    }

    @Override
    public List<AlbumNotes> toEntity(List<AlbumNotesDTO> dtoList) {
        return dtoList == null ? null :
                dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<AlbumNotesDTO> toDto(List<AlbumNotes> entityList) {
        return entityList == null ? null :
                entityList.stream().map(this::toDto).collect(Collectors.toList());
    }

    public AlbumNotesDTO toDtoCompact(AlbumNotes entity) {
        return entity == null ? null :
                new AlbumNotesDTO(entity.getId(), entity.getAlbumId(), entity.getNotes().substring(0, 13));
    }

    public List<AlbumNotesDTO> toDtoCompact(List<AlbumNotes> entityList) {
        return entityList == null ? null :
                entityList.stream().map(this::toDtoCompact).collect(Collectors.toList());
    }
}
