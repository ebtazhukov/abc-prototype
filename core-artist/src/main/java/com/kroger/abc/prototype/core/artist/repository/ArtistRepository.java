package com.kroger.abc.prototype.core.artist.repository;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.kroger.abc.prototype.core.artist.domain.Artist;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.springframework.stereotype.Repository;

/**
 * Cassandra repository for the Artist entity.
 */
@Repository
public class ArtistRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Artist> mapper;

    private PreparedStatement findAllStmt;


    public ArtistRepository(Session session, Validator validator) {
        this.session = session;
        this.mapper = new MappingManager(session).mapper(Artist.class);
        this.findAllStmt = session.prepare("SELECT * FROM artist");
        this.validator = validator;
    }

    public List<Artist> findAll() {
        List<Artist> artistList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
                row -> {
                    Artist artist = new Artist();
                    artist.setId(row.getUUID("id"));
                    artist.setCreatedAt(row.get("createdAt", Instant.class));
                    artist.setModifiedAt(row.get("modifiedAt", Instant.class));
                    artist.setName(row.getString("name"));
                    return artist;
                }
        ).forEach(artistList::add);
        return artistList;
    }

    public Artist findOne(UUID id) {
        return mapper.get(id);
    }

    public Artist create(String name) {
        Artist artist = new Artist();
        artist.setId(UUID.randomUUID());
        artist.setCreatedAt(Instant.now());
        artist.setModifiedAt(Instant.now());
        artist.setName(name);

        Set<ConstraintViolation<Artist>> violations = validator.validate(artist);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(artist);
        return artist;
    }
}
