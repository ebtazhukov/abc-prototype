package com.kroger.abc.prototype.core.artist.rest;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;
import com.kroger.abc.prototype.common.rest.param.filter.ArtistFilters;
import com.kroger.abc.prototype.common.rest.param.projection.ArtistProjections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.ResponseUtil;
import com.kroger.abc.prototype.core.artist.service.ArtistCoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Artist.
 */
@RestController
@RequestMapping("/core")
@Api(
        value = "core-artist",
        tags = "core-artist",
        description = "Core service for providing artist."
)
@Profile({"default", "core-artists"})
public class ArtistCoreController {

    private static final String ENTITY_NAME = "artists";
    private final Logger log = LoggerFactory.getLogger(ArtistCoreController.class);
    private final ArtistCoreService artistCoreService;

    public ArtistCoreController(ArtistCoreService artistCoreService) {
        this.artistCoreService = artistCoreService;
    }

    /**
     * GET  /artists/:id : get artist for specific id.
     *
     * @param id the id of the artist for which artistDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the artistDTO, or with status 404 (Not Found)
     */
    @GetMapping("/artists/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get artist for specific id",
            notes = "This method is for obtaining artist for specific id"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = ArtistDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<ArtistDTO>> getArtist(
            @ApiParam(value = "**Artist ID**.", required = true) @PathVariable String id,
            @Valid ArtistProjections projections) {

        log.debug("REST request to get Artist : {}, query: {}",
                QueryParametersHelper.toDebugString(projections));
        ArtistDTO artistDTO = artistCoreService.findOne(id, projections);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(artistDTO));
    }

    /**
     * GET  /artists : get all artists.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of artist in body
     */
    @GetMapping("/artists")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get artist for all songs",
            notes = "This method is for obtaining artist for all songs"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = ArtistDTO[].class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<List<ArtistDTO>>> getAllArtists(
            @Valid ArtistProjections projections,
            @Valid ArtistFilters artistFilters) {

        log.debug("REST request to get all Artist, query: {}",
                QueryParametersHelper.toDebugString(projections, artistFilters));
        List<ArtistDTO> artistDTOS = artistCoreService.findAll(projections, artistFilters);
        return ResponseEntity.ok().body(ApiResponseBodyDeprecated.of(artistDTOS));
    }

    /**
     * POST  /artists/get-or-create : Return all artists, creates any non-existed.
     *
     * @param createDTO List<ArtistCreateDTO> list of artists to return (create if doesn't exist)
     * @return the ResponseEntity with status 200 (OK) and with body the List<ArtistDTO>
     */
    @PostMapping("/artists/get-or-create")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get or create artists in bulk",
            notes = "This method is for returning all requested artists, matching by name." +
                    " Any non-existed will be created."
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = ArtistDTO[].class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<List<ArtistDTO>>> getOrCreateArtistsInBulk(
            @Valid @RequestBody List<ArtistCreateDTO> createDTO) {

        log.debug("REST request to get or create Artists : {}", createDTO);
        List<ArtistDTO> artistDTOS = artistCoreService.getOrCreateInBulk(createDTO);
        return ResponseEntity.ok().body(ApiResponseBodyDeprecated.of(artistDTOS));
    }
}
