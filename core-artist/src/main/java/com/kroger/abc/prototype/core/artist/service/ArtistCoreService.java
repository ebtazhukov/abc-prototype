package com.kroger.abc.prototype.core.artist.service;

import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;
import com.kroger.abc.prototype.common.rest.param.filter.ArtistFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;

/**
 * Service Interface for managing Artist.
 */
public interface ArtistCoreService {

    /**
     * Get all the artist.
     *
     * @param projections
     * @param artistFilters
     * @return the list of entities
     */
    List<ArtistDTO> findAll(Projections projections, ArtistFilters artistFilters);

    /**
     * Get the "id" artist.
     *
     * @param id          the id of the entity
     * @param projections
     * @return the entity
     */
    ArtistDTO findOne(String id, Projections projections);

    List<ArtistDTO> getOrCreateInBulk(List<ArtistCreateDTO> createDTO);
}
