package com.kroger.abc.prototype.core.artist.service.impl;

import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;
import com.kroger.abc.prototype.common.rest.param.filter.ArtistFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.util.projection.Projection;
import com.kroger.abc.prototype.core.artist.domain.Artist;
import com.kroger.abc.prototype.core.artist.repository.ArtistRepository;
import com.kroger.abc.prototype.core.artist.service.ArtistCoreService;
import com.kroger.abc.prototype.core.artist.service.mapper.ArtistMapper;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.kroger.abc.prototype.common.rest.util.projection.Projection.PROJECTION_ARTIST_KEY;
import static com.kroger.abc.prototype.common.rest.util.projection.Projection.Shape.FULL;

/**
 * Service Implementation for managing Artist.
 */
@Service
public class ArtistCoreServiceImpl implements ArtistCoreService {

    private final Logger log = LoggerFactory.getLogger(ArtistCoreServiceImpl.class);

    private final ArtistRepository artistRepository;

    private final ArtistMapper artistMapper;

    public ArtistCoreServiceImpl(ArtistRepository artistRepository, ArtistMapper artistMapper) {
        this.artistRepository = artistRepository;
        this.artistMapper = artistMapper;
    }

    /**
     * Get all the artists.
     *
     * @param projections
     * @param artistFilters
     * @return the list of entities
     */
    @Override
    public List<ArtistDTO> findAll(Projections projections, ArtistFilters artistFilters) {
        log.debug("Request to get all Artists");
        Projection.Shape shape = projections.extract().getByKey(PROJECTION_ARTIST_KEY);

        List<Artist> artists = artistRepository.findAll();

        //emulate real DB queries for simplicity
        List<UUID> filterIds = artistFilters.getIds();
        String nameFilter = artistFilters.getName();

        if (CollectionUtils.isNotEmpty(filterIds)) {
            artists = artists.stream()
                    .filter(artist -> filterIds.contains(artist.getId()))
                    .collect(Collectors.toList());
        } else {
            artists = artists.stream()
                    .filter(artist -> StringUtils.isBlank(nameFilter) ||
                            StringUtils.containsIgnoreCase(artist.getName(), nameFilter))
                    .collect(Collectors.toList());
        }
        return artists.stream()
                .map(FULL.equals(shape) ? artistMapper::toDto : artistMapper::toDtoCompact)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one artist by id.
     *
     * @param id          the id of the entity
     * @param projections
     * @return the entity
     */
    @Override
    public ArtistDTO findOne(String id, Projections projections) {
        log.debug("Request to get Artist : {}", id);
        Projection.Shape shape = projections.extract().getByKey(PROJECTION_ARTIST_KEY);

        Artist artist = artistRepository.findOne(UUID.fromString(id));
        return FULL.equals(shape) ? artistMapper.toDto(artist) : artistMapper.toDtoCompact(artist);
    }

    @Override
    public List<ArtistDTO> getOrCreateInBulk(List<ArtistCreateDTO> createDTO) {
        log.debug("Request to get or create Artists in bulk : {}", createDTO);
        List<Artist> result = new ArrayList<>();

        Set<String> requestedNames = createDTO.stream().map(ArtistCreateDTO::getName).collect(Collectors.toSet());

        //find all existed artists and remove it names from requested set
        List<Artist> existed = artistRepository.findAll();
        existed.forEach(artist -> {
            if (requestedNames.contains(artist.getName())) {
                result.add(artist);
                requestedNames.remove(artist.getName());
            }
        });

        //creates all remaining artists
        if (CollectionUtils.isNotEmpty(requestedNames)) {
            requestedNames.forEach(name -> result.add(artistRepository.create(name)));
        }

        return artistMapper.toDtoCompact(result);
    }
}
