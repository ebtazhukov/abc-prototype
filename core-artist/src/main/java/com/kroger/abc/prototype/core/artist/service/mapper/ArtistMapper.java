package com.kroger.abc.prototype.core.artist.service.mapper;

import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;
import com.kroger.abc.prototype.common.rest.mapper.EntityMapper;
import com.kroger.abc.prototype.core.artist.domain.Artist;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * Mapper for the entity Artist and its DTO ArtistDTO.
 */
@Component
public class ArtistMapper implements EntityMapper<ArtistDTO, Artist> {


    @Override
    public Artist toEntity(ArtistDTO dto) {
        return dto == null ? null : new Artist(dto.getId(), dto.getCreatedAt(), dto.getModifiedAt(), dto.getName());
    }

    @Override
    public ArtistDTO toDto(Artist entity) {
        return entity == null ? null :
                new ArtistDTO(entity.getId(), entity.getCreatedAt(), entity.getModifiedAt(), entity.getName());
    }

    @Override
    public List<Artist> toEntity(List<ArtistDTO> dtoList) {
        return dtoList == null ? null : dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<ArtistDTO> toDto(List<Artist> entityList) {
        return entityList == null ? null : entityList.stream().map(this::toDto).collect(Collectors.toList());
    }

    public ArtistDTO toDtoCompact(Artist entity) {
        return entity == null ? null : new ArtistDTO(entity.getId(), null, null, entity.getName());
    }

    public List<ArtistDTO> toDtoCompact(List<Artist> entityList) {
        return entityList == null ? null : entityList.stream().map(this::toDtoCompact).collect(Collectors.toList());
    }
}
