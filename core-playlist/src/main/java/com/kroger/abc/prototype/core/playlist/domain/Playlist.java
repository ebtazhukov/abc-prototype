package com.kroger.abc.prototype.core.playlist.domain;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "playlist")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Playlist implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @PartitionKey
    private UUID id;

    private List<UUID> songIds;

    private String createdBy;

    private Instant createdAt;

    private Instant modifiedAt;

    private String title;

}
