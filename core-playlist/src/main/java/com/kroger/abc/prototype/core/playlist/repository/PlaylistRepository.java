package com.kroger.abc.prototype.core.playlist.repository;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.kroger.abc.prototype.common.rest.util.SecurityContextHelper;
import com.kroger.abc.prototype.core.playlist.domain.Playlist;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

/**
 * Cassandra repository for the Playlist entity.
 */
@Repository
public class PlaylistRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Playlist> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public PlaylistRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(Playlist.class);
        this.findAllStmt = session.prepare("SELECT * FROM playlist");
        this.truncateStmt = session.prepare("TRUNCATE playlist");
    }

    public List<Playlist> findAll() {
        return findAll(SecurityContextHelper.getUserName());
    }

    public List<Playlist> findAll(String username) {
        List<Playlist> playlists = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream()
                .map(
                        row -> {
                            Playlist playlist = new Playlist();
                            playlist.setId(row.getUUID("id"));
                            playlist.setSongIds(row.getList("songIds", UUID.class));
                            playlist.setCreatedBy(row.getString("createdBy"));
                            playlist.setCreatedAt(row.get("createdAt", Instant.class));
                            playlist.setModifiedAt(row.get("modifiedAt", Instant.class));
                            playlist.setTitle(row.getString("title"));
                            return playlist;
                        }
                )
                .filter(pl -> StringUtils.equals(pl.getCreatedBy(), username))
                .forEach(playlists::add);
        return playlists;
    }

    public Playlist findOne(UUID id) {
        Playlist existed = mapper.get(id);
        return (existed != null && StringUtils.equals(existed.getCreatedBy(), SecurityContextHelper.getUserName())) ? existed : null;
    }

    public Playlist save(Playlist playlist) {
        if (playlist.getId() == null) {
            playlist.setId(UUID.randomUUID());
            playlist.setCreatedAt(Instant.now());
            playlist.setCreatedBy(SecurityContextHelper.getUserName());
        } else {
            Playlist existed = findOne(playlist.getId());
            if (existed != null) {
                if (playlist.getTitle() == null) {
                    playlist.setTitle(existed.getTitle());
                }
                if (playlist.getSongIds() == null) {
                    playlist.setSongIds(existed.getSongIds());
                }
                playlist.setCreatedAt(existed.getCreatedAt());
                playlist.setCreatedBy(existed.getCreatedBy());
            } else {
                return null;
            }
        }
        playlist.setModifiedAt(Instant.now());

        Set<ConstraintViolation<Playlist>> violations = validator.validate(playlist);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(playlist);
        return playlist;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteWithCheck(UUID id) {
        if (findOne(id) != null) {
            delete(id);
        }
    }
}
