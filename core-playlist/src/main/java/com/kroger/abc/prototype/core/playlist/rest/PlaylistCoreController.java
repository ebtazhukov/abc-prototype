package com.kroger.abc.prototype.core.playlist.rest;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistUpdateDTO;
import com.kroger.abc.prototype.common.rest.param.filter.PlaylistFilters;
import com.kroger.abc.prototype.common.rest.param.projection.PlaylistProjections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.ResponseUtil;
import com.kroger.abc.prototype.core.playlist.service.PlaylistCoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Playlist.
 */
@RestController
@RequestMapping("/core")
@Api(
        value = "core-playlists",
        tags = "core-playlists",
        description = "Core service for providing playlists."
)
@Profile({"default", "core-playlists"})
public class PlaylistCoreController {

    private static final String ENTITY_NAME = "playlist";
    private final Logger log = LoggerFactory.getLogger(PlaylistCoreController.class);
    private final PlaylistCoreService playlistCoreService;

    public PlaylistCoreController(PlaylistCoreService playlistCoreService) {
        this.playlistCoreService = playlistCoreService;
    }

    /**
     * GET  /playlists/:id : get playlist for specific song.
     *
     * @param id the id of the song for which playlistDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the playlistDTO, or with status 404 (Not Found)
     */
    @GetMapping("/playlists/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get playlist for specific song",
            notes = "This method is for obtaining song details for specific song"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = PlaylistDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<PlaylistDTO>> getPlaylist(
            @ApiParam(value = "**Playlist ID**.", required = true) @PathVariable String id,
            @Valid PlaylistProjections projections) {

        log.debug("REST request to get Playlist : {}, query: {}",
                QueryParametersHelper.toDebugString(projections));
        PlaylistDTO playlistDTO = playlistCoreService.findOne(id, projections);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(playlistDTO));
    }

    /**
     * GET  /playlists : get all playlists.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of playlist in body
     */
    @GetMapping("/playlists")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get playlist for all songs",
            notes = "This method is for obtaining song details for all songs"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = PlaylistDTO[].class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<List<PlaylistDTO>>> getAllPlaylist(
            @Valid PlaylistProjections projections,
            @Valid PlaylistFilters playlistFilters) {

        log.debug("REST request to get all Playlist, query: {}",
                QueryParametersHelper.toDebugString(projections, playlistFilters));
        List<PlaylistDTO> playlistDTOS = playlistCoreService.findAll(projections, playlistFilters);
        return ResponseEntity.ok(ApiResponseBodyDeprecated.of(playlistDTOS));
    }

    /**
     * POST  /playlists : Creates a new playlist.
     *
     * @param playlistDTO playlistDTO to create
     * @return the ResponseEntity with status 201 (CREATED) and with body the playlistDTO
     */
    @PostMapping("/playlists")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(
            value = "Create the playlist",
            notes = "This method is for creating playlist"
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "CREATED", response = PlaylistDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<PlaylistDTO>> createPlaylist(
            @Valid @RequestBody PlaylistCreateDTO playlistDTO) {

        log.debug("REST request to create Playlist : {}", playlistDTO);
        PlaylistDTO result = playlistCoreService.create(playlistDTO);
        return ResponseEntity.created(URI.create("")).body(ApiResponseBodyDeprecated.of(result));
    }

    /**
     * PUT  /playlists : Updates an existing playlist.
     *
     * @param playlistDTO the playlistDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated playlistDTO, or with status 404 (Not Found)
     */
    @PutMapping("/playlists")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Update the song details",
            notes = "This method is for updating song details"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = PlaylistDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<PlaylistDTO>> updateSong(@Valid @RequestBody PlaylistUpdateDTO playlistDTO) {
        log.debug("REST request to update song details: {}", playlistDTO);
        PlaylistDTO result = playlistCoreService.save(playlistDTO);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(result));
    }

    /**
     * DELETE  /playlists/:id : Deletes an existing playlist.
     *
     * @param id the id of the playlist to delete
     * @return the ResponseEntity with status 204 (NO_CONTENT), or with status 404 (Not Found)
     */
    @DeleteMapping("/playlists/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(
            value = "Delete the playlist",
            notes = "This method is for deleting playlist"
    )
    @ApiResponses({
            @ApiResponse(code = 204, message = "NO_CONTENT")
    })
    public ResponseEntity<Void> deletePlaylist(
            @ApiParam(value = "**Playlist ID**.", required = true) @PathVariable String id) {
        log.debug("REST request to delete Playlist : {}", id);
        playlistCoreService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
