package com.kroger.abc.prototype.core.playlist.service;

import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistUpdateDTO;
import com.kroger.abc.prototype.common.rest.param.filter.PlaylistFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;

/**
 * Service Interface for managing Playlist.
 */
public interface PlaylistCoreService {

    /**
     * Create the playlist.
     *
     * @param playlistDTO the entity to save
     * @return saved entity
     */
    PlaylistDTO create(PlaylistCreateDTO playlistDTO);

    /**
     * Save a playlist.
     *
     * @param playlistDTO the entity to save
     * @return the persisted entity
     */
    PlaylistDTO save(PlaylistUpdateDTO playlistDTO);

    /**
     * Get all the playlist.
     *
     * @param projections
     * @param playlistFilters
     * @return the list of entities
     */
    List<PlaylistDTO> findAll(Projections projections, PlaylistFilters playlistFilters);

    /**
     * Get the "id" playlist.
     *
     * @param id          the id of the entity
     * @param projections
     * @return the entity
     */
    PlaylistDTO findOne(String id, Projections projections);

    /**
     * Delete the "id" playlist.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
