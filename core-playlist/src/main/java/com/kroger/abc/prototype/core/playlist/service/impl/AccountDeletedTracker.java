package com.kroger.abc.prototype.core.playlist.service.impl;

import com.kroger.abc.prototype.common.client.message.AccountMessageDTO;
import com.kroger.abc.prototype.common.client.message.AccountMessagesClient;
import com.kroger.abc.prototype.core.playlist.domain.Playlist;
import com.kroger.abc.prototype.core.playlist.repository.PlaylistRepository;
import java.util.List;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class AccountDeletedTracker {

    private static final long MESSAGES_CHECK_DELAY = 1000 * 60;

    private final PlaylistRepository playlistRepository;

    private final AccountMessagesClient messagesClient;

    public AccountDeletedTracker(PlaylistRepository playlistRepository, AccountMessagesClient messagesClient) {
        this.playlistRepository = playlistRepository;
        this.messagesClient = messagesClient;
    }

    @Scheduled(fixedDelay = MESSAGES_CHECK_DELAY)
    public void checkAccountDeletedMessages() {
        List<AccountMessageDTO> messages = messagesClient.findAll();
        messages.forEach(message -> {
            List<Playlist> playlists = playlistRepository.findAll(message.getBody());
            playlists.forEach(playlist -> playlistRepository.delete(playlist.getId()));
            messagesClient.delete(message.getId());
        });
    }
}
