package com.kroger.abc.prototype.core.playlist.service.impl;

import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistUpdateDTO;
import com.kroger.abc.prototype.common.rest.param.filter.PlaylistFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.util.projection.Projection;
import com.kroger.abc.prototype.core.playlist.domain.Playlist;
import com.kroger.abc.prototype.core.playlist.repository.PlaylistRepository;
import com.kroger.abc.prototype.core.playlist.service.PlaylistCoreService;
import com.kroger.abc.prototype.core.playlist.service.mapper.PlaylistMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.kroger.abc.prototype.common.rest.util.projection.Projection.PROJECTION_PLAYLIST_KEY;
import static com.kroger.abc.prototype.common.rest.util.projection.Projection.Shape.FULL;

/**
 * Service Implementation for managing Playlist.
 */
@Service
public class PlaylistCoreServiceImpl implements PlaylistCoreService {

    private final Logger log = LoggerFactory.getLogger(PlaylistCoreServiceImpl.class);

    private final PlaylistRepository playlistRepository;

    private final PlaylistMapper playlistMapper;

    public PlaylistCoreServiceImpl(PlaylistRepository playlistRepository, PlaylistMapper playlistMapper) {
        this.playlistRepository = playlistRepository;
        this.playlistMapper = playlistMapper;
    }

    /**
     * Create a playlist.
     *
     * @param playlistDTO the entity to create
     * @return the persisted entity
     */
    @Override
    public PlaylistDTO create(PlaylistCreateDTO playlistDTO) {
        log.debug("Request to save Playlist : {}", playlistDTO);
        Playlist playlist = playlistMapper.toEntity(playlistDTO);
        playlist = playlistRepository.save(playlist);
        return playlistMapper.toDto(playlist);
    }

    /**
     * Save a playlist.
     *
     * @param playlistDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlaylistDTO save(PlaylistUpdateDTO playlistDTO) {
        log.debug("Request to save Playlist : {}", playlistDTO);
        Playlist playlist = playlistMapper.toEntity(playlistDTO);
        playlist = playlistRepository.save(playlist);
        return playlistMapper.toDto(playlist);
    }

    /**
     * Get all the playlist.
     *
     * @param projections
     * @param playlistFilters
     * @return the list of entities
     */
    @Override
    public List<PlaylistDTO> findAll(Projections projections, PlaylistFilters playlistFilters) {
        log.debug("Request to get all Playlist");
        Projection.Shape shape = projections.extract().getByKey(PROJECTION_PLAYLIST_KEY);

        List<Playlist> playlists = playlistRepository.findAll();

        //emulate real DB queries for simplicity
        String titleFilter = playlistFilters.getTitle();
        List<UUID> filterIds = playlistFilters.getIds();

        if (CollectionUtils.isNotEmpty(filterIds)) {
            playlists = playlists.stream()
                    .filter(playlist -> filterIds.contains(playlist.getId()))
                    .collect(Collectors.toList());
        } else {
            playlists = playlists.stream()
                    .filter(playlist -> StringUtils.isBlank(titleFilter) ||
                            StringUtils.containsIgnoreCase(playlist.getTitle(), titleFilter))
                    .collect(Collectors.toList());
        }
        return playlists.stream()
                .map(FULL.equals(shape) ? playlistMapper::toDto : playlistMapper::toDtoCompact)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one playlist by id.
     *
     * @param id          the id of the entity
     * @param projections
     * @return the entity
     */
    @Override
    public PlaylistDTO findOne(String id, Projections projections) {
        log.debug("Request to get Playlist : {}", id);
        Projection.Shape shape = projections.extract().getByKey(PROJECTION_PLAYLIST_KEY);

        Playlist playlist = playlistRepository.findOne(UUID.fromString(id));
        return FULL.equals(shape) ? playlistMapper.toDto(playlist) : playlistMapper.toDtoCompact(playlist);
    }

    /**
     * Delete the playlist by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Playlist : {}", id);
        playlistRepository.deleteWithCheck(UUID.fromString(id));
    }
}
