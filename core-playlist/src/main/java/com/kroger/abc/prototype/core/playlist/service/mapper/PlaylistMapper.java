package com.kroger.abc.prototype.core.playlist.service.mapper;

import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistRelationships;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistUpdateDTO;
import com.kroger.abc.prototype.common.rest.mapper.EntityMapper;
import com.kroger.abc.prototype.core.playlist.domain.Playlist;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * Mapper for the entity Playlist and its DTO PlaylistDTO.
 */
@Component
public class PlaylistMapper implements EntityMapper<PlaylistDTO, Playlist> {


    @Override
    public Playlist toEntity(PlaylistDTO dto) {
        return dto == null ? null :
                new Playlist(dto.getId(), PlaylistRelationships.extractSongs(dto.getRelationships()),
                        null, dto.getCreatedAt(), dto.getModifiedAt(), dto.getTitle());
    }

    public Playlist toEntity(PlaylistCreateDTO dto) {
        return dto == null ? null :
                new Playlist(null, PlaylistRelationships.extractSongs(dto.getRelationships()),
                        null, null, null, dto.getTitle());
    }

    public Playlist toEntity(PlaylistUpdateDTO dto) {
        return dto == null ? null :
                new Playlist(dto.getId(), PlaylistRelationships.extractSongs(dto.getRelationships()),
                        null, null, null, dto.getTitle());
    }

    @Override
    public PlaylistDTO toDto(Playlist entity) {
        return entity == null ? null :
                new PlaylistDTO(entity.getId(), PlaylistRelationships.build(entity.getSongIds()),
                        entity.getCreatedAt(), entity.getModifiedAt(), entity.getTitle());
    }

    @Override
    public List<Playlist> toEntity(List<PlaylistDTO> dtoList) {
        return dtoList == null ? null : dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<PlaylistDTO> toDto(List<Playlist> entityList) {
        return entityList == null ? null : entityList.stream().map(this::toDto).collect(Collectors.toList());
    }

    public PlaylistDTO toDtoCompact(Playlist entity) {
        return entity == null ? null :
                new PlaylistDTO(entity.getId(), null, null, null, entity.getTitle());
    }

    public List<PlaylistDTO> toDtoCompact(List<Playlist> entityList) {
        return entityList == null ? null : entityList.stream().map(this::toDtoCompact).collect(Collectors.toList());
    }
}
