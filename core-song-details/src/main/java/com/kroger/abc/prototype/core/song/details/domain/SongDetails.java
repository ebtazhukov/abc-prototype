package com.kroger.abc.prototype.core.song.details.domain;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "songDetails")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SongDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @PartitionKey
    private UUID id;

    private Instant createdAt;

    private Instant modifiedAt;

    private String title;

    private UUID artistId;

    private Long duration;

    private Integer bitrate;

    private Boolean subscribeOnly;
}
