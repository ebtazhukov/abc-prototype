package com.kroger.abc.prototype.core.song.details.repository;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.kroger.abc.prototype.core.song.details.domain.SongDetails;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.springframework.stereotype.Repository;

/**
 * Cassandra repository for the SongDetails entity.
 */
@Repository
public class SongDetailsRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<SongDetails> mapper;

    private PreparedStatement findAllStmt;

    public SongDetailsRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(SongDetails.class);
        this.findAllStmt = session.prepare("SELECT * FROM songDetails");
    }

    public List<SongDetails> findAll() {
        List<SongDetails> songDetailsList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
                row -> {
                    SongDetails songDetails = new SongDetails();
                    songDetails.setId(row.getUUID("id"));
                    songDetails.setCreatedAt(row.get("createdAt", Instant.class));
                    songDetails.setModifiedAt(row.get("modifiedAt", Instant.class));
                    songDetails.setTitle(row.getString("title"));
                    songDetails.setArtistId(row.getUUID("artistId"));
                    songDetails.setDuration(row.getLong("duration"));
                    songDetails.setBitrate(row.getInt("bitrate"));
                    songDetails.setSubscribeOnly(row.getBool("subscribeOnly"));
                    return songDetails;
                }
        ).forEach(songDetailsList::add);
        return songDetailsList;
    }

    public SongDetails findOne(UUID id) {
        return mapper.get(id);
    }

    public SongDetails save(SongDetails songDetails) {
        if (songDetails.getId() == null) {
            songDetails.setId(UUID.randomUUID());
            songDetails.setCreatedAt(Instant.now());
        } else {
            SongDetails existed = findOne(songDetails.getId());
            if (existed != null) {
                if (songDetails.getTitle() != null) {
                    existed.setTitle(songDetails.getTitle());
                }
                songDetails = existed;
            } else {
                return null;
            }
        }
        Set<ConstraintViolation<SongDetails>> violations = validator.validate(songDetails);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        songDetails.setModifiedAt(Instant.now());
        mapper.save(songDetails);
        return songDetails;
    }

}
