package com.kroger.abc.prototype.core.song.details.rest;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.projection.SongDetailsProjections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.ResponseUtil;
import com.kroger.abc.prototype.core.song.details.service.SongDetailsCoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing SongDetails.
 */
@RestController
@RequestMapping("/core")
@Api(
        value = "core-song-details",
        tags = "core-song-details",
        description = "Core service for providing song details."
)
@Profile({"default", "core-song-details"})
public class SongDetailsCoreController {

    private static final String ENTITY_NAME = "songDetails";
    private final Logger log = LoggerFactory.getLogger(SongDetailsCoreController.class);
    private final SongDetailsCoreService songDetailsCoreService;

    public SongDetailsCoreController(SongDetailsCoreService songDetailsCoreService) {
        this.songDetailsCoreService = songDetailsCoreService;
    }

    /**
     * GET  /song-details/:id : get songDetails for specific song.
     *
     * @param id the id of the song for which songDetailsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the songDetailsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/song-details/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get songDetails for specific song",
            notes = "This method is for obtaining song details for specific song"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = SongDetailsDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<SongDetailsDTO>> getSongDetails(
            @ApiParam(value = "**Song ID**.", required = true) @PathVariable String id,
            @Valid SongDetailsProjections projections) {

        log.debug("REST request to get SongDetails : {}, query: {}",
                QueryParametersHelper.toDebugString(projections));
        SongDetailsDTO songDetailsDTO = songDetailsCoreService.findOne(id, projections);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(songDetailsDTO));
    }

    /**
     * GET  /song-details : get songDetails for all songs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of songDetails in body
     */
    @GetMapping("/song-details")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get songDetails for all songs",
            notes = "This method is for obtaining song details for all songs"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = SongDetailsDTO[].class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<List<SongDetailsDTO>>> getAllSongDetails(
            @Valid SongDetailsProjections projections,
            @Valid SongFilters songFilters) {

        log.debug("REST request to get all SongDetails, query: {}",
                QueryParametersHelper.toDebugString(projections, songFilters));
        List<SongDetailsDTO> songDetailsDTOS = songDetailsCoreService.findAll(projections, songFilters);
        return ResponseEntity.ok().body(ApiResponseBodyDeprecated.of(songDetailsDTOS));
    }

    /**
     * PUT  /song-details : Updates an existing songDetails.
     *
     * @param songDetailsDTO the songDetailsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated songDetailsDTO, or with status 404 (Not Found)
     */
    @PutMapping("/song-details")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Update the song details",
            notes = "This method is for updating song details"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = SongDetailsDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<SongDetailsDTO>> updateSong(@Valid @RequestBody SongDetailsDTO songDetailsDTO) {
        log.debug("REST request to update song details: {}", songDetailsDTO);
        SongDetailsDTO result = songDetailsCoreService.save(songDetailsDTO);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(result));
    }

    /**
     * POST  /song-details : Creates songDetails.
     *
     * @param songDetailsDTO the songDetailsDTO to create
     * @return the ResponseEntity with status 201 (CREATED) and with body of created songDetailsDTO
     */
    @PostMapping("/song-details")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Create the song details",
            notes = "This method is for creating song details"
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "CREATED", response = SongDetailsDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<SongDetailsDTO>> createSong(@Valid @RequestBody SongDetailsCreateDTO songDetailsDTO) {
        log.debug("REST request to create song details: {}", songDetailsDTO);
        SongDetailsDTO result = songDetailsCoreService.create(songDetailsDTO);
        return ResponseEntity.created(URI.create("")).body(ApiResponseBodyDeprecated.of(result));
    }

}
