package com.kroger.abc.prototype.core.song.details.service;

import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;

/**
 * Service Interface for managing SongDetails.
 */
public interface SongDetailsCoreService {

    /**
     * Save a songDetails.
     *
     * @param songDetailsDTO the entity to save
     * @return the persisted entity
     */
    SongDetailsDTO save(SongDetailsDTO songDetailsDTO);

    /**
     * Get all the songDetails.
     *
     * @param projections
     * @param songFilters
     * @return the list of entities
     */
    List<SongDetailsDTO> findAll(Projections projections, SongFilters songFilters);

    /**
     * Get the "id" songDetails.
     *
     * @param id          the id of the entity
     * @param projections
     * @return the entity
     */
    SongDetailsDTO findOne(String id, Projections projections);

    SongDetailsDTO create(SongDetailsCreateDTO songDetailsDTO);
}
