package com.kroger.abc.prototype.core.song.details.service.impl;

import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongRelationships;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.util.projection.Projection;
import com.kroger.abc.prototype.core.song.details.domain.SongDetails;
import com.kroger.abc.prototype.core.song.details.repository.SongDetailsRepository;
import com.kroger.abc.prototype.core.song.details.service.SongDetailsCoreService;
import com.kroger.abc.prototype.core.song.details.service.mapper.SongDetailsMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.kroger.abc.prototype.common.rest.util.projection.Projection.PROJECTION_SONG_DETAILS_KEY;
import static com.kroger.abc.prototype.common.rest.util.projection.Projection.Shape.FULL;

/**
 * Service Implementation for managing SongDetails.
 */
@Service
public class SongDetailsCoreServiceImpl implements SongDetailsCoreService {

    private final Logger log = LoggerFactory.getLogger(SongDetailsCoreServiceImpl.class);

    private final SongDetailsRepository songDetailsRepository;

    private final SongDetailsMapper songDetailsMapper;

    public SongDetailsCoreServiceImpl(SongDetailsRepository songDetailsRepository, SongDetailsMapper songDetailsMapper) {
        this.songDetailsRepository = songDetailsRepository;
        this.songDetailsMapper = songDetailsMapper;
    }

    /**
     * Save a songDetails.
     *
     * @param songDetailsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SongDetailsDTO save(SongDetailsDTO songDetailsDTO) {
        log.debug("Request to save SongDetails : {}", songDetailsDTO);
        SongDetails songDetails = songDetailsMapper.toEntity(songDetailsDTO);
        songDetails = songDetailsRepository.save(songDetails);
        return songDetailsMapper.toDto(songDetails);
    }

    /**
     * Get all the songDetails.
     *
     * @param projections
     * @param songFilters
     * @return the list of entities
     */
    @Override
    public List<SongDetailsDTO> findAll(Projections projections, SongFilters songFilters) {
        log.debug("Request to get all SongDetails");
        Projection.Shape shape = projections.extract().getByKey(PROJECTION_SONG_DETAILS_KEY);

        List<SongDetails> songDetails = songDetailsRepository.findAll();

        //emulate real DB queries for simplicity
        List<UUID> filterIds = songFilters.getIds();
        String titleFilter = songFilters.getTitle();
        List<UUID> artistIds = songFilters.getArtistIds();

        if (CollectionUtils.isNotEmpty(filterIds)) {
            songDetails = songDetails.stream()
                    .filter(details -> filterIds.contains(details.getId()))
                    .collect(Collectors.toList());
        } else {
            songDetails = songDetails.stream()
                    .filter(details -> {
                        boolean suitable = StringUtils.isBlank(titleFilter) && CollectionUtils.isEmpty(artistIds);
                        if ((StringUtils.isNotBlank(titleFilter) && StringUtils.containsIgnoreCase(details.getTitle(), titleFilter)) ||
                                (CollectionUtils.isNotEmpty(artistIds) && artistIds.contains(details.getArtistId()))) {
                            suitable = true;
                        }
                        return suitable;
                    })
                    .collect(Collectors.toList());
        }
        return songDetails.stream()
                .map(FULL.equals(shape) ? songDetailsMapper::toDto : songDetailsMapper::toDtoCompact)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one songDetails by id.
     *
     * @param id          the id of the entity
     * @param projections
     * @return the entity
     */
    @Override
    public SongDetailsDTO findOne(String id, Projections projections) {
        log.debug("Request to get SongDetails : {}", id);
        Projection.Shape shape = projections.extract().getByKey(PROJECTION_SONG_DETAILS_KEY);

        SongDetails songDetails = songDetailsRepository.findOne(UUID.fromString(id));
        return FULL.equals(shape) ? songDetailsMapper.toDto(songDetails) : songDetailsMapper.toDtoCompact(songDetails);
    }

    @Override
    public SongDetailsDTO create(SongDetailsCreateDTO songDetailsDTO) {
        log.debug("Request to create SongDetails : {}", songDetailsDTO);
        SongDetails songDetails = new SongDetails(null, null, null, songDetailsDTO.getTitle(),
                SongRelationships.extractArtist(songDetailsDTO.getRelationships()), songDetailsDTO.getDuration(),
                songDetailsDTO.getBitrate(), songDetailsDTO.getSubscribeOnly());
        songDetails = songDetailsRepository.save(songDetails);
        return songDetailsMapper.toDto(songDetails);
    }

}
