package com.kroger.abc.prototype.core.song.details.service.mapper;

import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongRelationships;
import com.kroger.abc.prototype.common.rest.mapper.EntityMapper;
import com.kroger.abc.prototype.core.song.details.domain.SongDetails;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * Mapper for the entity SongDetails and its DTO SongDetailsDTO.
 */
@Component
public class SongDetailsMapper implements EntityMapper<SongDetailsDTO, SongDetails> {


    @Override
    public SongDetails toEntity(SongDetailsDTO dto) {
        return dto == null ? null :
                new SongDetails(dto.getId(), dto.getCreatedAt(), dto.getModifiedAt(), dto.getTitle(),
                        SongRelationships.extractArtist(dto.getRelationships()),
                        dto.getDuration(), dto.getBitrate(), dto.getSubscribeOnly());
    }

    @Override
    public SongDetailsDTO toDto(SongDetails entity) {
        return entity == null ? null :
                new SongDetailsDTO(entity.getId(), entity.getCreatedAt(), entity.getModifiedAt(), entity.getTitle(),
                        SongRelationships.build(entity.getArtistId()),
                        entity.getDuration(), entity.getBitrate(), entity.getSubscribeOnly());
    }

    @Override
    public List<SongDetails> toEntity(List<SongDetailsDTO> dtoList) {
        return dtoList == null ? null :
                dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<SongDetailsDTO> toDto(List<SongDetails> entityList) {
        return entityList == null ? null :
                entityList.stream().map(this::toDto).collect(Collectors.toList());
    }

    public SongDetailsDTO toDtoCompact(SongDetails entity) {
        return entity == null ? null :
                new SongDetailsDTO(entity.getId(), null, null, entity.getTitle(),
                        SongRelationships.build(entity.getArtistId()), null, null, null);
    }

    public List<SongDetailsDTO> toDtoCompact(List<SongDetails> entityList) {
        return entityList == null ? null :
                entityList.stream().map(this::toDtoCompact).collect(Collectors.toList());
    }
}
