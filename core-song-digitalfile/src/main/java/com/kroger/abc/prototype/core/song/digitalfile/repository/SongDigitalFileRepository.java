package com.kroger.abc.prototype.core.song.digitalfile.repository;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileCreateDTO;
import com.kroger.abc.prototype.core.song.digitalfile.domain.SongDigitalFile;
import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.springframework.stereotype.Repository;

/**
 * Cassandra repository for the SongDigitalFile entity.
 */
@Repository
public class SongDigitalFileRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<SongDigitalFile> mapper;

    private PreparedStatement findBySongIdStmt;

    private PreparedStatement truncateStmt;


    public SongDigitalFileRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(SongDigitalFile.class);
        this.findBySongIdStmt = session.prepare("SELECT * FROM songDigitalFile WHERE songId = :songId");
        this.truncateStmt = session.prepare("TRUNCATE songDigitalFile");
    }

    public SongDigitalFile findBySongId(UUID songId) {
        BoundStatement stmt = findBySongIdStmt.bind()
                .setUUID("songId", songId);
        return session.execute(stmt).all().stream().map(
                row -> {
                    SongDigitalFile SongDigitalFile = new SongDigitalFile();
                    SongDigitalFile.setId(row.getUUID("id"));
                    SongDigitalFile.setSongId(row.getUUID("songId"));
                    SongDigitalFile.setUrl(row.getString("url"));
                    return SongDigitalFile;
                }
        ).findFirst().orElse(null);
    }

    public SongDigitalFile save(SongDigitalFile SongDigitalFile) {
        if (SongDigitalFile.getId() == null) {
            SongDigitalFile.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<SongDigitalFile>> violations = validator.validate(SongDigitalFile);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(SongDigitalFile);
        return SongDigitalFile;
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }

    public SongDigitalFile create(SongDigitalFileCreateDTO createDTO) {
        SongDigitalFile songDigitalFile = new SongDigitalFile();
        songDigitalFile.setId(UUID.randomUUID());
        songDigitalFile.setSongId(createDTO.getSongId());
        songDigitalFile.setUrl(createDTO.getUrl());

        Set<ConstraintViolation<SongDigitalFile>> violations = validator.validate(songDigitalFile);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(songDigitalFile);
        return songDigitalFile;
    }
}
