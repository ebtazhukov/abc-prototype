package com.kroger.abc.prototype.core.song.digitalfile.rest;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongIdFilter;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.ResponseUtil;
import com.kroger.abc.prototype.core.song.digitalfile.service.SongDigitalFileCoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing SongDigitalFile.
 */
@RestController
@RequestMapping("/core")
@Api(
        value = "core-song-digital-file",
        tags = "core-song-digital-file",
        description = "Core service for providing song digital file."
)
@Profile({"default", "core-song-digitalfiles"})
public class SongDigitalFileCoreController {

    private static final String ENTITY_NAME = "songDigitalFiles";
    private final Logger log = LoggerFactory.getLogger(SongDigitalFileCoreController.class);
    private final SongDigitalFileCoreService songDigitalFileCoreService;

    public SongDigitalFileCoreController(SongDigitalFileCoreService songDigitalFileCoreService) {
        this.songDigitalFileCoreService = songDigitalFileCoreService;
    }

    /**
     * GET  /song-digital-files : get songDigitalFile entities.
     *
     * @return the ResponseEntity with status 200 (OK) and with body the list of songDigitalFileDTO
     */
    @GetMapping("/song-digital-files")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get songDigitalFiles",
            notes = "This method is for obtaining song digital files"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = SongDigitalFileDTO[].class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<List<SongDigitalFileDTO>>> getSongDigitalFiles(
            @Valid SongIdFilter songIdFilter) {

        log.debug("REST request to get SongDigitalFiles, query: {}", QueryParametersHelper.toDebugString(songIdFilter));
        List<SongDigitalFileDTO> songDigitalFileDTOs = songDigitalFileCoreService.findAll(songIdFilter);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(songDigitalFileDTOs));
    }

    /**
     * POST  /song-digital-files : Creates songDigitalFile.
     *
     * @param createDTO the SongDigitalFileCreateDTO to create
     * @return the ResponseEntity with status 201 (CREATED) and with body of created SongDigitalFileDTO
     */
    @PostMapping("/song-digital-files")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Create the songDigitalFiles",
            notes = "This method is for creating songDigitalFiles"
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "CREATED", response = SongDigitalFileDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<SongDigitalFileDTO>> create(@Valid @RequestBody SongDigitalFileCreateDTO createDTO) {
        log.debug("REST request to create songDigitalFiles: {}", createDTO);
        SongDigitalFileDTO result = songDigitalFileCoreService.create(createDTO);
        return ResponseEntity.created(URI.create("")).body(ApiResponseBodyDeprecated.of(result));
    }

}
