package com.kroger.abc.prototype.core.song.digitalfile.service;

import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongIdFilter;
import java.util.List;

/**
 * Service Interface for managing SongDigitalFile.
 */
public interface SongDigitalFileCoreService {


    /**
     * Get the digitalFile by song id.
     *
     * @param songIdFilter the id of the song entity
     * @return the entity
     */
    List<SongDigitalFileDTO> findAll(SongIdFilter songIdFilter);

    SongDigitalFileDTO create(SongDigitalFileCreateDTO createDTO);
}