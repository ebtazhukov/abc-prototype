package com.kroger.abc.prototype.core.song.digitalfile.service.impl;

import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongIdFilter;
import com.kroger.abc.prototype.core.song.digitalfile.domain.SongDigitalFile;
import com.kroger.abc.prototype.core.song.digitalfile.repository.SongDigitalFileRepository;
import com.kroger.abc.prototype.core.song.digitalfile.service.SongDigitalFileCoreService;
import com.kroger.abc.prototype.core.song.digitalfile.service.mapper.SongDigitalFileMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing SongDigitalFile.
 */
@Service
public class SongDigitalFileCoreServiceImpl implements SongDigitalFileCoreService {

    private final Logger log = LoggerFactory.getLogger(SongDigitalFileCoreServiceImpl.class);

    private final SongDigitalFileRepository songDigitalFileRepository;

    private final SongDigitalFileMapper songDigitalFileMapper;

    public SongDigitalFileCoreServiceImpl(SongDigitalFileRepository songDigitalFileRepository, SongDigitalFileMapper songDigitalFileMapper) {
        this.songDigitalFileRepository = songDigitalFileRepository;
        this.songDigitalFileMapper = songDigitalFileMapper;
    }

    /**
     * Get one SongDigitalFile by song id.
     *
     * @param songIdFilter the id of the song entity
     * @return the entity
     */
    @Override
    public List<SongDigitalFileDTO> findAll(SongIdFilter songIdFilter) {
        log.debug("Request to get SongDigitalFile : {}", songIdFilter.getIds());
        List<SongDigitalFile> songDigitalFiles = new ArrayList<>();
        if (songIdFilter.getIds() != null) {
            songDigitalFiles = songIdFilter.getIds().stream()
                    .map(songDigitalFileRepository::findBySongId)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
        return songDigitalFileMapper.toDto(songDigitalFiles);
    }

    @Override
    public SongDigitalFileDTO create(SongDigitalFileCreateDTO createDTO) {
        log.debug("Request to create SongDigitalFile : {}", createDTO);
        SongDigitalFile songDigitalFile = songDigitalFileRepository.create(createDTO);
        return songDigitalFileMapper.toDto(songDigitalFile);
    }
}
