package com.kroger.abc.prototype.core.song.digitalfile.service.mapper;

import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileDTO;
import com.kroger.abc.prototype.common.rest.mapper.EntityMapper;
import com.kroger.abc.prototype.core.song.digitalfile.domain.SongDigitalFile;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * Mapper for the entity SongDigitalFile and its DTO SongDigitalFileDTO.
 */
@Component
public class SongDigitalFileMapper implements EntityMapper<SongDigitalFileDTO, SongDigitalFile> {


    @Override
    public SongDigitalFile toEntity(SongDigitalFileDTO dto) {
        return dto == null ? null :
                new SongDigitalFile(dto.getId(), dto.getSongId(), dto.getUrl());
    }

    @Override
    public SongDigitalFileDTO toDto(SongDigitalFile entity) {
        return entity == null ? null :
                new SongDigitalFileDTO(entity.getId(), entity.getSongId(), entity.getUrl());
    }

    @Override
    public List<SongDigitalFile> toEntity(List<SongDigitalFileDTO> dtoList) {
        return dtoList == null ? null :
                dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<SongDigitalFileDTO> toDto(List<SongDigitalFile> entityList) {
        return entityList == null ? null :
                entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}
