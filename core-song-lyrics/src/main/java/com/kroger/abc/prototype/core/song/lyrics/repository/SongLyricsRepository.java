package com.kroger.abc.prototype.core.song.lyrics.repository;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsCreateDTO;
import com.kroger.abc.prototype.core.song.lyrics.domain.SongLyrics;
import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.springframework.stereotype.Repository;

/**
 * Cassandra repository for the SongLyrics entity.
 */
@Repository
public class SongLyricsRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<SongLyrics> mapper;

    private PreparedStatement findBySongIdStmt;

    private PreparedStatement truncateStmt;


    public SongLyricsRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(SongLyrics.class);
        this.findBySongIdStmt = session.prepare("SELECT * FROM songLyrics WHERE songId = :songId");
        this.truncateStmt = session.prepare("TRUNCATE songLyrics");
    }

    public SongLyrics findBySongId(UUID songId) {
        BoundStatement stmt = findBySongIdStmt.bind()
                .setUUID("songId", songId);
        return session.execute(stmt).all().stream().map(
                row -> {
                    SongLyrics songLyrics = new SongLyrics();
                    songLyrics.setId(row.getUUID("id"));
                    songLyrics.setSongId(row.getUUID("songId"));
                    songLyrics.setLyrics(row.getString("lyrics"));
                    return songLyrics;
                }
        ).findFirst().orElse(null);
    }

    public SongLyrics save(SongLyrics songLyrics) {
        if (songLyrics.getId() == null) {
            songLyrics.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<SongLyrics>> violations = validator.validate(songLyrics);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(songLyrics);
        return songLyrics;
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }

    public SongLyrics create(SongLyricsCreateDTO createDTO) {
        SongLyrics songLyrics = new SongLyrics();
        songLyrics.setId(UUID.randomUUID());
        songLyrics.setSongId(createDTO.getSongId());
        songLyrics.setLyrics(createDTO.getLyrics());

        Set<ConstraintViolation<SongLyrics>> violations = validator.validate(songLyrics);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(songLyrics);
        return songLyrics;
    }
}
