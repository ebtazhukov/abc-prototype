package com.kroger.abc.prototype.core.song.lyrics.rest;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongIdFilter;
import com.kroger.abc.prototype.common.rest.param.projection.SongLyricsProjections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.ResponseUtil;
import com.kroger.abc.prototype.core.song.lyrics.service.SongLyricsCoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing SongLyrics.
 */
@RestController
@RequestMapping("/core")
@Api(
        value = "core-song-lyrics",
        tags = "core-song-lyrics",
        description = "Core service for providing song lyrics."
)
@Profile({"default", "core-song-lyrics"})
public class SongLyricsCoreController {

    private static final String ENTITY_NAME = "songLyrics";
    private final Logger log = LoggerFactory.getLogger(SongLyricsCoreController.class);
    private final SongLyricsCoreService songLyricsCoreService;

    public SongLyricsCoreController(SongLyricsCoreService songLyricsCoreService) {
        this.songLyricsCoreService = songLyricsCoreService;
    }

    /**
     * GET  /song-lyrics : get songLyrics entities.
     *
     * @return the ResponseEntity with status 200 (OK) and with body the list of songLyricsDTO
     */
    @GetMapping("/song-lyrics")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get songLyrics",
            notes = "This method is for obtaining song lyrics"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = SongLyricsDTO[].class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<List<SongLyricsDTO>>> getSongLyrics(
            @Valid SongLyricsProjections projections,
            @Valid SongIdFilter songIdFilter) {

        log.debug("REST request to get SongLyrics, query: {}",
                QueryParametersHelper.toDebugString(projections, songIdFilter));
        List<SongLyricsDTO> songLyricsDTOs = songLyricsCoreService.findAll(songIdFilter, projections);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(songLyricsDTOs));
    }

    /**
     * POST  /song-lyrics : Creates songLyrics.
     *
     * @param createDTO the SongLyricsCreateDTO to create
     * @return the ResponseEntity with status 201 (CREATED) and with body of created SongLyricsDTO
     */
    @PostMapping("/song-lyrics")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Create the songLyrics",
            notes = "This method is for creating songLyrics"
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "CREATED", response = SongLyricsDTO.class)
    })
    public ResponseEntity<ApiResponseBodyDeprecated<SongLyricsDTO>> create(@Valid @RequestBody SongLyricsCreateDTO createDTO) {
        log.debug("REST request to create song lyrics: {}", createDTO);
        SongLyricsDTO result = songLyricsCoreService.create(createDTO);
        return ResponseEntity.created(URI.create("")).body(ApiResponseBodyDeprecated.of(result));
    }

}
