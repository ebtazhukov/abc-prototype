package com.kroger.abc.prototype.core.song.lyrics.service;

import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongIdFilter;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;

/**
 * Service Interface for managing SongLyrics.
 */
public interface SongLyricsCoreService {


    /**
     * Get the songLyrics
     *
     * @param songIdFilter
     * @param projections
     * @return the entity
     */
    List<SongLyricsDTO> findAll(SongIdFilter songIdFilter, Projections projections);

    SongLyricsDTO create(SongLyricsCreateDTO createDTO);
}