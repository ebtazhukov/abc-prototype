package com.kroger.abc.prototype.core.song.lyrics.service.impl;

import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongIdFilter;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.util.projection.Projection;
import com.kroger.abc.prototype.core.song.lyrics.domain.SongLyrics;
import com.kroger.abc.prototype.core.song.lyrics.repository.SongLyricsRepository;
import com.kroger.abc.prototype.core.song.lyrics.service.SongLyricsCoreService;
import com.kroger.abc.prototype.core.song.lyrics.service.mapper.SongLyricsMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.kroger.abc.prototype.common.rest.util.projection.Projection.PROJECTION_SONG_LYRICS_KEY;
import static com.kroger.abc.prototype.common.rest.util.projection.Projection.Shape.FULL;

/**
 * Service Implementation for managing SongLyrics.
 */
@Service
public class SongLyricsCoreServiceImpl implements SongLyricsCoreService {

    private final Logger log = LoggerFactory.getLogger(SongLyricsCoreServiceImpl.class);

    private final SongLyricsRepository songLyricsRepository;

    private final SongLyricsMapper songLyricsMapper;

    public SongLyricsCoreServiceImpl(SongLyricsRepository songLyricsRepository, SongLyricsMapper songLyricsMapper) {
        this.songLyricsRepository = songLyricsRepository;
        this.songLyricsMapper = songLyricsMapper;
    }

    /**
     * Get one songLyrics.
     *
     * @param songIdFilter
     * @param projections
     * @return the entity
     */
    @Override
    public List<SongLyricsDTO> findAll(SongIdFilter songIdFilter, Projections projections) {
        log.debug("Request to get SongLyrics : {}", songIdFilter.getIds());
        Projection.Shape shape = projections.extract().getByKey(PROJECTION_SONG_LYRICS_KEY);

        List<SongLyrics> songLyrics = new ArrayList<>();
        if (songIdFilter.getIds() != null) {
            songLyrics = songIdFilter.getIds().stream()
                    .map(songLyricsRepository::findBySongId)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
        return FULL.equals(shape) ? songLyricsMapper.toDto(songLyrics) : songLyricsMapper.toDtoCompact(songLyrics);
    }

    @Override
    public SongLyricsDTO create(SongLyricsCreateDTO createDTO) {
        log.debug("Request to create SongLyrics : {}", createDTO);
        SongLyrics songLyrics = songLyricsRepository.create(createDTO);
        return songLyricsMapper.toDto(songLyrics);
    }
}
