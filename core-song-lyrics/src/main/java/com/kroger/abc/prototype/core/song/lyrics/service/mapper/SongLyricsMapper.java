package com.kroger.abc.prototype.core.song.lyrics.service.mapper;

import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsDTO;
import com.kroger.abc.prototype.common.rest.mapper.EntityMapper;
import com.kroger.abc.prototype.core.song.lyrics.domain.SongLyrics;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * Mapper for the entity SongLyrics and its DTO SongLyricsDTO.
 */
@Component
public class SongLyricsMapper implements EntityMapper<SongLyricsDTO, SongLyrics> {


    @Override
    public SongLyrics toEntity(SongLyricsDTO dto) {
        return dto == null ? null :
                new SongLyrics(dto.getId(), dto.getSongId(), dto.getLyrics());
    }

    @Override
    public SongLyricsDTO toDto(SongLyrics entity) {
        return entity == null ? null :
                new SongLyricsDTO(entity.getId(), entity.getSongId(), entity.getLyrics());
    }

    @Override
    public List<SongLyrics> toEntity(List<SongLyricsDTO> dtoList) {
        return dtoList == null ? null :
                dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<SongLyricsDTO> toDto(List<SongLyrics> entityList) {
        return entityList == null ? null :
                entityList.stream().map(this::toDto).collect(Collectors.toList());
    }

    public SongLyricsDTO toDtoCompact(SongLyrics entity) {
        return entity == null ? null :
                new SongLyricsDTO(entity.getId(), entity.getSongId(), StringUtils.substring(entity.getLyrics(), 0, 13));
    }

    public List<SongLyricsDTO> toDtoCompact(List<SongLyrics> entityList) {
        return entityList == null ? null :
                entityList.stream().map(this::toDtoCompact).collect(Collectors.toList());
    }
}
