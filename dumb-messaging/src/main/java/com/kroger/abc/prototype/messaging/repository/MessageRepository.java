package com.kroger.abc.prototype.messaging.repository;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.kroger.abc.prototype.messaging.domain.Message;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.springframework.stereotype.Repository;

@Repository
public class MessageRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Message> mapper;

    private PreparedStatement findAllStmt;

    public MessageRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(Message.class);
        this.findAllStmt = session.prepare("SELECT * FROM message WHERE type = :type ALLOW FILTERING");
    }

    public List<Message> findByType(String type) {
        List<Message> messages = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind()
                .setString("type", type);
        session.execute(stmt).all().stream()
                .map(
                        row -> {
                            Message message = new Message();
                            message.setId(row.getUUID("id"));
                            message.setCreatedAt(row.get("createdAt", Instant.class));
                            message.setType(row.getString("type"));
                            message.setBody(row.getString("body"));
                            return message;
                        }
                )
                .forEach(messages::add);
        return messages;
    }

    public Message save(Message message) {
        message.setId(UUID.randomUUID());
        message.setCreatedAt(Instant.now());

        Set<ConstraintViolation<Message>> violations = validator.validate(message);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(message);
        return message;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }
}
