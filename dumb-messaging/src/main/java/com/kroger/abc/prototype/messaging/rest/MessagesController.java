package com.kroger.abc.prototype.messaging.rest;

import com.kroger.abc.prototype.messaging.domain.Message;
import com.kroger.abc.prototype.messaging.service.MessagesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(
        value = "messages",
        tags = "messages",
        description = "Very simple implementation of messaging system"
)
@RequestMapping("/aux")
@Profile({"default", "aux-messages"})
public class MessagesController {

    private static final String ENTITY_NAME = "messages";
    private final Logger log = LoggerFactory.getLogger(MessagesController.class);
    private final MessagesService messagesService;

    public MessagesController(MessagesService messagesService) {
        this.messagesService = messagesService;
    }

    /**
     * GET  /messages?type=MESSAGE_TYPE : get all messages by specific type.
     */
    @GetMapping("/messages")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(
            value = "Get messages by specific type",
            notes = "This method is for obtaining messages by specific type"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK", response = Message.class)
    })
    public ResponseEntity<List<Message>> getMessagesByType(
            @ApiParam(value = "**Message type**.", required = true) @RequestParam("type") String type) {

        log.debug("REST request to get Messages by type : {}", type);
        List<Message> messages = messagesService.getByType(type);
        return ResponseEntity.ok().body(messages);
    }

    /**
     * POST  /messages : Creates a new message.
     */
    @PostMapping("/messages")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(
            value = "Creates a new message",
            notes = "This method is for creating a message"
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "CREATED", response = Message.class)
    })
    public ResponseEntity<Message> createMessage(@RequestBody Message message) {

        log.debug("REST request to create Message : {}", message);
        message = messagesService.create(message);
        return ResponseEntity.created(URI.create("")).body(message);
    }

    /**
     * DELETE  /messages/:id : Deletes an existing message.
     */
    @DeleteMapping("/messages/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(
            value = "Delete the message",
            notes = "This method is for deleting message"
    )
    @ApiResponses({
            @ApiResponse(code = 204, message = "NO_CONTENT")
    })
    public ResponseEntity<Void> deleteMessage(
            @ApiParam(value = "**Message ID**.", required = true) @PathVariable String id) {
        log.debug("REST request to delete Message : {}", id);
        messagesService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
