package com.kroger.abc.prototype.messaging.service;

import com.kroger.abc.prototype.messaging.domain.Message;
import java.util.List;

public interface MessagesService {

    Message create(Message message);

    void delete(String id);

    List<Message> getByType(String type);
}
