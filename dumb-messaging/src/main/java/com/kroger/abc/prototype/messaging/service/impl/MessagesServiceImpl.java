package com.kroger.abc.prototype.messaging.service.impl;

import com.kroger.abc.prototype.messaging.domain.Message;
import com.kroger.abc.prototype.messaging.repository.MessageRepository;
import com.kroger.abc.prototype.messaging.service.MessagesService;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class MessagesServiceImpl implements MessagesService {

    private final Logger log = LoggerFactory.getLogger(MessagesServiceImpl.class);

    private final MessageRepository messageRepository;

    public MessagesServiceImpl(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public Message create(Message message) {
        return messageRepository.save(message);
    }

    @Override
    public void delete(String id) {
        messageRepository.delete(UUID.fromString(id));
    }

    @Override
    public List<Message> getByType(String type) {
        return messageRepository.findByType(type);
    }
}
