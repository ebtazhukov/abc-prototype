#!/usr/bin/env bash

echo "Setup environment"
source setup_env.sh

export PROJECT_ROOT=$PWD/../..
cd ${PROJECT_ROOT}

export DOCKER_IMAGE_NAME=abc-prototype-monolith

export APP_DOCKER_IMAGE=${DOCKER_REGISTRY}/opsitron/${DOCKER_IMAGE_NAME}

printf "\nBuilding monolith project \n"

./gradlew clean build -x test

printf "\nBuilding docker image for monolith: %s\n" "$APP_DOCKER_IMAGE"

docker build -t ${APP_DOCKER_IMAGE} .

printf "\nPushing docker image for monolith into artifactory: %s\n" "$APP_DOCKER_IMAGE"

docker push ${APP_DOCKER_IMAGE}:latest

printf "\nStarting deployment...\n"

for VAR in app-artists app-accounts app-playlists app-songs business-albums business-songs core-accounts core-artists core-playlists core-album-details core-album-notes core-song-details core-song-lyrics core-song-digitalfiles aux-messages
do
	export ENV=${VAR}

	printf "\n%s service deployment. Docker container reference: \n" "$ENV"

	docker run -d -e ENV=${ENV} -e CF_NONPROD_PW=${CF_NONPROD_PW} \
        -e CASSANDRA_HOSTNAME=${CASSANDRA_HOSTNAME} -e CASSANDRA_USERNAME=${CASSANDRA_USERNAME} -e CASSANDRA_PASSWORD=${CASSANDRA_PASSWORD} \
        -e GITLAB_USER_NAME=someUser -e GITLAB_USER_EMAIL=some@user.email \
        --rm --volume $(pwd):/app --workdir /app docker-prod.registry.kroger.com/tools/dcploy dcploy deploy ${ENV} --debug --no-smoketest
done