#!/usr/bin/env bash

echo "Setup environment"
source setup_env.sh

export PROJECT_ROOT=$PWD/../..
cd ${PROJECT_ROOT}/ui

export ENV=ui

export DOCKER_IMAGE_NAME=abc-prototype-${ENV}

export APP_DOCKER_IMAGE=${DOCKER_REGISTRY}/opsitron/${DOCKER_IMAGE_NAME}

printf "\nBuilding docker image: %s\n" "$APP_DOCKER_IMAGE"

docker build -t ${APP_DOCKER_IMAGE} .

printf "\nPushing docker image into artifactory: %s\n" "$APP_DOCKER_IMAGE"

docker push ${APP_DOCKER_IMAGE}:latest

cd ${PROJECT_ROOT}

printf "\n%s service deployment. Docker container reference: \n" "$ENV"

docker run -d -e ENV=${ENV} -e CF_NONPROD_PW=${CF_NONPROD_PW} \
    -e CASSANDRA_HOSTNAME=${CASSANDRA_HOSTNAME} -e CASSANDRA_USERNAME=${CASSANDRA_USERNAME} -e CASSANDRA_PASSWORD=${CASSANDRA_PASSWORD} \
    -e GITLAB_USER_NAME=someUser -e GITLAB_USER_EMAIL=some@user.email \
    --rm --volume $(pwd):/app --workdir /app docker-prod.registry.kroger.com/tools/dcploy dcploy deploy ${ENV} --debug --no-smoketest