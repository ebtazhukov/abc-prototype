#!/usr/bin/env bash

echo "Setup environment"
if [[ ! -f secret_sauce.sh ]] ; then
    echo 'File "secret_sauce.sh" is not there, aborting.'
    exit
fi

source secret_sauce.sh

export PROJECT_ROOT=$PWD/../..
cd ${PROJECT_ROOT}/monolith-main/src/main/docker/cassandra

export APP_DOCKER_IMAGE=abc-prototype-cassandra-reset

printf "\nBuilding docker image: %s\n" "$APP_DOCKER_IMAGE"

docker build -t ${APP_DOCKER_IMAGE} -f Dockerfile.reset \
--build-arg CASSANDRA_HOSTNAME=${CASSANDRA_HOSTNAME} --build-arg CASSANDRA_USERNAME=${CASSANDRA_USERNAME} \
--build-arg CASSANDRA_PASSWORD=${CASSANDRA_PASSWORD} .


docker run ${APP_DOCKER_IMAGE}
