#!/usr/bin/env bash

if [[ ! -f secret_sauce.sh ]] ; then
    echo 'File "secret_sauce.sh" is not there, aborting.'
    exit
fi

source secret_sauce.sh

export DOCKER_REGISTRY=docker-prod.registry.kroger.com

docker logout ${DOCKER_REGISTRY}

docker login ${DOCKER_REGISTRY} -u ${ARTIFACTORY_USERNAME} -p ${ARTIFACTORY_PASSWORD}