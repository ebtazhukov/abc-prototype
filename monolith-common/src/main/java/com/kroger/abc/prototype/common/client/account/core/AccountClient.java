package com.kroger.abc.prototype.common.client.account.core;

import com.kroger.abc.prototype.common.rest.dto.account.AccountDTO;

public interface AccountClient {

    AccountDTO findOne();

    void delete();
}
