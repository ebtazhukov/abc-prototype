package com.kroger.abc.prototype.common.client.account.core;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.account.AccountDTO;
import com.kroger.abc.prototype.common.rest.util.SecurityContextHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static java.util.Collections.EMPTY_MAP;

@Component
public class AccountClientImpl implements AccountClient {

    private final RestTemplate restTemplate;
    @Value("${abc.prototype.client.core.accounts}")
    private String serviceURL;
    private ParameterizedTypeReference<ApiResponseBodyDeprecated<AccountDTO>> accountDetailsResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<AccountDTO>>() {
            };

    public AccountClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public AccountDTO findOne() {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<AccountDTO>> entity = restTemplate.exchange(
                    serviceURL,
                    HttpMethod.GET,
                    new HttpEntity<>(getAuthHeaders()),
                    accountDetailsResponseType,
                    EMPTY_MAP);
            return HttpStatus.OK.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void delete() {
        try {
            restTemplate.exchange(serviceURL, HttpMethod.DELETE, new HttpEntity<>(getAuthHeaders()), Void.class);
        } catch (Exception ignored) {
        }
    }

    private HttpHeaders getAuthHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + SecurityContextHelper.getToken());
        return headers;
    }
}
