package com.kroger.abc.prototype.common.client.album.core.albumdetails;

import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.AlbumFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;
import java.util.UUID;

public interface AlbumDetailsClient {

    List<AlbumDetailsDTO> findAll(Projections projections, AlbumFilters albumFilters);

    AlbumDetailsDTO findOne(UUID id, Projections projections);

    AlbumDetailsDTO create(AlbumDetailsCreateDTO createDTO);
}
