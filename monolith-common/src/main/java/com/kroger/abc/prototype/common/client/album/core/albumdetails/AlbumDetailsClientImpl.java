package com.kroger.abc.prototype.common.client.album.core.albumdetails;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumDetailsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.AlbumFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.SecurityContextHelper;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class AlbumDetailsClientImpl implements AlbumDetailsClient {

    private final RestTemplate restTemplate;
    @Value("${abc.prototype.client.core.albums.details}")
    private String serviceURL;
    private ParameterizedTypeReference<ApiResponseBodyDeprecated<List<AlbumDetailsDTO>>> listAlbumDetailsResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<List<AlbumDetailsDTO>>>() {
            };
    private ParameterizedTypeReference<ApiResponseBodyDeprecated<AlbumDetailsDTO>> albumDetailsResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<AlbumDetailsDTO>>() {
            };

    public AlbumDetailsClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<AlbumDetailsDTO> findAll(Projections projections, AlbumFilters albumFilters) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<List<AlbumDetailsDTO>>> entity = restTemplate.exchange(
                    serviceURL + QueryParametersHelper.getUrlBindings(projections, albumFilters),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    listAlbumDetailsResponseType,
                    QueryParametersHelper.getValuesMap(projections, albumFilters));
            return HttpStatus.OK.equals(entity.getStatusCode()) ?
                    entity.getBody().getData() : Collections.emptyList();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    @Override
    public AlbumDetailsDTO findOne(UUID id, Projections projections) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<AlbumDetailsDTO>> entity = restTemplate.exchange(
                    serviceURL + "/" + id + QueryParametersHelper.getUrlBindings(projections),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    albumDetailsResponseType,
                    QueryParametersHelper.getValuesMap(projections));
            return HttpStatus.OK.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public AlbumDetailsDTO create(AlbumDetailsCreateDTO createDTO) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<AlbumDetailsDTO>> entity = restTemplate.exchange(
                    serviceURL,
                    HttpMethod.POST,
                    new HttpEntity<>(createDTO, getAuthHeaders()),
                    albumDetailsResponseType,
                    new HashMap<>());
            return HttpStatus.CREATED.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    private HttpHeaders getAuthHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + SecurityContextHelper.getToken());
        return headers;
    }
}
