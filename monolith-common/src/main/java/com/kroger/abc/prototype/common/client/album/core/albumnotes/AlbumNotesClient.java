package com.kroger.abc.prototype.common.client.album.core.albumnotes;

import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesDTO;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;
import java.util.UUID;

public interface AlbumNotesClient {

    List<AlbumNotesDTO> findByAlbumIds(List<UUID> songIds, Projections projections);

    AlbumNotesDTO create(AlbumNotesCreateDTO createDTO);
}
