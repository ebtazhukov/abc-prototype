package com.kroger.abc.prototype.common.client.album.core.albumnotes;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.album.core.AlbumNotesDTO;
import com.kroger.abc.prototype.common.rest.param.filter.AlbumIdFilter;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.SecurityContextHelper;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class AlbumNotesClientImpl implements AlbumNotesClient {

    private final RestTemplate restTemplate;
    @Value("${abc.prototype.client.core.albums.notes}")
    private String serviceURL;

    private ParameterizedTypeReference<ApiResponseBodyDeprecated<AlbumNotesDTO>> albumNotesResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<AlbumNotesDTO>>() {
            };

    private ParameterizedTypeReference<ApiResponseBodyDeprecated<List<AlbumNotesDTO>>> listAlbumNotesResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<List<AlbumNotesDTO>>>() {
            };

    public AlbumNotesClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<AlbumNotesDTO> findByAlbumIds(List<UUID> albumIds, Projections projections) {
        AlbumIdFilter albumIdFilter = new AlbumIdFilter();
        albumIdFilter.setIds(albumIds);

        try {
            ResponseEntity<ApiResponseBodyDeprecated<List<AlbumNotesDTO>>> entity = restTemplate.exchange(
                    serviceURL + QueryParametersHelper.getUrlBindings(projections, albumIdFilter),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    listAlbumNotesResponseType,
                    QueryParametersHelper.getValuesMap(projections, albumIdFilter));
            return HttpStatus.OK.equals(entity.getStatusCode()) ?
                    entity.getBody().getData() : Collections.emptyList();
        } catch (Exception ignored) {
            return Collections.emptyList();
        }
    }

    @Override
    public AlbumNotesDTO create(AlbumNotesCreateDTO createDTO) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<AlbumNotesDTO>> entity = restTemplate.exchange(
                    serviceURL,
                    HttpMethod.POST,
                    new HttpEntity<>(createDTO, getAuthHeaders()),
                    albumNotesResponseType,
                    new HashMap<>());
            return HttpStatus.CREATED.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    private HttpHeaders getAuthHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + SecurityContextHelper.getToken());
        return headers;
    }
}
