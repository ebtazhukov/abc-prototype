package com.kroger.abc.prototype.common.client.artist.core;

import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;
import com.kroger.abc.prototype.common.rest.param.filter.ArtistFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;
import java.util.UUID;

public interface ArtistClient {

    List<ArtistDTO> findAll(Projections projections, ArtistFilters artistFilters);

    ArtistDTO findOne(UUID id, Projections projections);

    List<ArtistDTO> getOrCreateInBulk(List<ArtistCreateDTO> artistsCreateDTO);
}
