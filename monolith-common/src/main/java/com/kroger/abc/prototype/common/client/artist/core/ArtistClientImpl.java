package com.kroger.abc.prototype.common.client.artist.core;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;
import com.kroger.abc.prototype.common.rest.param.filter.ArtistFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.SecurityContextHelper;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ArtistClientImpl implements ArtistClient {

    private final RestTemplate restTemplate;
    @Value("${abc.prototype.client.core.artists}")
    private String serviceURL;
    private ParameterizedTypeReference<ApiResponseBodyDeprecated<List<ArtistDTO>>> listArtistDetailsResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<List<ArtistDTO>>>() {
            };
    private ParameterizedTypeReference<ApiResponseBodyDeprecated<ArtistDTO>> artistDetailsResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<ArtistDTO>>() {
            };

    public ArtistClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<ArtistDTO> findAll(Projections projections, ArtistFilters artistFilters) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<List<ArtistDTO>>> entity = restTemplate.exchange(
                    serviceURL + QueryParametersHelper.getUrlBindings(projections, artistFilters),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    listArtistDetailsResponseType,
                    QueryParametersHelper.getValuesMap(projections, artistFilters));
            return HttpStatus.OK.equals(entity.getStatusCode()) ?
                    entity.getBody().getData() : Collections.emptyList();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    @Override
    public ArtistDTO findOne(UUID id, Projections projections) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<ArtistDTO>> entity = restTemplate.exchange(
                    serviceURL + "/" + id + QueryParametersHelper.getUrlBindings(projections),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    artistDetailsResponseType,
                    QueryParametersHelper.getValuesMap(projections));
            return HttpStatus.OK.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<ArtistDTO> getOrCreateInBulk(List<ArtistCreateDTO> artistsCreateDTO) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<List<ArtistDTO>>> entity = restTemplate.exchange(
                    serviceURL + "/get-or-create",
                    HttpMethod.POST,
                    new HttpEntity<>(artistsCreateDTO, getAuthHeaders()),
                    listArtistDetailsResponseType,
                    new HashMap<>());
            return HttpStatus.OK.equals(entity.getStatusCode()) ?
                    entity.getBody().getData() : Collections.emptyList();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    private HttpHeaders getAuthHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + SecurityContextHelper.getToken());
        return headers;
    }
}
