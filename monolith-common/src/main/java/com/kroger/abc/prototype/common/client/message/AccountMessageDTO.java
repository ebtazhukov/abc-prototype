package com.kroger.abc.prototype.common.client.message;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountMessageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID id;

    private Instant createdAt;

    private String type;

    private String body;

    public AccountMessageDTO(String type, String username) {
        this.type = type;
        this.body = username;
    }
}
