package com.kroger.abc.prototype.common.client.message;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class AccountMessagesClient {

    private static final String MESSAGE_TYPE = "ACCOUNT_DELETED";
    private final RestTemplate restTemplate;
    @Value("${abc.prototype.client.aux.messages}")
    private String serviceURL;

    public AccountMessagesClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public AccountMessageDTO create(String username) {
        try {
            AccountMessageDTO body = new AccountMessageDTO(MESSAGE_TYPE, username);
            ResponseEntity<AccountMessageDTO> entity =
                    restTemplate.exchange(serviceURL, HttpMethod.POST, new HttpEntity<>(body), AccountMessageDTO.class);
            return HttpStatus.CREATED.equals(entity.getStatusCode()) ? entity.getBody() : null;
        } catch (Exception e) {
            return null;
        }
    }

    public List<AccountMessageDTO> findAll() {
        try {
            ResponseEntity<AccountMessageDTO[]> entity = restTemplate.exchange(
                    serviceURL + "?type=" + MESSAGE_TYPE,
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    AccountMessageDTO[].class);
            return HttpStatus.OK.equals(entity.getStatusCode()) ?
                    Arrays.asList(entity.getBody()) : Collections.emptyList();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    public void delete(UUID id) {
        try {
            restTemplate.exchange(serviceURL + "/" + id, HttpMethod.DELETE, HttpEntity.EMPTY, Void.class);
        } catch (Exception ignored) {
        }
    }
}
