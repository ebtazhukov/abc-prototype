package com.kroger.abc.prototype.common.client.playlist.core.playlist;

import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistUpdateDTO;
import com.kroger.abc.prototype.common.rest.param.filter.PlaylistFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;
import java.util.UUID;

public interface PlaylistClient {

    PlaylistDTO create(PlaylistCreateDTO playlistDTO);

    PlaylistDTO save(PlaylistUpdateDTO playlistDTO);

    List<PlaylistDTO> findAll(Projections projections, PlaylistFilters playlistFilters);

    PlaylistDTO findOne(UUID id, Projections projections);

    void delete(UUID id);
}
