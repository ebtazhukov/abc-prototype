package com.kroger.abc.prototype.common.client.playlist.core.playlist;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistUpdateDTO;
import com.kroger.abc.prototype.common.rest.param.filter.PlaylistFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.SecurityContextHelper;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PlaylistClientImpl implements PlaylistClient {

    @Value("${abc.prototype.client.core.playlists}")
    private String serviceURL;

    private final RestTemplate restTemplate;

    private ParameterizedTypeReference<ApiResponseBodyDeprecated<List<PlaylistDTO>>> listPlaylistResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<List<PlaylistDTO>>>() {
            };
    private ParameterizedTypeReference<ApiResponseBodyDeprecated<PlaylistDTO>> playlistResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<PlaylistDTO>>() {
            };

    public PlaylistClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public PlaylistDTO create(PlaylistCreateDTO playlistDTO) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<PlaylistDTO>> entity =
                    restTemplate.exchange(serviceURL, HttpMethod.POST, new HttpEntity<>(playlistDTO, getAuthHeaders()), playlistResponseType);
            return HttpStatus.CREATED.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public PlaylistDTO save(PlaylistUpdateDTO playlistDTO) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<PlaylistDTO>> entity =
                    restTemplate.exchange(serviceURL, HttpMethod.PUT, new HttpEntity<>(playlistDTO, getAuthHeaders()), playlistResponseType);
            return HttpStatus.OK.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<PlaylistDTO> findAll(Projections projections, PlaylistFilters playlistFilters) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<List<PlaylistDTO>>> entity = restTemplate.exchange(
                    serviceURL + QueryParametersHelper.getUrlBindings(projections, playlistFilters),
                    HttpMethod.GET,
                    new HttpEntity<>(getAuthHeaders()),
                    listPlaylistResponseType,
                    QueryParametersHelper.getValuesMap(projections, playlistFilters));
            return HttpStatus.OK.equals(entity.getStatusCode()) ?
                    entity.getBody().getData() : Collections.emptyList();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    @Override
    public PlaylistDTO findOne(UUID id, Projections projections) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<PlaylistDTO>> entity = restTemplate.exchange(
                    serviceURL + "/" + id + QueryParametersHelper.getUrlBindings(projections),
                    HttpMethod.GET,
                    new HttpEntity<>(getAuthHeaders()),
                    playlistResponseType,
                    QueryParametersHelper.getValuesMap(projections));
            return HttpStatus.OK.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void delete(UUID id) {
        //TODO: handle not found
        try {
            restTemplate.exchange(serviceURL + "/" + id, HttpMethod.DELETE, new HttpEntity<>(getAuthHeaders()), Void.class);
        } catch (Exception ignored) {
        }
    }

    private HttpHeaders getAuthHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + SecurityContextHelper.getToken());
        return headers;
    }
}
