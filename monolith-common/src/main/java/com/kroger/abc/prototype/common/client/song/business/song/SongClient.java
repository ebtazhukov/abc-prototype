package com.kroger.abc.prototype.common.client.song.business.song;

import com.kroger.abc.prototype.common.rest.dto.song.business.SongCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;
import java.util.UUID;

public interface SongClient {

    List<SongDTO> findAll(Projections projections, SongFilters songFilters);

    SongDTO findOne(UUID id, Projections projections);

    SongDTO update(SongDTO songDTO);

    List<SongDTO> getOrCreateInBulk(List<SongCreateDTO> songsCreateDTO);
}
