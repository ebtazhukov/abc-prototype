package com.kroger.abc.prototype.common.client.song.business.song;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.SecurityContextHelper;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SongClientImpl implements SongClient {

    private final RestTemplate restTemplate;
    @Value("${abc.prototype.client.business.songs}")
    private String serviceURL;
    private ParameterizedTypeReference<ApiResponseBodyDeprecated<List<SongDTO>>> listSongResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<List<SongDTO>>>() {
            };
    private ParameterizedTypeReference<ApiResponseBodyDeprecated<SongDTO>> songResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<SongDTO>>() {
            };

    public SongClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<SongDTO> findAll(Projections projections, SongFilters songFilters) {
        try {

            ResponseEntity<ApiResponseBodyDeprecated<List<SongDTO>>> entity = restTemplate.exchange(
                    serviceURL + QueryParametersHelper.getUrlBindings(projections, songFilters),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    listSongResponseType,
                    QueryParametersHelper.getValuesMap(projections, songFilters));
            return HttpStatus.OK.equals(entity.getStatusCode()) ?
                    entity.getBody().getData() : Collections.emptyList();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    @Override
    public SongDTO findOne(UUID id, Projections projections) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<SongDTO>> entity = restTemplate.exchange(
                    serviceURL + "/" + id + QueryParametersHelper.getUrlBindings(projections),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    songResponseType,
                    QueryParametersHelper.getValuesMap(projections));
            return HttpStatus.OK.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public SongDTO update(SongDTO songDTO) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<SongDTO>> entity =
                    restTemplate.exchange(serviceURL, HttpMethod.PUT, new HttpEntity<>(songDTO), songResponseType);
            return HttpStatus.OK.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<SongDTO> getOrCreateInBulk(List<SongCreateDTO> songsCreateDTO) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<List<SongDTO>>> entity = restTemplate.exchange(
                    serviceURL + "/get-or-create",
                    HttpMethod.POST,
                    new HttpEntity<>(songsCreateDTO, getAuthHeaders()),
                    listSongResponseType,
                    new HashMap<>());
            return HttpStatus.OK.equals(entity.getStatusCode()) ?
                    entity.getBody().getData() : Collections.emptyList();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    private HttpHeaders getAuthHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + SecurityContextHelper.getToken());
        return headers;
    }
}
