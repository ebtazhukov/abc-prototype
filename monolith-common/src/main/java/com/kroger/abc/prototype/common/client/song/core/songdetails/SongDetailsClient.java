package com.kroger.abc.prototype.common.client.song.core.songdetails;

import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;
import java.util.UUID;

public interface SongDetailsClient {

    List<SongDetailsDTO> findAll(Projections projections, SongFilters songFilters);

    SongDetailsDTO findOne(UUID id, Projections projections);

    SongDetailsDTO update(SongDetailsDTO songDetailsDTO);

    SongDetailsDTO create(SongDetailsCreateDTO songDetailsCreateDTO);
}
