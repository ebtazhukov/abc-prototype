package com.kroger.abc.prototype.common.client.song.core.songdetails;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDetailsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongFilters;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.SecurityContextHelper;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SongDetailsClientImpl implements SongDetailsClient {

    private final RestTemplate restTemplate;
    @Value("${abc.prototype.client.core.songs.details}")
    private String serviceURL;
    private ParameterizedTypeReference<ApiResponseBodyDeprecated<List<SongDetailsDTO>>> listSongDetailsResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<List<SongDetailsDTO>>>() {
            };
    private ParameterizedTypeReference<ApiResponseBodyDeprecated<SongDetailsDTO>> songDetailsResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<SongDetailsDTO>>() {
            };

    public SongDetailsClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<SongDetailsDTO> findAll(Projections projections, SongFilters songFilters) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<List<SongDetailsDTO>>> entity = restTemplate.exchange(
                    serviceURL + QueryParametersHelper.getUrlBindings(projections, songFilters),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    listSongDetailsResponseType,
                    QueryParametersHelper.getValuesMap(projections, songFilters));
            return HttpStatus.OK.equals(entity.getStatusCode()) ?
                    entity.getBody().getData() : Collections.emptyList();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    @Override
    public SongDetailsDTO findOne(UUID id, Projections projections) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<SongDetailsDTO>> entity = restTemplate.exchange(
                    serviceURL + "/" + id + QueryParametersHelper.getUrlBindings(projections),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    songDetailsResponseType,
                    QueryParametersHelper.getValuesMap(projections));
            return HttpStatus.OK.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public SongDetailsDTO update(SongDetailsDTO songDetailsDTO) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<SongDetailsDTO>> entity = restTemplate.exchange(
                    serviceURL, HttpMethod.PUT, new HttpEntity<>(songDetailsDTO, getAuthHeaders()), songDetailsResponseType);
            return HttpStatus.OK.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public SongDetailsDTO create(SongDetailsCreateDTO songDetailsCreateDTO) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<SongDetailsDTO>> entity = restTemplate.exchange(
                    serviceURL, HttpMethod.POST, new HttpEntity<>(songDetailsCreateDTO, getAuthHeaders()), songDetailsResponseType);
            return HttpStatus.CREATED.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    private HttpHeaders getAuthHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + SecurityContextHelper.getToken());
        return headers;
    }

}
