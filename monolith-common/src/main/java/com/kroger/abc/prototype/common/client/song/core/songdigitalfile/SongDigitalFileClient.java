package com.kroger.abc.prototype.common.client.song.core.songdigitalfile;

import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileDTO;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;
import java.util.UUID;

public interface SongDigitalFileClient {

    List<SongDigitalFileDTO> findBySongIds(List<UUID> songIds, Projections projections);

    SongDigitalFileDTO create(SongDigitalFileCreateDTO digitalFileCreateDTO);
}
