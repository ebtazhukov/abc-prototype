package com.kroger.abc.prototype.common.client.song.core.songdigitalfile;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongDigitalFileDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongIdFilter;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.SecurityContextHelper;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SongDigitalFileClientImpl implements SongDigitalFileClient {

    private final RestTemplate restTemplate;
    @Value("${abc.prototype.client.core.songs.digital-files}")
    private String serviceURL;

    private ParameterizedTypeReference<ApiResponseBodyDeprecated<SongDigitalFileDTO>> songDigitalFileResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<SongDigitalFileDTO>>() {
            };

    private ParameterizedTypeReference<ApiResponseBodyDeprecated<List<SongDigitalFileDTO>>> listSongDigitalFileResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<List<SongDigitalFileDTO>>>() {
            };

    public SongDigitalFileClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<SongDigitalFileDTO> findBySongIds(List<UUID> songIds, Projections projections) {
        SongIdFilter songIdFilter = new SongIdFilter();
        songIdFilter.setIds(songIds);

        try {
            ResponseEntity<ApiResponseBodyDeprecated<List<SongDigitalFileDTO>>> entity = restTemplate.exchange(
                    serviceURL + QueryParametersHelper.getUrlBindings(projections, songIdFilter),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    listSongDigitalFileResponseType,
                    QueryParametersHelper.getValuesMap(projections, songIdFilter));
            return HttpStatus.OK.equals(entity.getStatusCode()) ?
                    entity.getBody().getData() : Collections.emptyList();
        } catch (Exception ignored) {
            return Collections.emptyList();
        }
    }

    @Override
    public SongDigitalFileDTO create(SongDigitalFileCreateDTO digitalFileCreateDTO) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<SongDigitalFileDTO>> entity = restTemplate.exchange(
                    serviceURL, HttpMethod.POST, new HttpEntity<>(digitalFileCreateDTO, getAuthHeaders()), songDigitalFileResponseType);
            return HttpStatus.CREATED.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    private HttpHeaders getAuthHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + SecurityContextHelper.getToken());
        return headers;
    }
}
