package com.kroger.abc.prototype.common.client.song.core.songlyrics;

import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsDTO;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.List;
import java.util.UUID;

public interface SongLyricsClient {

    List<SongLyricsDTO> findBySongIds(List<UUID> songIds, Projections projections);

    SongLyricsDTO create(SongLyricsCreateDTO lyricsCreateDTO);
}
