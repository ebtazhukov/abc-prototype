package com.kroger.abc.prototype.common.client.song.core.songlyrics;

import com.kroger.abc.prototype.common.rest.ApiResponseBodyDeprecated;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsCreateDTO;
import com.kroger.abc.prototype.common.rest.dto.song.core.SongLyricsDTO;
import com.kroger.abc.prototype.common.rest.param.filter.SongIdFilter;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.param.util.QueryParametersHelper;
import com.kroger.abc.prototype.common.rest.util.SecurityContextHelper;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SongLyricsClientImpl implements SongLyricsClient {

    private final RestTemplate restTemplate;
    @Value("${abc.prototype.client.core.songs.lyrics}")
    private String serviceURL;
    private ParameterizedTypeReference<ApiResponseBodyDeprecated<SongLyricsDTO>> songLyricsResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<SongLyricsDTO>>() {
            };
    private ParameterizedTypeReference<ApiResponseBodyDeprecated<List<SongLyricsDTO>>> listSongLyricsResponseType =
            new ParameterizedTypeReference<ApiResponseBodyDeprecated<List<SongLyricsDTO>>>() {
            };

    public SongLyricsClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<SongLyricsDTO> findBySongIds(List<UUID> songIds, Projections projections) {
        SongIdFilter songIdFilter = new SongIdFilter();
        songIdFilter.setIds(songIds);

        try {
            ResponseEntity<ApiResponseBodyDeprecated<List<SongLyricsDTO>>> entity = restTemplate.exchange(
                    serviceURL + QueryParametersHelper.getUrlBindings(projections, songIdFilter),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    listSongLyricsResponseType,
                    QueryParametersHelper.getValuesMap(projections, songIdFilter));
            return HttpStatus.OK.equals(entity.getStatusCode()) ?
                    entity.getBody().getData() : Collections.emptyList();
        } catch (Exception ignored) {
            return Collections.emptyList();
        }
    }

    @Override
    public SongLyricsDTO create(SongLyricsCreateDTO lyricsCreateDTO) {
        try {
            ResponseEntity<ApiResponseBodyDeprecated<SongLyricsDTO>> entity = restTemplate.exchange(
                    serviceURL, HttpMethod.POST, new HttpEntity<>(lyricsCreateDTO, getAuthHeaders()), songLyricsResponseType);
            return HttpStatus.CREATED.equals(entity.getStatusCode()) ? entity.getBody().getData() : null;
        } catch (Exception e) {
            return null;
        }
    }

    private HttpHeaders getAuthHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + SecurityContextHelper.getToken());
        return headers;
    }
}
