package com.kroger.abc.prototype.common.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class ApiRequestBody<D> {

    @JsonInclude(JsonInclude.Include.ALWAYS)
    @JsonProperty(value = "data", required = true)
    private final D data;

    @JsonCreator
    public ApiRequestBody(@JsonProperty("data") D data) {
        this.data = data;
    }

}
