package com.kroger.abc.prototype.common.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kroger.abc.prototype.common.rest.dto.MetadataDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class ApiResponseBody<D, I> {

    @JsonInclude(JsonInclude.Include.ALWAYS)
    @JsonProperty(value = "data", required = true)
    private final D data;

    @JsonInclude(JsonInclude.Include.ALWAYS)
    @JsonProperty(value = "included", defaultValue = "{}")
    private final I included;

    @JsonInclude(JsonInclude.Include.ALWAYS)
    @JsonProperty(value = "meta", defaultValue = "{}", required = true)
    private final MetadataDTO meta;

    @JsonCreator
    public ApiResponseBody(@JsonProperty("data") D data,
                           @JsonProperty("included") I included,
                           @JsonProperty("meta") MetadataDTO meta) {
        this.data = data;
        this.included = included;
        this.meta = meta;
    }

    public static <T, I> ApiResponseBody<T, I> of(T data, I included, MetadataDTO meta) {
        return new ApiResponseBody<>(data, included, meta);
    }

}
