package com.kroger.abc.prototype.common.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

@Value
@ToString
@EqualsAndHashCode
public class ApiResponseBodyDeprecated<T> {

    @JsonInclude(JsonInclude.Include.ALWAYS)
    @JsonProperty(value = "data", required = true)
    private final T data;

    @JsonInclude(JsonInclude.Include.ALWAYS)
    @JsonProperty(value = "meta", defaultValue = "{}", required = true)
    private final Metadata meta;

    private ApiResponseBodyDeprecated(T data) {
        this(new Metadata(), data);
    }

    @JsonCreator
    public ApiResponseBodyDeprecated(@JsonProperty("meta") Metadata meta, @JsonProperty("data") T data) {
        this.data = data;
        this.meta = meta;
    }

    public static <T> ApiResponseBodyDeprecated<T> of(T data) {
        return new ApiResponseBodyDeprecated<>(data);
    }

    public static final class Metadata {

    }

}
