package com.kroger.abc.prototype.common.rest.dto;

import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import com.kroger.abc.prototype.common.rest.util.projection.Inclusion;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Metadata")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MetadataDTO {

    @ApiModelProperty(value = "**Included domains and their projections.**\n\n",
            example = "{\"domain1\" : {\"subEntity1\": \"full|compact\", \"subEntity2\": \"full|compact\"}," +
                    "\"domain2\" : {\"subEntity\": \"full|compact\"}}")
    private Map<String, Map<String, String>> projections = new HashMap<>();

    public static MetadataDTO fromInclusion(Inclusion inclusion) {
        MetadataDTO metadata = new MetadataDTO();

        for (Map.Entry<String, Projections> entry : inclusion.getValue().entrySet()) {
            metadata.getProjections().put(entry.getKey(), entry.getValue().extract().getValue());
        }

        return metadata;
    }

}
