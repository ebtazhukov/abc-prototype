package com.kroger.abc.prototype.common.rest.dto.account;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the Account entity.
 */
@ApiModel(description = "Account")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountDTO implements Serializable {

    @ApiModelProperty(value = "**Entity type**\n\n",
            example = "accounts")
    private final String type = "accounts";

    @NotNull
    @ApiModelProperty(value = "**Unique username in a global context**\n\n",
            required = true,
            example = "user1",
            position = 1
    )
    private String username;

    @ApiModelProperty(
            value = "**Timestamp when account entity was created**\n\n",
            example = "2018-02-13T16:36:48.691Z")
    private Instant createdAt;

    @ApiModelProperty(
            value = "**Timestamp when account entity was modified**\n\n",
            example = "2018-02-13T16:36:48.691Z")
    private Instant modifiedAt;

    @ApiModelProperty(
            value = "**User first name**\n\n",
            example = "John")
    private String firstName;

    @ApiModelProperty(
            value = "**User last name**\n\n",
            example = "Doe")
    private String lastName;

    @ApiModelProperty(
            value = "**Whether user have a paid subscription**\n\n",
            example = "true")
    private Boolean isSubscribed;
}
