package com.kroger.abc.prototype.common.rest.dto.album.business;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

@ApiModel(description = "Album creation entity")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumCreateDTO implements Serializable {

    @ApiModelProperty(value = "**Title of the album**", example = "The wall", required = true)
    @NotNull
    private String title;

    @ApiModelProperty(value = "**Artist of the album**\n\n" +
            "Artist lookup by name will be performed. If artist doesn't exist it will be created.",
            example = "Queen", required = true)
    @NotNull
    private String artist;

    @ApiModelProperty(value = "**Liner notes for the album**", example = "Liner notes for the album")
    private String notes;

    @ApiModelProperty(value = "**Songs to be added into album**\n\n" +
            "Song lookup by title, artist and duration will be performed. If song doesn't exist it will be created.")
    @Valid
    @NotEmpty
    private List<AlbumSongCreateDTO> songs;

}
