package com.kroger.abc.prototype.common.rest.dto.album.business;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Song to be added into album while it's created")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumSongCreateDTO implements Serializable {

    @ApiModelProperty(value = "**Title of the song**", example = "Sweet Emotion", required = true)
    @NotNull
    private String title;

    @ApiModelProperty(value = "**Artist of the song**\n\n" +
            "Artist lookup by name will be performed. If artist doesn't exist it will be created.",
            example = "Queen", required = true)
    @NotNull
    private String artist;

    @ApiModelProperty(value = "**Duration of the song in seconds**", example = "234", required = true)
    @NotNull
    private Long duration;

    @ApiModelProperty(value = "**Bitrate of the song**", example = "128")
    private Integer bitrate;

    @ApiModelProperty(value = "**Is digital file for this song available only to users with active subscription**",
            example = "true")
    private Boolean subscribeOnly;

    @ApiModelProperty(value = "**Lyrics for the song**", example = "Bla bla bla, an awesome song...")
    private String lyrics;

    @ApiModelProperty(value = "**Digital file URL for the song**", example = "https://filestore.com/123")
    private String fileURL;
}
