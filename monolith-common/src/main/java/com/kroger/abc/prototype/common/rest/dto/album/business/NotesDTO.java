package com.kroger.abc.prototype.common.rest.dto.album.business;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the Albums's liner notes entity.
 */
@ApiModel(description = "Liner notes")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotesDTO {

    @ApiModelProperty(
            value = "**Liner notes for the album**\n\n",
            example = "Liner notes for the album")
    private String notes;
}
