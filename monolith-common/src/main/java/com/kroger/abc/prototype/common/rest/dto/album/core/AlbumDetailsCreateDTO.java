package com.kroger.abc.prototype.common.rest.dto.album.core;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Album details creation entity")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumDetailsCreateDTO implements Serializable {

    @ApiModelProperty(value = "**Title of the album**", example = "The wall")
    private String title;

    @ApiModelProperty(value = "**Album relationships object**")
    private AlbumRelationships relationships;

}
