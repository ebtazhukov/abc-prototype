package com.kroger.abc.prototype.common.rest.dto.album.core;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the AlbumDetails entity.
 */
@ApiModel(description = "Album details")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumDetailsDTO implements Serializable {

    @NotNull
    @ApiModelProperty(value = "**Unique ID of the album in a global context**\n\n" +
            "Shapes: full, compact",
            required = true,
            example = "67e3ab40-27fd-4435-9bf5-cb58a9820bf1",
            position = 1
    )
    private UUID id;

    @ApiModelProperty(
            value = "**Timestamp when album entity was created**\n\n" +
                    "Shapes: full",
            example = "2018-02-13T16:36:48.691Z")
    private Instant createdAt;

    @ApiModelProperty(
            value = "**Timestamp when album entity was modified**\n\n" +
                    "Shapes: full",
            example = "2018-02-13T16:36:48.691Z")
    private Instant modifiedAt;

    @ApiModelProperty(
            value = "**Title of the album**\n\n" +
                    "Shapes: full, compact",
            example = "The wall")
    private String title;

    @ApiModelProperty(
            value = "**Album relationships object**\n\n" +
                    "Shapes: full")
    private AlbumRelationships relationships;

}
