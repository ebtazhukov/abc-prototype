package com.kroger.abc.prototype.common.rest.dto.album.core;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Album liner notes creation entity")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumNotesCreateDTO implements Serializable {

    @NotNull
    @ApiModelProperty(value = "**Unique ID of the album in a global context**",
            required = true,
            example = "67e3ab40-27fd-4435-9bf5-cb58a9820bf1",
            position = 1
    )
    private UUID albumId;

    @NotNull
    @ApiModelProperty(
            value = "**Liner notes for the album**",
            example = "Liner notes for the album",
            required = true)
    private String notes;

}
