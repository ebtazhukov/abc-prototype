package com.kroger.abc.prototype.common.rest.dto.album.core;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the AlbumNotes entity.
 */
@ApiModel(description = "Album liner notes")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumNotesDTO implements Serializable {

    @NotNull
    @ApiModelProperty(value = "**Unique ID of the album notes entity in a global context**\n\n" +
            "Shapes: full, compact",
            required = true,
            example = "67e3ab40-27fd-4435-9bf5-cb58a9820bf1",
            position = 1
    )
    private UUID id;

    @NotNull
    @ApiModelProperty(value = "**Unique ID of the album in a global context**\n\n" +
            "Shapes: full, compact",
            required = true,
            example = "67e3ab40-27fd-4435-9bf5-cb58a9820bf1",
            position = 1
    )
    private UUID albumId;

    @ApiModelProperty(
            value = "**Liner notes for the album**\n\n" +
                    "Shapes: full, compact (first 13 symbols)",
            example = "Liner notes for the album")
    private String notes;

}
