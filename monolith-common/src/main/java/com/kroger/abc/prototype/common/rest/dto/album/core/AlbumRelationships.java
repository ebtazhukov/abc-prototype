package com.kroger.abc.prototype.common.rest.dto.album.core;

import com.kroger.abc.prototype.common.rest.dto.song.core.ArtistReference;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumRelationships {

    @ApiModelProperty(value = "**Artist reference**\n\n")
    private ArtistReference artist;

    @ApiModelProperty(value = "**Songs references**\n\n")
    private List<SongReference> songs;

    public static AlbumRelationships build(UUID artistId, List<UUID> songIds) {
        AlbumRelationships result = new AlbumRelationships();
        if (artistId != null) {
            result.setArtist(new ArtistReference(artistId));
        }
        result.setSongs(
                CollectionUtils.emptyIfNull(songIds).stream()
                        .map(SongReference::new)
                        .collect(Collectors.toList()));
        return result;
    }

    public static UUID extractArtist(AlbumRelationships albumRelationships) {
        return albumRelationships == null ? null :
                albumRelationships.getArtist() == null ? null : albumRelationships.getArtist().getId();
    }

    public static List<UUID> extractSongs(AlbumRelationships albumRelationships) {
        return albumRelationships == null ? new ArrayList<>() :
                CollectionUtils.emptyIfNull(albumRelationships.getSongs()).stream()
                        .map(SongReference::getId)
                        .collect(Collectors.toList());
    }
}
