package com.kroger.abc.prototype.common.rest.dto.artist.app;

import com.kroger.abc.prototype.common.rest.ApiResponseBody;
import com.kroger.abc.prototype.common.rest.dto.MetadataDTO;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;
import java.util.List;

public class ArtistListResponseBody extends ApiResponseBody<List<ArtistDTO>, Object> {

    private ArtistListResponseBody(List<ArtistDTO> data, MetadataDTO metadata) {
        super(data, new Object(), metadata);
    }

    public static ArtistListResponseBody of(List<ArtistDTO> data, MetadataDTO metadata) {
        return new ArtistListResponseBody(data, metadata);
    }
}
