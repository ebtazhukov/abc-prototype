package com.kroger.abc.prototype.common.rest.dto.artist.app;

import com.kroger.abc.prototype.common.rest.ApiResponseBody;
import com.kroger.abc.prototype.common.rest.dto.MetadataDTO;
import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;

public class ArtistResponseBody extends ApiResponseBody<ArtistDTO, Object> {

    private ArtistResponseBody(ArtistDTO data, MetadataDTO metadata) {
        super(data, new Object(), metadata);
    }

    public static ArtistResponseBody of(ArtistDTO data, MetadataDTO metadata) {
        return new ArtistResponseBody(data, metadata);
    }
}
