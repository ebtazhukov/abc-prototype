package com.kroger.abc.prototype.common.rest.dto.artist.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Artist creation entity")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArtistCreateDTO implements Serializable {

    @ApiModelProperty(value = "**Name of the artist**", example = "Queen")
    private String name;
}