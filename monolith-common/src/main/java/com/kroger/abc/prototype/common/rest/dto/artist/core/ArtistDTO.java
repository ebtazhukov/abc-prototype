package com.kroger.abc.prototype.common.rest.dto.artist.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the Artist entity.
 */
@ApiModel(description = "Artist")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArtistDTO implements Serializable {

    @ApiModelProperty(value = "**Entity type**\n\n" +
            "Shapes: full, compact",
            example = "artists")
    private final String type = "artists";

    @NotNull
    @ApiModelProperty(value = "**Unique ID of the artist in a global context**\n\n" +
            "Shapes: full, compact",
            required = true,
            example = "67e3ab40-27fd-4435-9bf5-cb58a9820bf1",
            position = 1
    )
    private UUID id;

    @ApiModelProperty(
            value = "**Timestamp when artist entity was created**\n\n" +
                    "Shapes: full",
            example = "2018-02-13T16:36:48.691Z")
    private Instant createdAt;

    @ApiModelProperty(
            value = "**Timestamp when artist entity was modified**\n\n" +
                    "Shapes: full",
            example = "2018-02-13T16:36:48.691Z")
    private Instant modifiedAt;

    @ApiModelProperty(
            value = "**Name of the artist**\n\n" +
                    "Shapes: full, compact",
            example = "Queen")
    private String name;
}