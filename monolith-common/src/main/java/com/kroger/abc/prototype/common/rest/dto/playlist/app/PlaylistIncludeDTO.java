package com.kroger.abc.prototype.common.rest.dto.playlist.app;

import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Song inclusions")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlaylistIncludeDTO {

    @ApiModelProperty(value = "**Songs referenced from playlists**\n\n")
    private List<SongDTO> songs;

    @ApiModelProperty(value = "**Artists referenced from songs**\n\n")
    private List<ArtistDTO> artists;
}
