package com.kroger.abc.prototype.common.rest.dto.playlist.app;

import com.kroger.abc.prototype.common.rest.ApiResponseBody;
import com.kroger.abc.prototype.common.rest.dto.MetadataDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistDTO;
import java.util.List;

public class PlaylistListResponseBody extends ApiResponseBody<List<PlaylistDTO>, PlaylistIncludeDTO> {

    private PlaylistListResponseBody(List<PlaylistDTO> data, PlaylistIncludeDTO included, MetadataDTO metadata) {
        super(data, included, metadata);
    }

    public static PlaylistListResponseBody of(List<PlaylistDTO> data, PlaylistIncludeDTO included, MetadataDTO metadata) {
        return new PlaylistListResponseBody(data, included, metadata);
    }
}
