package com.kroger.abc.prototype.common.rest.dto.playlist.app;

import com.kroger.abc.prototype.common.rest.ApiResponseBody;
import com.kroger.abc.prototype.common.rest.dto.MetadataDTO;
import com.kroger.abc.prototype.common.rest.dto.playlist.core.PlaylistDTO;

public class PlaylistResponseBody extends ApiResponseBody<PlaylistDTO, PlaylistIncludeDTO> {

    private PlaylistResponseBody(PlaylistDTO data, PlaylistIncludeDTO included, MetadataDTO metadata) {
        super(data, included, metadata);
    }

    public static PlaylistResponseBody of(PlaylistDTO data, PlaylistIncludeDTO included, MetadataDTO metadata) {
        return new PlaylistResponseBody(data, included, metadata);
    }
}
