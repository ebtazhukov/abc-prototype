package com.kroger.abc.prototype.common.rest.dto.playlist.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the create Playlist entity.
 */
@ApiModel(description = "Playlist create")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlaylistCreateDTO implements Serializable {

    @ApiModelProperty(
            value = "**Title of the playlist**\n\n",
            example = "Awesome music")
    private String title;

    @ApiModelProperty(value = "**Playlist relationships object**\n\n")
    private PlaylistRelationships relationships;
}
