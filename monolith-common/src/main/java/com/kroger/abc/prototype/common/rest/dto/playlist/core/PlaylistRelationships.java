package com.kroger.abc.prototype.common.rest.dto.playlist.core;

import com.kroger.abc.prototype.common.rest.dto.album.core.SongReference;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlaylistRelationships {

    @ApiModelProperty(value = "**Songs references**\n\n")
    private List<SongReference> songs;

    public static PlaylistRelationships build(List<UUID> songIds) {
        PlaylistRelationships result = new PlaylistRelationships();
        result.setSongs(
                CollectionUtils.emptyIfNull(songIds).stream()
                        .map(SongReference::new)
                        .collect(Collectors.toList()));
        return result;
    }

    public static List<UUID> extractSongs(PlaylistRelationships albumRelationships) {
        return albumRelationships == null ? new ArrayList<>() :
                CollectionUtils.emptyIfNull(albumRelationships.getSongs()).stream()
                        .map(SongReference::getId)
                        .collect(Collectors.toList());
    }
}
