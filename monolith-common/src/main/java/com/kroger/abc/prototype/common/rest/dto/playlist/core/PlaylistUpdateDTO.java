package com.kroger.abc.prototype.common.rest.dto.playlist.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the update Playlist entity.
 */
@ApiModel(description = "Playlist update")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlaylistUpdateDTO implements Serializable {

    @NotNull
    @ApiModelProperty(value = "**Unique ID of the playlist in a global context**\n\n",
            required = true,
            example = "67e3ab40-27fd-4435-9bf5-cb58a9820bf1",
            position = 1
    )
    private UUID id;

    @ApiModelProperty(
            value = "**Title of the playlist**\n\n",
            example = "Awesome music")
    private String title;

    @ApiModelProperty(value = "**Playlist relationships object**\n\n")
    private PlaylistRelationships relationships;
}
