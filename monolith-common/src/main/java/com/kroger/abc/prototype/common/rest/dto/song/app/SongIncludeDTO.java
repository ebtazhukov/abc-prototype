package com.kroger.abc.prototype.common.rest.dto.song.app;

import com.kroger.abc.prototype.common.rest.dto.artist.core.ArtistDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Song inclusions")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SongIncludeDTO {

    @ApiModelProperty(value = "**Artists referenced from songs**\n\n")
    private List<ArtistDTO> artists = new ArrayList<>();
}
