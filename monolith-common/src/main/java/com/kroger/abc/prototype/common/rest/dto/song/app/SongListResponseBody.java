package com.kroger.abc.prototype.common.rest.dto.song.app;

import com.kroger.abc.prototype.common.rest.ApiResponseBody;
import com.kroger.abc.prototype.common.rest.dto.MetadataDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;
import java.util.List;

public class SongListResponseBody extends ApiResponseBody<List<SongDTO>, SongIncludeDTO> {

    private SongListResponseBody(List<SongDTO> data, SongIncludeDTO included, MetadataDTO metadata) {
        super(data, included, metadata);
    }

    public static SongListResponseBody of(List<SongDTO> data, SongIncludeDTO included, MetadataDTO metadata) {
        return new SongListResponseBody(data, included, metadata);
    }
}
