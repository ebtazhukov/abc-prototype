package com.kroger.abc.prototype.common.rest.dto.song.app;

import com.kroger.abc.prototype.common.rest.ApiResponseBody;
import com.kroger.abc.prototype.common.rest.dto.MetadataDTO;
import com.kroger.abc.prototype.common.rest.dto.song.business.SongDTO;

public class SongResponseBody extends ApiResponseBody<SongDTO, SongIncludeDTO> {

    private SongResponseBody(SongDTO data, SongIncludeDTO included, MetadataDTO metadata) {
        super(data, included, metadata);
    }

    public static SongResponseBody of(SongDTO data, SongIncludeDTO included, MetadataDTO metadata) {
        return new SongResponseBody(data, included, metadata);
    }
}
