package com.kroger.abc.prototype.common.rest.dto.song.business;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the Song's digital file entity.
 */
@ApiModel(description = "Song Digital file")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DigitalFileDTO {

    @ApiModelProperty(
            value = "**Digital file URL for the related song**\n\n",
            example = "https://filestore.com/123")
    private String fileURL;
}
