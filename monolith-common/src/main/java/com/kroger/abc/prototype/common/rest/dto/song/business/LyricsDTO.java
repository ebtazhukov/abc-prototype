package com.kroger.abc.prototype.common.rest.dto.song.business;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the Song's lyrics entity.
 */
@ApiModel(description = "Song Lyrics")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LyricsDTO {

    @ApiModelProperty(
            value = "**Lyrics for the related song**\n\n" +
                    "Shapes: full, compact (first 13 symbols)",
            example = "Bla bla bla, an awesome song...")
    private String lyrics;
}
