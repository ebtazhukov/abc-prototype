package com.kroger.abc.prototype.common.rest.dto.song.business;


import com.kroger.abc.prototype.common.rest.dto.song.core.SongRelationships;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Song creation object")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SongCreateDTO implements Serializable {

    @ApiModelProperty(value = "**Title of the song**", example = "Sweet Emotion")
    private String title;

    @ApiModelProperty(value = "**Song relationships object**")
    private SongRelationships relationships;

    @ApiModelProperty(value = "**Duration of the song in seconds**", example = "234")
    private Long duration;

    @ApiModelProperty(value = "**Bitrate of the song**", example = "128")
    private Integer bitrate;

    @ApiModelProperty(value = "**Is digital file for this song available only to users with active subscription**",
            example = "true")
    private Boolean subscribeOnly;

    @ApiModelProperty(value = "**Lyrics for the related song**", example = "Bla bla bla, an awesome song...")
    private String lyrics;

    @ApiModelProperty(value = "**Digital file URL for the related song**", example = "https://filestore.com/123")
    private String fileURL;

}
