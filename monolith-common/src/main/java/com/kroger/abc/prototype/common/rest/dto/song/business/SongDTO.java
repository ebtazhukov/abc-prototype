package com.kroger.abc.prototype.common.rest.dto.song.business;


import com.kroger.abc.prototype.common.rest.dto.song.core.SongRelationships;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the Song entity.
 */
@ApiModel(description = "Song")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SongDTO implements Serializable {

    @ApiModelProperty(value = "**Entity type**\n\n" +
            "Shapes: full, compact",
            example = "songs")
    private final String type = "songs";

    @NotNull
    @ApiModelProperty(value = "**Unique ID of the song in a global context**\n\n" +
            "Shapes: full, compact",
            required = true,
            example = "67e3ab40-27fd-4435-9bf5-cb58a9820bf1",
            position = 1
    )
    private UUID id;

    @ApiModelProperty(
            value = "**Timestamp when song entity was created**\n\n" +
                    "Shapes: full",
            example = "2018-02-13T16:36:48.691Z")
    private Instant createdAt;

    @ApiModelProperty(
            value = "**Timestamp when song entity was modified**\n\n" +
                    "Shapes: full",
            example = "2018-02-13T16:36:48.691Z")
    private Instant modifiedAt;

    @ApiModelProperty(
            value = "**Title of the song**\n\n" +
                    "Shapes: full, compact",
            example = "Sweet Emotion")
    private String title;

    @ApiModelProperty(
            value = "**Song relationships object**\n\n" +
                    "Shapes: full, compact")
    private SongRelationships relationships;

    @ApiModelProperty(
            value = "**Duration of the song in seconds**\n\n" +
                    "Shapes: full",
            example = "234")
    private Long duration;

    @ApiModelProperty(
            value = "**Bitrate of the song**\n\n" +
                    "Shapes: full",
            example = "128")
    private Integer bitrate;

    @ApiModelProperty(
            value = "**Is digital file for this song available only to users with active subscription**\n\n" +
                    "Shapes: full",
            example = "true")
    private Boolean subscribeOnly;

    @ApiModelProperty(
            value = "**Lyrics for the song**\n\n" +
                    "Included only with lyrics projection")
    private LyricsDTO lyrics;

    @ApiModelProperty(
            value = "**Digital file for the song**\n\n" +
                    "Included only with digitalFiles projection")
    private DigitalFileDTO digitalFile;
}
