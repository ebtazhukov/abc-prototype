package com.kroger.abc.prototype.common.rest.dto.song.core;

import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArtistReference {

    @ApiModelProperty(value = "**Type of referenced entity**\n\n", required = true, example = "artists")
    private final String type = "artists";

    @ApiModelProperty(value = "**Unique ID of the referenced entity in a global context**\n\n",
            required = true, example = "67e3ab40-27fd-4435-9bf5-cb58a9820bf1")
    private UUID id;
}
