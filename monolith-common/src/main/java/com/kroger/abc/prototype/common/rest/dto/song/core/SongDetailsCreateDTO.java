package com.kroger.abc.prototype.common.rest.dto.song.core;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Song details creation entity")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SongDetailsCreateDTO implements Serializable {

    @ApiModelProperty(value = "**Title of the song**", example = "Sweet Emotion")
    private String title;

    @ApiModelProperty(value = "**Song relationships object**")
    private SongRelationships relationships;

    @ApiModelProperty(value = "**Duration of the song in seconds**\n\n", example = "234")
    private Long duration;

    @ApiModelProperty(value = "**Bitrate of the song**", example = "128")
    private Integer bitrate;

    @ApiModelProperty(value = "**Is digital file for this song available only to users with active subscription**",
            example = "true")

    private Boolean subscribeOnly;
}
