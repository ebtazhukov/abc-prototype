package com.kroger.abc.prototype.common.rest.dto.song.core;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the SongDigitalFile entity.
 */
@ApiModel(description = "Song digital file")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SongDigitalFileDTO implements Serializable {

    @NotNull
    @ApiModelProperty(value = "**Unique ID of the song digital file entity in a global context**\n\n",
            required = true,
            example = "67e3ab40-27fd-4435-9bf5-cb58a9820bf1",
            position = 1
    )
    private UUID id;

    @NotNull
    @ApiModelProperty(value = "**Unique ID of the song in a global context**\n\n",
            required = true,
            example = "67e3ab40-27fd-4435-9bf5-cb58a9820bf1",
            position = 1
    )
    private UUID songId;

    @ApiModelProperty(
            value = "**URL to the song digital file**\n\n",
            example = "http://filestore.com/Hotel California")
    private String url;

}
