package com.kroger.abc.prototype.common.rest.dto.song.core;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Song lyrics creation entity")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SongLyricsCreateDTO implements Serializable {

    @NotNull
    @ApiModelProperty(value = "**Unique ID of the song in a global context**",
            required = true,
            example = "67e3ab40-27fd-4435-9bf5-cb58a9820bf1",
            position = 1
    )
    private UUID songId;

    @ApiModelProperty(value = "**Lyrics of the song**",
            example = "Hotel California lyrics")
    private String lyrics;

}
