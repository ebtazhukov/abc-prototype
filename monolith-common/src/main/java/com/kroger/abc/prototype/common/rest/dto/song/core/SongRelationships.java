package com.kroger.abc.prototype.common.rest.dto.song.core;

import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SongRelationships {

    @ApiModelProperty(value = "**Artist relationship**\n\n")
    private ArtistReference artist;

    public static SongRelationships build(UUID artistId) {
        return artistId == null ? null : new SongRelationships(new ArtistReference(artistId));
    }

    public static UUID extractArtist(SongRelationships songRelationships) {
        return songRelationships == null ? null :
                songRelationships.getArtist() == null ? null : songRelationships.getArtist().getId();
    }
}
