package com.kroger.abc.prototype.common.rest.mapper;

import java.util.List;

/**
 * Contract for a generic dto to entity mapper.
 *
 * @param <S> - source DTO type parameter.
 * @param <D> - destination DTO type parameter.
 */

public interface DTOMapper<S, D> {

    S toSource(D dto);

    D toDestination(S dto);

    List<S> toSource(List<D> dtoList);

    List<D> toDestination(List<S> dtoList);
}
