package com.kroger.abc.prototype.common.rest.param.filter;

import com.kroger.abc.prototype.common.rest.param.name.ParamName;
import com.kroger.abc.prototype.common.rest.param.util.QueryParameterModel;
import io.swagger.annotations.ApiParam;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;

@Getter
@Setter
public class AlbumIdFilter implements QueryParameterModel {

    private static final String ID_IN_FILTER = "filter.album.id";

    @ApiParam(name = ID_IN_FILTER, value = "**Comma-separated list of album ids to filter by.**",
            example = "87b8e302-0acc-43ae-b5a8-22b0d512c8aa,03f23554-843c-49bc-a517-a88684c00924")
    //TODO: deal with validation
    @ParamName(ID_IN_FILTER)
    private List<UUID> ids;

    @Override
    public List<String> getUrlBindings() {
        return CollectionUtils.isEmpty(ids) ?
                Collections.emptyList() : Collections.singletonList(ID_IN_FILTER + "={" + ID_IN_FILTER + "}");
    }

    @Override
    public Map<String, String> getValuesMap() {
        Map<String, String> result = new HashMap<>();
        if (CollectionUtils.isNotEmpty(ids)) {
            result.put(ID_IN_FILTER, ids.stream().map(UUID::toString).collect(Collectors.joining(",")));
        }
        return result;
    }
}
