package com.kroger.abc.prototype.common.rest.param.filter;

import com.kroger.abc.prototype.common.rest.param.name.ParamName;
import com.kroger.abc.prototype.common.rest.param.util.QueryParameterModel;
import io.swagger.annotations.ApiParam;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class ArtistFilters implements QueryParameterModel {

    private static final String ID_IN_FILTER = "filter.id";
    private static final String NAME_CONTAINS_FILTER = "filter.name.contains";

    @ApiParam(name = ID_IN_FILTER, value = "**Comma-separated list of artist ids to filter by.** \n\n" +
            "If applied, other filters will be ignored.",
            example = "87b8e302-0acc-43ae-b5a8-22b0d512c8aa,03f23554-843c-49bc-a517-a88684c00924")
    //TODO: deal with validation
    @ParamName(ID_IN_FILTER)
    private List<UUID> ids;

    @ApiParam(name = NAME_CONTAINS_FILTER, value = "**Case-insensitive filter by artist name.**", example = "Queen")
    @ParamName(NAME_CONTAINS_FILTER)
    private String name;

    @Override
    public List<String> getUrlBindings() {
        List<String> result = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(ids)) {
            result.add(ID_IN_FILTER + "={" + ID_IN_FILTER + "}");
        }
        if (StringUtils.isNotBlank(name)) {
            result.add(NAME_CONTAINS_FILTER + "={" + NAME_CONTAINS_FILTER + "}");
        }
        return result;
    }

    @Override
    public Map<String, String> getValuesMap() {
        Map<String, String> result = new HashMap<>();
        if (CollectionUtils.isNotEmpty(ids)) {
            result.put(ID_IN_FILTER, ids.stream().map(UUID::toString).collect(Collectors.joining(",")));
        }
        if (StringUtils.isNotBlank(name)) {
            result.put(NAME_CONTAINS_FILTER, name);
        }
        return result;
    }
}
