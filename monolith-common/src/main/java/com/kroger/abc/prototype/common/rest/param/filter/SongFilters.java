package com.kroger.abc.prototype.common.rest.param.filter;

import com.kroger.abc.prototype.common.rest.param.name.ParamName;
import com.kroger.abc.prototype.common.rest.param.util.QueryParameterModel;
import io.swagger.annotations.ApiParam;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class SongFilters implements QueryParameterModel {

    private static final String ID_IN_FILTER = "filter.id";
    private static final String TITLE_CONTAINS_FILTER = "filter.title.contains";
    private static final String ARTIST_ID_IN_FILTER = "filter.artist.id";

    @ApiParam(name = ID_IN_FILTER, value = "**Comma-separated list of song ids to filter by.** \n\n" +
            "If applied, other filters will be ignored.",
            example = "87b8e302-0acc-43ae-b5a8-22b0d512c8aa,03f23554-843c-49bc-a517-a88684c00924")
    //TODO: deal with validation
    @ParamName(ID_IN_FILTER)
    private List<UUID> ids;

    @ApiParam(name = TITLE_CONTAINS_FILTER, example = "Rhapsody", value = "**Case-insensitive filter by song title.** \n\n" +
            "In case of using with '" + ARTIST_ID_IN_FILTER + "', **OR** rule will be applied.")
    @ParamName(TITLE_CONTAINS_FILTER)
    private String title;

    @ApiParam(name = ARTIST_ID_IN_FILTER, value = "**Comma-separated list of artist ids to filter by.** \n\n" +
            "In case of using with '" + TITLE_CONTAINS_FILTER + "', **OR** rule will be applied.",
            example = "87b8e302-0acc-43ae-b5a8-22b0d512c8aa,03f23554-843c-49bc-a517-a88684c00924")
    @ParamName(ARTIST_ID_IN_FILTER)
    private List<UUID> artistIds;

    @Override
    public List<String> getUrlBindings() {
        List<String> result = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(ids)) {
            result.add(ID_IN_FILTER + "={" + ID_IN_FILTER + "}");
        }
        if (StringUtils.isNotBlank(title)) {
            result.add(TITLE_CONTAINS_FILTER + "={" + TITLE_CONTAINS_FILTER + "}");
        }
        if (CollectionUtils.isNotEmpty(artistIds)) {
            result.add(ARTIST_ID_IN_FILTER + "={" + ARTIST_ID_IN_FILTER + "}");
        }
        return result;
    }

    @Override
    public Map<String, String> getValuesMap() {
        Map<String, String> result = new HashMap<>();
        if (CollectionUtils.isNotEmpty(ids)) {
            result.put(ID_IN_FILTER, ids.stream().map(UUID::toString).collect(Collectors.joining(",")));
        }
        if (StringUtils.isNotBlank(title)) {
            result.put(TITLE_CONTAINS_FILTER, title);
        }
        if (CollectionUtils.isNotEmpty(artistIds)) {
            result.put(ARTIST_ID_IN_FILTER, artistIds.stream().map(UUID::toString).collect(Collectors.joining(",")));
        }
        return result;
    }
}
