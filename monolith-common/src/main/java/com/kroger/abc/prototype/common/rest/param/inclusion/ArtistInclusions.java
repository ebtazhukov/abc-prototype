package com.kroger.abc.prototype.common.rest.param.inclusion;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ArtistInclusions extends Inclusions {

    private static final String DEFAULT_VALUE = "artists[artists_compact]";
    private static final String DEFINITION =
            "**Domains to be included in response body along with their projections.** \n\n" +
                    "**Supported domains are following:** \n\n" +
                    "**- artists** with available projections: **artists** \n\n" +
                    "**Examples:** \n\n" +
                    "** - Most complete:** artists[artists_full]";

    @ApiParam(value = DEFINITION, defaultValue = DEFAULT_VALUE)
    private String include = DEFAULT_VALUE;
}
