package com.kroger.abc.prototype.common.rest.param.inclusion;

import com.kroger.abc.prototype.common.rest.param.util.QueryParameterModel;
import com.kroger.abc.prototype.common.rest.util.projection.Inclusion;
import com.kroger.abc.prototype.common.rest.util.projection.InclusionParser;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.Pattern;
import org.apache.commons.lang3.StringUtils;

public abstract class Inclusions implements QueryParameterModel {

    private static final String INCLUDE = "include";

    @Pattern(regexp = "^((\\w+?)\\[((\\w+?)_(compact|full),?)*\\],?)*$", message = "'include' has wrong format (${validatedValue})")
    public abstract String getInclude();

    public abstract void setInclude(String include);

    public Inclusion extract() {
        return InclusionParser.parseFromString(getInclude());
    }

    @Override
    public List<String> getUrlBindings() {
        return StringUtils.isNoneBlank(getInclude()) ?
                Collections.singletonList(INCLUDE + "={" + INCLUDE + "}") : Collections.emptyList();
    }

    @Override
    public Map<String, String> getValuesMap() {
        Map<String, String> result = new HashMap<>();
        if (StringUtils.isNoneBlank(getInclude())) {
            result.put(INCLUDE, getInclude());
        }
        return result;
    }
}
