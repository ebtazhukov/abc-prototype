package com.kroger.abc.prototype.common.rest.param.inclusion;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PlaylistInclusions extends Inclusions {

    private static final String DEFAULT_VALUE = "playlists[playlists_compact]";
    private static final String DEFINITION =
            "**Domains to be included in response body along with their projections.** \n\n" +
                    "**Supported domains are following:** \n\n" +
                    "**- playlists** with available projections: **playlists** \n\n" +
                    "**- songs** with available projections: **details, lyrics and digitalFiles** \n\n" +
                    "**- artists** with available projections: **artists** \n\n" +
                    "**Examples:** \n\n" +
                    "**- Most complete:** playlists[playlists_full],songs[details_full,lyrics_full,digitalFiles_full],artists[artists_full] \n\n" +
                    "**- Minimum valuable with songs and artists included:** playlists[playlists_full],songs[details_compact],artists[artists_compact]";

    @ApiParam(value = DEFINITION, defaultValue = DEFAULT_VALUE)
    private String include = DEFAULT_VALUE;
}
