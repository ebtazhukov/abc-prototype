package com.kroger.abc.prototype.common.rest.param.inclusion;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SongInclusions extends Inclusions {

    private static final String DEFAULT_VALUE = "songs[details_compact]";
    private static final String DEFINITION =
            "**Domains to be included in response body along with their projections.** \n\n" +
                    "**Supported domains are following:** \n\n" +
                    "**- songs** with available projections: **details, lyrics and digitalFiles** \n\n" +
                    "**- artists** with available projections: **artists** \n\n" +
                    "**Examples:** \n\n" +
                    "**- Most complete:** songs[details_full,lyrics_full,digitalFiles_full],artists[artists_full] \n\n" +
                    "**- Minimum valuable with artists included:** songs[details_compact],artists[artists_compact]";

    @ApiParam(value = DEFINITION, defaultValue = DEFAULT_VALUE)
    private String include = DEFAULT_VALUE;
}
