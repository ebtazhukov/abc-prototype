package com.kroger.abc.prototype.common.rest.param.projection;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AlbumDetailsProjections extends Projections {

    private static final String DEFAULT_VALUE = "details_compact";
    private static final String DEFINITION =
            "**Response body shape.** \n\n" +
                    "**Supported projections are following:** \n\n" +
                    "**- details** with available values: **full or compact** \n\n" +
                    "**Examples:** \n\n" +
                    "**- Most complete:** details_full \n\n";

    @ApiParam(value = DEFINITION, defaultValue = DEFAULT_VALUE)
    private String projection = DEFAULT_VALUE;
}
