package com.kroger.abc.prototype.common.rest.param.projection;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AlbumNotesProjections extends Projections {

    private static final String DEFAULT_VALUE = "notes_compact";
    private static final String DEFINITION =
            "**Response body shape.** \n\n" +
                    "**Supported projections are following:** \n\n" +
                    "**- notes** with available values: **full or compact** \n\n" +
                    "**Examples:** \n\n" +
                    "**- Most complete:** notes_full \n\n";

    @ApiParam(value = DEFINITION, defaultValue = DEFAULT_VALUE)
    private String projection = DEFAULT_VALUE;
}
