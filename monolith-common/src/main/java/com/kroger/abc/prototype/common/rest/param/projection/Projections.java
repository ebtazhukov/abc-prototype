package com.kroger.abc.prototype.common.rest.param.projection;

import com.kroger.abc.prototype.common.rest.param.util.QueryParameterModel;
import com.kroger.abc.prototype.common.rest.util.projection.Projection;
import com.kroger.abc.prototype.common.rest.util.projection.ProjectionParser;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.Pattern;
import org.apache.commons.lang3.StringUtils;

public abstract class Projections implements QueryParameterModel {

    private static final String PROJECTION = "projection";

    @Pattern(regexp = "^((\\w+?)_(compact|full),?)*$", message = "'projection' has wrong format (${validatedValue})")
    public abstract String getProjection();

    public abstract void setProjection(String projection);

    public Projection extract() {
        return ProjectionParser.parseFromString(getProjection());
    }

    @Override
    public List<String> getUrlBindings() {
        return StringUtils.isNoneBlank(getProjection()) ?
                Collections.singletonList(PROJECTION + "={" + PROJECTION + "}") : Collections.emptyList();
    }

    @Override
    public Map<String, String> getValuesMap() {
        Map<String, String> result = new HashMap<>();
        if (StringUtils.isNoneBlank(getProjection())) {
            result.put(PROJECTION, getProjection());
        }
        return result;
    }
}
