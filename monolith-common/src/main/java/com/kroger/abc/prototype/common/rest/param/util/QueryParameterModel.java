package com.kroger.abc.prototype.common.rest.param.util;

import java.util.List;
import java.util.Map;

public interface QueryParameterModel {

    List<String> getUrlBindings();

    Map<String, String> getValuesMap();

}
