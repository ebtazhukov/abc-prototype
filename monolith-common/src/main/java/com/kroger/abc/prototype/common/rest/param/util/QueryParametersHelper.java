package com.kroger.abc.prototype.common.rest.param.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class QueryParametersHelper {

    public static String getUrlBindings(QueryParameterModel... models) {
        return Arrays.stream(models)
                .filter(Objects::nonNull)
                .flatMap(model -> model.getUrlBindings().stream())
                .collect(Collectors.joining("&", "?", ""));
    }

    public static Map<String, String> getValuesMap(QueryParameterModel... models) {
        Map<String, String> result = new HashMap<>();
        Arrays.stream(models)
                .filter(Objects::nonNull)
                .forEach(model -> result.putAll(model.getValuesMap()));
        return result;
    }

    public static String toDebugString(QueryParameterModel... models) {
        return getValuesMap(models).entrySet().stream()
                .map(entry -> entry.getKey() + " = " + entry.getValue())
                .collect(Collectors.joining(" & ", "{", "}"));
    }
}
