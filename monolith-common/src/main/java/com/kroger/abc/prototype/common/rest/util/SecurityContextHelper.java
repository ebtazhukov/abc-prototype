package com.kroger.abc.prototype.common.rest.util;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityContextHelper {

    public static boolean isAuthenticated() {
        SecurityContext context = SecurityContextHolder.getContext();
        return context.getAuthentication() != null;
    }

    public static String getUserName() {
        if (isAuthenticated()) {
            SecurityContext context = SecurityContextHolder.getContext();
            return (String) context.getAuthentication().getPrincipal();
        } else {
            return null;
        }
    }

    public static String getToken() {
        if (isAuthenticated()) {
            SecurityContext context = SecurityContextHolder.getContext();
            return (String) context.getAuthentication().getCredentials();
        } else {
            return null;
        }
    }
}
