package com.kroger.abc.prototype.common.rest.util.projection;

import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.HashMap;
import java.util.Map;

public class Inclusion {

    public final static String INCLUSION_SONG_DOMAIN = "songs";

    public final static String INCLUSION_PLAYLIST_DOMAIN = "playlists";

    public final static String INCLUSION_ARTIST_DOMAIN = "artists";

    public final static String INCLUSION_ACCOUNT_DOMAIN = "accounts";

    private Map<String, Projections> value;

    Inclusion(Map<String, Projections> value) {
        this.value = value != null ? value : new HashMap<>();
    }

    public Projections getByKey(String key) {
        return value.get(key);
    }

    public boolean containsKey(String key) {
        return value.containsKey(key);
    }

    public Map<String, Projections> getValue() {
        return value;
    }
}
