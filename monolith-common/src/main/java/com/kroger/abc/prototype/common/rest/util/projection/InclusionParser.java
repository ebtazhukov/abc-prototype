package com.kroger.abc.prototype.common.rest.util.projection;

import com.kroger.abc.prototype.common.rest.param.projection.CommonProjections;
import com.kroger.abc.prototype.common.rest.param.projection.Projections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public class InclusionParser {

    private static Pattern domainPattern = Pattern.compile("(\\w+?)\\[(.+?)\\],?");

    public static Inclusion parseFromString(String inclusion) {
        Map<String, Projections> parsed = new HashMap<>();

        if (StringUtils.isNotBlank(inclusion)) {
            Matcher matcher = domainPattern.matcher(inclusion);
            while (matcher.find()) {

                parsed.put(matcher.group(1), new CommonProjections(matcher.group(2)));
            }
        }
        return new Inclusion(parsed);
    }
}
