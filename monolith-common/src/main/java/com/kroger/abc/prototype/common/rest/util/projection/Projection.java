package com.kroger.abc.prototype.common.rest.util.projection;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Projection {

    public final static String PROJECTION_ALBUM_DETAILS_KEY = "details";
    public final static String PROJECTION_ALBUM_NOTES_KEY = "notes";

    public final static String PROJECTION_SONG_DETAILS_KEY = "details";
    public final static String PROJECTION_SONG_DIGITAL_FILE_KEY = "digitalFiles";
    public final static String PROJECTION_SONG_LYRICS_KEY = "lyrics";

    public final static String PROJECTION_PLAYLIST_KEY = "playlists";

    public final static String PROJECTION_ARTIST_KEY = "artists";

    private Map<String, Shape> value;

    Projection(Map<String, Shape> value) {
        this.value = value != null ? value : new HashMap<>();
    }

    public Shape getByKey(String key) {
        return value.get(key);
    }

    public boolean containsKey(String key) {
        return value.containsKey(key);
    }

    public Map<String, String> getValue() {
        return value.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().toString()));
    }

    public enum Shape {COMPACT, FULL}
}
