package com.kroger.abc.prototype.common.rest.util.projection;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public class ProjectionParser {

    private static Pattern entityPattern = Pattern.compile("(\\w+?)_(compact|full),?");

    public static Projection parseFromString(String projection) {
        Map<String, Projection.Shape> parsed = new HashMap<>();

        if (StringUtils.isNotBlank(projection)) {
            Matcher matcher = entityPattern.matcher(projection);
            while (matcher.find()) {
                parsed.put(matcher.group(1), Projection.Shape.valueOf(matcher.group(2).toUpperCase()));
            }
        }
        return new Projection(parsed);
    }
}
