#!/bin/bash

mkdir ~/.cassandra

echo "[authentication]" >> ~/.cassandra/cqlshrc
echo "username =" ${CASSANDRA_USERNAME} >> ~/.cassandra/cqlshrc
echo "password =" ${CASSANDRA_PASSWORD} >> ~/.cassandra/cqlshrc

chmod 440 ~/.cassandra/cqlshrc

bash -c "autoMigrate"
