package com.kroger.abc.prototype.config;

import com.kroger.abc.prototype.filter.JWTAuthorizationFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http
                .cors().disable()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .antMatchers(HttpMethod.PUT, "/app/songs*").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.POST, "/business/albums*").hasAuthority("ADMIN")
                .antMatchers("/app/playlists*").hasAuthority("USER")
                .antMatchers("/app/accounts*").hasAuthority("USER")
                .antMatchers("/core/playlists*").hasAuthority("USER")
                .antMatchers("/core/accounts*").hasAuthority("USER")
                .anyRequest().permitAll()
                .and()
                .addFilterBefore(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
                // this disables session creation on Spring Security
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }
}
