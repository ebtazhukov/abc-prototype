package com.kroger.abc.prototype.filter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

public class JWTAuthorizationFilter extends OncePerRequestFilter {

    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(HEADER_STRING);

        if (header == null || !StringUtils.startsWithIgnoreCase(header, TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            // parse the token.
            token = StringUtils.replaceIgnoreCase(token, TOKEN_PREFIX, "");
            String withoutSignature = token.substring(0, token.lastIndexOf('.') + 1);
            Jwt<Header, Claims> untrusted = Jwts.parser().parseClaimsJwt(withoutSignature);
            String username = untrusted.getBody().get("preferred_username", String.class);

            Map<String, List<String>> realmAccess = untrusted.getBody().get("realm_access", Map.class);
            List<String> roles = MapUtils.emptyIfNull(realmAccess).get("roles");
            List<SimpleGrantedAuthority> grantedAuthorities = CollectionUtils.emptyIfNull(roles).stream()
                    .map(SimpleGrantedAuthority::new).collect(Collectors.toList());

            if (username != null) {
                //Temporary solution (put raw token)
                return new UsernamePasswordAuthenticationToken(username, token, grantedAuthorities);
            }
            return null;
        }
        return null;
    }
}
