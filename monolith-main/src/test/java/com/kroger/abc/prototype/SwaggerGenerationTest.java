package com.kroger.abc.prototype;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.CharEncoding;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SwaggerGenerationTest {

    @Value("${SWAGGER_TEST_OUTPUT_DIR:#{null}}")
    private String swaggerOutputPath;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setup() {
        Assert.assertNotNull(swaggerOutputPath);
    }

    @Test
    public void createSwaggerSpec() throws IOException {
        File directory = FileUtils.getFile(swaggerOutputPath);
        FileUtils.deleteDirectory(directory);
        FileUtils.forceMkdir(directory);

        String swaggerJSONAsString = this.restTemplate.getForObject("/v2/api-docs", String.class);

        FileUtils.writeStringToFile(
                FileUtils.getFile(directory, "swagger.json"), swaggerJSONAsString, CharEncoding.UTF_8);
    }

}