const Koa = require('koa')
const cors = require('@koa/cors')
const router = require('koa-router')()
const logger = require('koa-logger')
const serve = require('koa-static')
const koaSwagger = require('koa2-swagger-ui')
const render = require('koa-ejs');
const path = require('path');

const app = new Koa()

render(app, {
    root: path.join(__dirname, 'static'),
    layout: false,
    viewExt: 'html',
    cache: false,
    debug: false,
});

const setUseLocalhostCookie = async (ctx, next) => {
  ctx.cookies.set('useLocalhost', process.env.USE_LOCALHOST || true, { httpOnly: false })

  await next()
}

app.use(
  cors({
    origin: '*'
  })
)

app.use(setUseLocalhostCookie)


router.get('/', async ctx => {
    await ctx.render('home', {appConfig:
        {
            uiUrl: process.env.uiUrl,
            authUrl: process.env.authUrl,
            apiUrl: process.env.apiUrl,
            sessionTTLMin: process.env.sessionTTLMin
        }
    });
})

router.get('/health', resp => (resp.body = 'OK'))

router.get(
  '/swagger',
  koaSwagger({
    title: 'Krandorify API',
    routePrefix: false,
    hideTopbar: true, // hide swagger top bar
    swaggerOptions: {
      url: '/swagger.json',
      tagsSorter: 'alpha'
    }
  })
)

const staticOpts = {}

app
  .use(logger())
  .use(serve('./static'))
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(10080)
