const fetchMock = require('fetch-mock');

fetchMock.config.overwriteRoutes = true;

require('../static/scripts/api-caller.js');

test('id01: there is no authorization request if \'expires_in\' cookie presents', done => {
    fetchMock.mock('/authorize', 401);
    fetchMock.get('/albums', 200);

    doWithCookie(() => call('/albums').then(completeTest((response) => expect(response.status).toBe(200), done)));
});

test('id02: get 401 response if request can\'t be authorized', done => {
    fetchMock.mock('/authorize', 401);
    fetchMock.get('/albums', 200);

    call('/albums').catch(completeTest((e) => expect(e.toString()).toEqual(expect.stringContaining('401')), done));
});

test('id03: try to authorize if service responses with 401', done => {
    fetchMock.mock('/authorize', 200);
    fetchMock.once('/albums', 401);
    fetchMock.once('/albums', 200, {overwriteRoutes: false});

    doWithCookie(() => call('/albums').then(completeTest((response) => expect(response.status).toBe(200), done)));
});

test('id04: try to authorize only one time if service responses with 401', done => {
    fetchMock.once('/authorize', 401);
    fetchMock.once('/authorize', 200, {overwriteRoutes: false});
    fetchMock.once('/albums', 401);
    fetchMock.once('/albums', 200, {overwriteRoutes: false});

    doWithCookie(() => call('/albums').catch(completeTest((e) => expect(e.toString()).toEqual(expect.stringContaining('401')), done)));
});

test('id05: if authorization is in progress requests are put on hold and executed in initial order when it\'s completed', done => {
    fetchMock.mock('/authorize', new Promise((res, rej) => setTimeout(() => res(200), 1000)));
    fetchMock.get('/albums', 200);
    fetchMock.get('/artists', 200);
    fetchMock.get('/songs', 200);
    fetchMock.get('/playlists', 200);

    const started = [];
    const completed = [];

    callWithOrder('/albums', started, completed);
    callWithOrder('/artists', started, completed);
    callWithOrder('/songs', started, completed);
    callWithOrder('/playlists', started, completed).finally(completeTest(() => expect(completed).toEqual(started), done));
});

test('id06: all following requests are rejected if current one couldn\'t be authorized', done => {
    fetchMock.mock('/authorize', new Promise((res, rej) => setTimeout(() => res(401), 1000)));
    fetchMock.get('/albums', 200);
    fetchMock.get('/artists', 200);
    fetchMock.get('/songs', 200);
    fetchMock.get('/playlists', 200);

    const started = [];
    const completed = [];

    callWithError('/albums', started, completed);
    callWithError('/artists', started, completed);
    callWithError('/songs', started, completed);
    callWithError('/playlists', started, completed).finally(completeTest(() => expect(completed).toEqual(started), done));
});

test('id07: if requested url is the same as authorization url there is no double authorization request', done => {
    fetchMock.once('/authorize', 200);
    fetchMock.once('/authorize', 401, {overwriteRoutes: false});

    let error = null;
    call('/authorize').then((response) => expect(response.status).toBe(200)).catch((e) => error = e).finally(() => {
        expect(error).toBeNull();

        done();
    });
});

test('id08: public APIs don\'t require authorization', done => {
    fetchMock.once('/authorize', 401);
    fetchMock.get('/artists12', 200);
    fetchMock.get('/songs', 200);
    fetchMock.get('/playlists', 200);

    window.apiCallHandler.setPublicApiList([/\/[a-z]+12/, '/songs']);

    let error = null;
    call('/artists12').then((response) => expect(response.status).toBe(200)).catch((e) => error = e).finally(() => {
        expect(error).toBeNull();

        if (error) {
            done();
        }
    });
    call('/songs').then((response) => expect(response.status).toBe(200)).catch((e) => error = e).finally(() => {
        expect(error).toBeNull();

        if (error) {
            done();
        }
    });
    call('/playlists').then((response) => expect(response).toBeNull()).catch((e) => error = e).finally(() => {
        expect(error.toString()).toEqual(expect.stringContaining('401'));

        if (error) {
            done();
        }
    });

    window.apiCallHandler.setPublicApiList([]);
});

function callWithOrder(input, started, completed) {
    started.push(input);

    return call(input).then(() => completed.push(input));
}

function callWithError(input, started, completed) {
    started.push(input);

    return call(input).catch((e) => completed.push(input));
}

function call(input) {
    return window.callApi(input)
        .then((response) => {
            console.log(`${input} call has completed.`);

            return response;
        });
}

function completeTest(callback, done) {
    return (response) => {
        callback(response);

        done();
    }
}

function doWithCookie(callback) {
    window.document.cookie = 'expires_in=1';

    callback();

    window.document.cookie = 'expires_in=; Max-Age=0';
}