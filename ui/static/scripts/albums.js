function displayAlbums(parentElement, searchTerm) {
  state.albums.search(searchTerm).then(albums => {
    parentElement.html(
      Object.values(albums)
        .map(
          album => `
                <li class="nav-item">
                    <a data-albumid='${album.id}' class="nav-link" href="#">
                        ${album.title}
                    </a>
                </li>`
        )
        .join('\n')
    )
  })
}

const displayAlbumSongs = album =>
  displaySongTable(getRelIDs(album, 'songs'), 'media-play')

function displayAlbumDetails(albumID, parentElement) {
  state.albums
    .get(albumID, FULL)
    .then(album => {
      const albumDetails = formatAlbumDetails(album, formatROAlbumAttrs)
      const actions = formatAlbumDetailActions(false)
      parentElement.html(albumDetails + '\n' + actions)

      return album
    })
    .then(displayAlbumSongs)
}

function formatROAlbumAttrs(album, attr) {
  const roRenderers = {
    title: album => renderROText(album, 'title', 'h1'),
    songs: album => renderSongTable()
  }

  return roRenderers[attr](album)
}

function formatAlbumDetails(album, fieldFormatter) {
  return `
        <form>
                <div class="row">
                    ${fieldFormatter(album, 'title')}
                </div>
                <div class="row" id="songList">
                    ${fieldFormatter(album, 'songs')}
                </div>
        </form>
    `
}

function formatAlbumDetailActions() {
  return `
            <div class="row mt-1">
                <div class="offset-sm-4 col-sm-4">
                    <input id="playcurralbum" type="button" value="Play"></input>
                </div>
            </div>`
}

function displayCreateAlbum(parentElement) {
        parentElement.html(
            `
            <BODY>
                <FORM NAME="myform" ACTION="" METHOD="GET">Enter Title: <BR>
                    <INPUT TYPE="text" NAME="inputbox" VALUE=""><P>
                    <INPUT TYPE="button" NAME="button" Value="Save">
                </FORM > 
            </BODY>
        
            `
        )
}
 