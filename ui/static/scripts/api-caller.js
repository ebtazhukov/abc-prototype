(function (w) {
    if (w.callApi) {
        return;
    }

    function ApiCall(input, init, resolve, reject) {
        this.input = input;
        this.init = init;
        this.resolve = resolve;
        this.reject = reject;
    }

    ApiCall.prototype.rejectUnauthorized = function () {
        console.debug(`Rejecting ${this.input} call with 401 status...`);

        this.reject(Error('401 Unauthorized'));
    };
    ApiCall.prototype.do = function (callback) {
        console.debug(`Calling ${this.input}...`);

        fetch(this.input, this.init).then(
            (response) => {
                if (response.status === 401) {
                    console.debug(`Unable to call ${this.input}: 401 Unauthorized`);

                    (callback || this.rejectUnauthorized.bind(this))(this);
                } else {
                    this.resolve(response);
                }
            },
            (error) => this.reject(error)
        );
    };

    function ApiCallHandler() {
        this.init();

        this.calls = [];
        this.authInProgress = false;
        this.publicApis = [];
        this.interceptors = [];
    }

    ApiCallHandler.prototype.init = function (expiresInCookie, authorizationUrl) {
        this.expiresInCookie = expiresInCookie || 'expires_in';
        this.authorizationUrl = authorizationUrl || '/authorize';
    };
    ApiCallHandler.prototype.setPublicApiList = function (publicApis) {
        this.publicApis = publicApis || [];
    };
    ApiCallHandler.prototype.addInterceptor = function (interceptor) {
        this.interceptors.push(interceptor);
    };
    ApiCallHandler.prototype.isAuthorizationRequired = function (c) {
        return this.authorizationUrl !== c.input && !this.publicApis.find((item) => {
            let match = false;

            if (item.constructor === RegExp) {
                match = item.test(c.input);
            } else {
                match = item === c.input
            }

            if (match) {
                console.debug(`${c.input} is public API. Doesn't require authorization`);
            }

            return match;
        });
    };
    ApiCallHandler.prototype.isAuthorized = function () {
        return this.expiresInCookie &&
            w.document.cookie.split(';').find((item) => item.indexOf(`${this.expiresInCookie}=`) >= 0);
    };
    ApiCallHandler.prototype.authorize = function () {
        this.authInProgress = true;

        return fetch(this.authorizationUrl, {credentials: 'include'}).finally(() => this.authInProgress = false);
    };
    ApiCall.prototype.unauthorized = function (response, c, callback) {
        if (response.status === 401) {
            console.debug('Unable to call API: 401');

            if (callback) {
                callback();
            } else {
                c.rejectUnauthorized();

                this.rejectCalls();
            }

            return true;
        }

        return false;
    };
    ApiCallHandler.prototype.preAuthorize = function (c) {
        if (this.authInProgress) {
            console.debug('Authorization is in progress. Will try to fetch when it\'s completed');

            this.wait4Auth(c);
        } else {
            console.debug('Trying to authorize...');

            this.authorize().then(
                (response) => {
                    if (response.status === 200) {
                        console.debug('Authorization request has successfully completed.');

                        this.retryCalls(c);
                    } else {
                        this.rejectCalls(c);
                    }
                },
                () => this.rejectCalls(c)
            );
        }
    };
    ApiCallHandler.prototype.retryCalls = function (c) {
        c.do();

        if (!this.calls.length) {
            return;
        }

        console.debug('Re-trying all calls waiting for authorization...');

        this.calls.forEach((c) => c.do());

        this.calls.length = 0;
    };
    ApiCallHandler.prototype.rejectCalls = function (c) {
        c.rejectUnauthorized();

        if (!this.calls.length) {
            return;
        }

        console.debug('Rejecting all calls waiting for authorization...');

        this.calls.forEach((c) => c.rejectUnauthorized());

        this.calls.length = 0;
    };
    ApiCallHandler.prototype.wait4Auth = function (c) {
        console.debug(`Putting call ${c.input} in queue...`);

        this.calls.push(c);

        console.debug(`Current queue is: [${this.calls.map((call) => call.input).join(', ')}]`);
    };
    ApiCallHandler.prototype.call = function (input, init) {
        this.interceptors.forEach((interceptor) => interceptor());

        const self = this;

        return new Promise((resolve, reject) => {
            const c = new ApiCall(input, init, resolve, reject);

            if (self.isAuthorizationRequired(c)) {
                if (self.authInProgress) {
                    console.debug(`Unable to call ${input}. Authorization is in progress.`);

                    self.wait4Auth(c);
                } else {
                    if (self.isAuthorized()) {
                        c.do(self.preAuthorize.bind(self));
                    } else {
                        console.debug(`There is no authorization to request ${input}`);

                        self.preAuthorize(c);
                    }
                }
            } else {
                c.do();
            }
        });
    };

    w.apiCallHandler = new ApiCallHandler();

    w.callApi = w.apiCallHandler.call.bind(w.apiCallHandler);
})(window);