function displayArtists(parentElement, searchTerm) {
  state.artists.search(searchTerm).then(artists => {
    parentElement.html(
      Object.values(artists)
        .map(
          artist => `
                <li class="nav-item">
                    <a data-artistid='${artist.id}' class="nav-link" href="#">
                        ${artist.name}
                    </a>
                </li>`
        )
        .join('\n')
    )
  })
}

const displayArtistSongs = artist =>
  displaySongTable(artist.songIds, 'media-play')

function displayArtistDetails(artistID, parentElement) {
  state.artists
    .get(artistID, FULL)
    //Rather than fetching all the songs later one at a time, go ahead and retrieve all of them into the cache
    .then(artist =>
      state.songs.retrieve(artist.songIds, MEDIUM).then(() => artist)
    )
    .then(artist => {
      const artistDetails = formatArtistDetails(artist, formatROArtistAttrs)
      const actions = formatArtistDetailActions(false)
      parentElement.html(artistDetails + '\n' + actions)

      return artist
    })
    .then(displayArtistSongs)
}

function formatROArtistAttrs(artist, attr) {
  const roRenderers = {
    name: artist => renderROText(artist, 'name', 'h1'),
    songs: artist => renderSongTable()
  }

  return roRenderers[attr](artist)
}

function formatArtistDetails(artist, fieldFormatter) {
  return `
        <form>
                <div class="row">
                    ${fieldFormatter(artist, 'name')}
                </div>
                <div class="row" id="songList">
                    ${fieldFormatter(artist, 'songs')}
                </div>
        </form>
    `
}

function formatArtistDetailActions() {
  return `
            <div class="row mt-1">
                <div class="offset-sm-4 col-sm-4">
                    <input id="playcurrartist" type="button" value="Play"></input>
                </div>
            </div>
    `
}
