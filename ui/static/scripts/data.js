//Projection levels
LEAST = 'LEAST'
COMPACT = 'COMPACT'
MEDIUM = 'MEDIUM'
FULL = 'FULL'
;(function initializeData(w) {
  w.state = {
    //setup functions
    registerDomain,

    //application state
    writeToState: (key, data) => {
      appState.store[key] = data
      return data
    },
    getFromState: key => appState.store[key],

    //utilities
    dumpCache: cacheName => console.log(appState.caches[cacheName])
  }

  const appState = {
    domainFns: {},
    caches: {},
    store: {}
  }

  function fetcher(url, options = {}) {
    const stdHeaders = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'user-roles': isSubscribed() ? 'subscribed' : ''
    }
    const authHeader = keycloak && keycloak.token ? { Authorization: `BEARER ${keycloak.token}` } : {}
    const headers = Object.assign({}, stdHeaders, authHeader, options.headers)
    const opts = Object.assign(options, {credentials: 'include'})
    return w.callApi(url, {
      ...opts,
      headers
    })
      .then(response => response.json())
      .then(body => cacheIncluded(body))
      .then(body => body.data || body)
  }

  w.sendGet = url => fetcher(url)
  w.sendDelete = url => fetcher(url, { method: 'DELETE' })
  w.sendPost = (url, body) => fetcher(url, { method: 'POST', body: JSON.stringify(body) })
  w.sendPut = (url, body) => fetcher(url, { method: 'PUT', body: JSON.stringify(body) })

  //Setup functions
  function getGetter(domainLabel) {
    return (id, projection) => {
      const { found, missingIds } = getFromCache(domainLabel, id, projection)

      return state[domainLabel].retrieve(missingIds, projection).then(missing => {
        Array.prototype.push.apply(found, ensureArray(missing))

        return Array.isArray(id) ? found : found[0]
      })
    }
  }

  function decorateDomainFns(domainLabel, domainFns) {
    const wrapDomainFn = (fnName, thenFn) => {
      return (...args) => {
        if (domainFns[fnName]) {
          return domainFns[fnName](...args).then(data => thenFn(data, ...args))
        } else {
          throw new TypeError('Domain', domainLabel, 'does not support create.')
        }
      }
    }

    return Object.assign({}, domainFns, {
      search: searchTerm => domainFns.search(searchTerm).then(addToCache(COMPACT, domainLabel)),
      retrieve: (id, projection) => domainFns.retrieve(id, projection).then(addToCache(projection, domainLabel)),
      create: wrapDomainFn('create', addToCache(FULL, domainLabel)),
      update: wrapDomainFn('update', addToCache(FULL, domainLabel)),
      delete: wrapDomainFn('delete', (data, id) => removeCacheEntry(id, domainLabel)),
      get: getGetter(domainLabel)
    })
  }

  function registerDomain(domainLabel, domainFns) {
    w.state[domainLabel] = Object.assign({}, decorateDomainFns(domainLabel, domainFns))
    appState.caches[domainLabel] = {}
  }

  //Cache utilities
  function cacheIncluded(body) {
    const projections = (body && body.meta && body.meta.projections) || {}

    for (const [key, value] of Object.entries(body.included || {})) {
      if (value.length > 0) {
        addToCache(state[key].fromServerProjections(projections), key)(value)
      }
    }

    return body
  }

  function setCacheEntry(data, cache, projection) {
    const oldCacheEntry = cache[data.id] || {}

    //In a real solution, this would be setup so that it didn't overright more complete data with less complete.
    //Unless the TTL had expired, of course.
    cache[data.id] = Object.assign(oldCacheEntry, {
      cacheData: {
        lastUpdated: Date.now(),
        projection
      },
      data
    })

    return cache
  }

  function addToCache(projection, cacheName) {
    return list => {
      const entries = ensureArray(list)

      const cache = appState.caches[cacheName]
      appState.caches[cacheName] = entries.reduce((currCache, data) => setCacheEntry(data, cache, projection), cache)

      return list
    }
  }

  const checkProjectionLimit = (minProjection, currProjection) => {
    const projectionLevels = {
      [LEAST]: 0,
      [COMPACT]: 1,
      [MEDIUM]: 2,
      [FULL]: 3
    }

    return projectionLevels[currProjection] >= projectionLevels[minProjection]
  }

  function getFromCache(cacheName, keys, minProjection = COMPACT) {
    cacheKeys = ensureArray(keys)

    return cacheKeys.reduce(
      (acc, key) => {
        const cacheEntry = appState.caches[cacheName][key]
        //Obviously a ttl check would be implemented here as well in a production application.
        if (checkProjectionLimit(minProjection, cacheEntry.cacheData.projection)) {
          acc.found.push(cacheEntry.data)
        } else {
          acc.missingIds.push(key)
        }
        return acc
      },
      {
        found: [],
        missingIds: []
      }
    )
  }

  function removeCacheEntry(id, cacheName) {
    delete appState.caches[cacheName][id]
  }
})(window)

state.writeToState('useLocalhost', document.cookie.match(/useLocalhost=(true|false)/)[1])

const constructURL = ((useLocalhost = true) => (domain) => {
    let origin;
    if (window.appConfig.apiUrl) {
        origin = window.appConfig.apiUrl;
    } else {
        origin = useLocalhost ? `http://localhost:${domain === 'albums' ? 9090 : 10072}` : `https://krandorify-app-${domain}.cfcdcinternaltest.kroger.com`;
    }

    return `${origin}/app/${domain}`;
})(isLocalhost())

const accountsURL = constructURL('accounts')

const selfURL = isLocalhost() ? 'http://localhost:10080' : 'https://krandorify-ui.cfcdcinternaltest.kroger.com';

const keycloakURL = isLocalhost() ? 'http://localhost:8080/auth' : 'https://krandorify-keycloak.cfcdcinternaltest.kroger.com/auth';

const albumURL = constructURL('albums')
state.registerDomain('albums', {
  search: searchTerm => {
    const filters = searchTerm ? `&filter.name.contains=${searchTerm}` : ''

    return sendGet(`${albumURL}?include=${state.albums.toServerProjections(COMPACT)}${filters}`).catch(error =>
      console.log('Error retrieving albums', error, 'searchTerm was', searchTerm)
    )
  },
  fromServerProjections: projections => {
    const projection = projections.albums || { details: COMPACT }

    if (projection.details === FULL) {
      if (projection.notes === FULL) {
        return FULL
      }

      return MEDIUM
    }
    return COMPACT
  },
  toServerProjections: (projection = COMPACT) =>
    encodeURIComponent(
      {
        [FULL]: 'albums[details_full,notes_full],songs[details_full]',
        [MEDIUM]: 'albums[details_full],songs[details_full]',
        [COMPACT]: 'albums[details_compact]'
      }[projection]
    ),
  retrieve: (ids, projection) => {
    const albumIDs = ensureArray(ids)
    return sendGet(`${albumURL}/${albumIDs}?include=${state.albums.toServerProjections(projection)}`).catch(error =>
      console.log('Error retrieving album details for ', albumIDs, error)
    )
  }
})

const playlistsURL = constructURL('playlists')
state.registerDomain('playlists', {
  search: searchTerm => {
    const filters = searchTerm ? `&filter.title.contains=${searchTerm}` : ''

    return sendGet(`${playlistsURL}?include=${state.playlists.toServerProjections(COMPACT)}${filters}`).catch(
      error => console.log('Error retrieving playLists', error, 'searchTerm was', searchTerm)
    )
  },
  fromServerProjections: projections => {
    return projections.playlists.playlists || COMPACT
  },
  toServerProjections: (projection = COMPACT) =>
    encodeURIComponent(
      {
        [FULL]: 'playlists[playlists_full],songs[detail_full]',
        [MEDIUM]: 'playlists[playlists_full]',
        [COMPACT]: 'playlists[playlists_compact]'
      }[projection]
    ),
  retrieve: (ids, projection) => {
    const playlistIDs = ensureArray(ids)

    return sendGet(
      `${playlistsURL}?include=${state.playlists.toServerProjections(projection)}&filter.id=${playlistIDs.join(',')}`
    ).catch(error => console.log('Error retrieving playlist details for ', playlistIDs, error))
  },
  create: playlist => {
    return sendPost(playlistsURL, playlist).catch(error => console.log('Error creating new playlist', playlist, error))
  },
  update: playlist => {
    return sendPut(playlistsURL, playlist).catch(error => console.log('Error updating playlist', playlist, error))
  },
  delete: playlistID => {
    return sendDelete(`${playlistsURL}/${playlistID}`).catch(error =>
      console.log('Error deleting playlist', playlistID, error)
    )
  }
})

const artistsURL = constructURL('artists')
state.registerDomain('artists', {
  search: searchTerm => {
    const filters = searchTerm ? `&filter.name.contains=${searchTerm}` : ''

    return sendGet(`${artistsURL}?include=${state.artists.toServerProjections(COMPACT)}${filters}`).catch(
      error => console.log('Error retrieving artists', error, 'searchTerm was "', searchTerm),
      '"'
    )
  },
  fromServerProjections: projections => {
    const artistProjection = projections.artists || { artists: COMPACT }

    if (artistProjection.artists === FULL) {
      return MEDIUM
    }

    return COMPACT
  },
  toServerProjections: (projection = COMPACT) =>
    encodeURIComponent(
      {
        [FULL]: 'artists[artists_full],songs[details_full]',
        [MEDIUM]: 'artists[artists_full],songs[details_compact]',
        [COMPACT]: 'artists[artists_full]',
        [LEAST]: 'artists[artists_compact]'
      }[projection]
    ),
  retrieve: (ids, projection) => {
    const artistIDs = ensureArray(ids)

    if (artistIDs.length === 0) {
      return returnResolved([])
    }

    return sendGet(
      `${artistsURL}?filter.id=${artistIDs.join(',')}&include=${state.artists.toServerProjections(projection)}`
    )
      .then(artists => {
        if (projection !== COMPACT && projection !== LEAST) {
          const retrievedArtists = ensureArray(artists)

          return Promise.all(
            retrievedArtists.map(artist => {
              return state.songs.retrieveByArtistID(artist.id, LEAST).then(artistsSongs => {
                artist.songIds = artistsSongs.map(song => song.id)
                return artist
              })
            })
          )
        }

        return artists
      })
      .catch(error => console.log('Error retrieving artist details for ', artistIDs, error))
  }
})

const songsURL = constructURL('songs')
state.registerDomain('songs', {
  search: searchTerm => {
    const filters = searchTerm ? `&filter.title.contains=${searchTerm}` : ''

    //TODO: search by artist name as well as song title
    // First, search for artists whose name contains the search term.
    // Second pass both the search term AND the list of artist IDs to the songs endpoint.
    return state.artists
      .search(searchTerm)
      .then(artistsThatMatch => {
        const artistsFilter =
          artistsThatMatch.length > 0 ? `&filter.artist.id=${artistsThatMatch.map(artist => artist.id).join(',')}` : ''
        return sendGet(`${songsURL}?include=${state.songs.toServerProjections(COMPACT)}${filters}${artistsFilter}`)
      })
      .catch(error => console.log('Error retrieving song list', error, 'searchTerm was "', searchTerm, '"'))
  },
  fromServerProjections: projections => {
    //details        |   full  | compact |
    //===================================
    //lyrics full    |   full  | compact |
    //lyrics compact |  medium | compact |
    //lyrics <none>  | compact | compact |
    const projection = projections.songs || { details: COMPACT }

    if (projection.details === COMPACT) {
      return COMPACT
    } else if (projection.lyrics === FULL) {
      return FULL
    } else if (projection.lyrics === COMPACT) {
      return MEDIUM
    }

    return COMPACT
  },
  toServerProjections: (projection = COMPACT) =>
    encodeURIComponent(
      {
        [FULL]: 'songs[details_full,lyrics_full,digitalFiles_full],artists[artists_full]',
        [MEDIUM]: 'songs[details_full,lyrics_compact],artists[artists_full]',
        [COMPACT]: 'songs[details_compact],artists[artists_full]',
        [LEAST]: 'songs[details_compact]'
      }[projection]
    ),
  retrieveByArtistID: (artistIds, projection) => {
    const artistsToFind = ensureArray(artistIds)

    return sendGet(
      `${songsURL}?include=${state.songs.toServerProjections(projection)}&filter.artist.id=${artistsToFind.join(',')}`
    ).catch(error => console.log('Error retrieving songs for artist(s)', error, 'artist IDs were', artistsToFind))
  },
  retrieve: (ids, projection) => {
    const songIDs = ensureArray(ids)

    return sendGet(`${songsURL}?include=${state.songs.toServerProjections(projection)}&filter.id=${songIDs.join(',')}`)
      .then(songs => {
        if (projection === MEDIUM) {
          //add an ellipsis to the end of the lyrics if the projection is medium
          songs = ensureArray(songs).map(song => {
            if (song.lyrics) song.lyrics.lyrics += '&hellip;'

            return song
          })
        }

        if (Array.isArray(ids)) {
          return songs
        }
        return songs[0]
      })
      .catch(error => console.log('Error retrieving song set', error, 'IDs were', songIDs))
  },
  update: song => {
    return sendPut(`${songsURL}`, song).catch(error =>
      console.log('Failed saving song with error', error, 'New data was', song)
    )
  }
})
