$(() => {
  initializeApiCaller()
  initializeAuth()
  initializeEventHandlers()
  initializeListView()
})

function initializeListView() {
  $('#displayopts option[value=songs]').prop('selected', true)
  $('#displayopts').trigger('change')
}

function initializeApiCaller() {
    if (window.apiCallHandler) {
        window.apiCallHandler.init(null, `${window.appConfig.authUrl}/authorize`);
    }
}

var keycloak
let authenticated;

function initializeAuth() {
  if (!window.appConfig.authUrl) {
      keycloak = Keycloak({
          url: keycloakURL,
          realm: 'abc-prototype',
          clientId: 'abc-app'
      })

      keycloak
          .init({ onLoad: 'check-sso' })
          .success(function(authenticated) {
              if (authenticated) {
                  onAuthenticated();
              }
          })
          .error(function() {
              alert('failed to initialize')
          })
  } else {
      fetch(`${window.appConfig.authUrl}/authenticate`, {credentials: 'include'}).then(
          (response) => {
              console.info(`Authentication response code is: ${response.status}`);

              if (response.status === 200) {
                  authenticated = true;

                  onAuthenticated();
              }
          },
          (error) => {
              console.error(`Unable to authenticate: ${error}`)
          }
      );
  }

  function onAuthenticated() {
      initAutoLogout();

      $('#playlistdisplayopt').removeAttr('disabled')
      sendGet(accountsURL)
          .then(account => state.writeToState('account', account))
          .then(accountStatusChanged)
          .catch(error => console.log('Error retrieving account', error))
  }

  function initAutoLogout() {
      if (window.appConfig.sessionTTLMin) {
          let logoutTimeout;

          function resetLogoutTimeout() {
              window.clearTimeout(logoutTimeout);

              logoutTimeout = window.setTimeout(logout, window.appConfig.sessionTTLMin * 60 * 1000);
          }

          window.apiCallHandler.addInterceptor(resetLogoutTimeout);

          resetLogoutTimeout();
      }
  }

  accountStatusChanged()

  $('#login_btn').click(() => {
    if (keycloak) {
      keycloak
        .login({redirectUri: selfURL})
        .success(() => {
            keycloak.loadUserProfile().success(profile => {
                console.log(profile)
            })
        })
        .error(() => {
            console.log('error login')
        })
    } else {
      window.location.href = `${window.appConfig.authUrl}?redirect_uri=${encodeURIComponent(window.appConfig.uiUrl)}`
    }
  })

  $('#logout_btn').click(logout);

    function logout() {
        if (keycloak) {
            keycloak.logout({redirectUri: selfURL}).success(() => {
                console.log('successfully logout')
            })
        } else {
            fetch(`${window.appConfig.authUrl}/logout`, {credentials: 'include'}).then(
                (response) => {
                    console.info(`Logout response code is: ${response.status}`);

                    if (response.status === 200) {
                        authenticated = false;

                        window.location.reload(true);
                    }
                },
                (error) => {
                    console.error(`Unable to logout: ${error}`)
                }
            );
        }
    }

    if (keycloak) {
        $('#deleteaccount').show().click(() => {
            if (confirm('Are you sure? This operation cannot be undone!')) {
                keycloak.logout({redirectUri: selfURL})
                sendDelete(accountsURL)
                    .then(accountStatusChanged)
                    .catch(error => console.log('Error deleting account', error))
            }
        })
    }
}

function accountStatusChanged() {
  $('#login_btn').prop('disabled', isAuthenticated())
  $('#logout_btn').prop('disabled', !isAuthenticated())
  $('#deleteaccount').prop('disabled', !isAuthenticated())
  $('#addnew').toggleClass('hidden', !isAdmin())
}

function songDataChanged() {
  $('#displayopts').trigger('change')
  displaySongDetails(state.getFromState('currID'), $('main'))
}

function playlistDataChanged() {
  $('#displayopts').trigger('change')
}

const doWithCurrSelectedID = fn => (...args) => fn(state.getFromState('currID'), ...args)
const doWithMain = fn => (...args) => fn($('main'), ...args)
const doWithMainAndID = fn => doWithMain(doWithCurrSelectedID(fn))

function initializeEventHandlers() {
  //Handlers for the sidebar (left side)
  $('#displayopts').change(() => changeListView($('#displayopts option:selected').val(), $('#searchterm').val()))
  $('#search').click(() => changeListView($('#displayopts option:selected').val(), $('#searchterm').val()))
  $('#listview').on('click', 'li a', displayItem)
  $('#addnew').click(() => createAlbum())

  //Handler's for buttons in the main area of the page
  $('main').on('click', '#editcurrsong', doWithMainAndID(editSongDetails))
  $('main').on('click', '#savecurrsong', doWithMainAndID(saveSongDetails))
  $('main').on('click', '#playcurrsong', doWithCurrSelectedID(playSong))
  $('main').on('click', '#stopcurrsong', stopPlaying)

  $('main').on('click', '#editcurrplaylist', doWithMainAndID(editPlaylistDetails))
  $('main').on('click', '#savecurrplaylist', doWithMainAndID(savePlaylistDetails))
  $('main').on('click', '#createplaylist', doWithMain(saveNewPlaylist))
  $('main').on('click', '#deletecurrplaylist', doWithMainAndID(deletePlaylist))
  $('main').on('click', '#createcurrplaylist', doWithMain(createPlaylist))
}

function displayItem(e) {
  switch (state.getFromState('displaying')) {
    case 'albums':
      state.writeToState('currID', $(e.target).data('albumid'))

      displayAlbumDetails($(e.target).data('albumid'), $('main'))
      break
    case 'artists':
      state.writeToState('currID', $(e.target).data('artistid'))

      displayArtistDetails($(e.target).data('artistid'), $('main'))
      break
    case 'playlists':
      state.writeToState('currID', $(e.target).data('playlistid'))

      displayPlaylistDetails($(e.target).data('playlistid'), $('main'))
      break
    case 'songs':
    default:
      state.writeToState('currID', $(e.target).data('songid'))

      displaySongDetails($(e.target).data('songid'), $('main'))
      break
  }
}

function createAlbum() {
  switch (state.getFromState('displaying')) {
    case 'albums':
      state.writeToState('currID', undefined)

      displayCreateAlbum($('main'))
      break

    default:

      break
  }
}

function changeListView(newView, searchTerm) {
  state.writeToState('displaying', newView)
  switch (newView) {
    case 'albums':
      displayAlbums($('#listview'), searchTerm)
      break
    case 'artists':
      displayArtists($('#listview'), searchTerm)
      break
    case 'playlists':
      displayPlaylists($('#listview'), searchTerm)
      break
    case 'songs':
    default:
      displaySongs($('#listview'), searchTerm)
      break
  }
}
