function displayPlaylists(parentElement, searchTerm) {
  state.playlists.search(searchTerm).then(playlists => {
    parentElement.html(
      Object.values(playlists)
        .map(
          playlist => `
                <li class="nav-item">
                    <a data-playlistid='${
                      playlist.id
                    }' class="nav-link" href="#">
                        ${playlist.title}
                    </a>
                </li>`
        )
        .join('\n')
    )
  })
}

const displayPlaylistSongTable = playlist =>
  displaySongTable(getRelIDs(playlist, 'songs'), 'media-play')

function displayPlaylistDetails(playlistID, parentElement) {
  state.playlists
    .get(playlistID, FULL)
    .then(playlist =>
      state.songs.get(getRelIDs(playlist, 'songs'), MEDIUM).then(() => playlist)
    )
    .then(playlist => {
      const playlistDetails = formatPlaylistDetails(
        playlist,
        formatROPlaylistAttrs
      )
      const actions = formatPlaylistDetailActions(false)
      parentElement.html(playlistDetails + '\n' + actions)

      return playlist
    })
    .then(displayPlaylistSongTable)
}

function editPlaylistDetails(playlistID, parentElement) {
  state.playlists
    .get(playlistID, FULL)
    .then(playlist =>
      state.songs.get(getRelIDs(playlist, 'songs'), MEDIUM).then(() => playlist)
    )
    .then(playlist => {
      const playlistDetails = formatPlaylistDetails(
        playlist,
        formatRWPlaylistAttrs
      )
      const actions = formatPlaylistDetailActions(true)
      parentElement.html(playlistDetails + '\n' + actions)

      return playlist
    })
    .then(displayPlaylistSongTable)
}

function createPlaylist(parentElement) {
  const playlist = {
    id: 'new',
    title: '',
    songIds: []
  }

  const playlistDetails = formatPlaylistDetails(playlist, formatRWPlaylistAttrs)
  const actions = formatPlaylistDetailActions(true, true)
  parentElement.html(playlistDetails + '\n' + actions)
  displayPlaylistSongTable(playlist)
}

function formatROPlaylistAttrs(playlist, attr) {
  const roRenderers = {
    title: playlist => renderROText(playlist, 'title', 'h1'),
    songs: playlist => renderSongTable(playlist)
  }

  return roRenderers[attr](playlist)
}

function formatRWPlaylistAttrs(playlist, attr) {
  const rwRenderers = {
    title: playlist => renderRWText(playlist, 'title', 'h1'),
    songs: playlist => renderSongTable(playlist)
  }

  return rwRenderers[attr](playlist)
}

function formatPlaylistDetails(playlist, fieldFormatter) {
  return `
        <form>
                <div class="row">
                    ${fieldFormatter(playlist, 'title')}
                </div>
                <div class="row" id="songList">
                    ${fieldFormatter(playlist, 'songs')}
                </div>
        </form>
    `
}

function formatPlaylistDetailActions(editing, creating) {
  const variableBtns = editing
    ? `
                    <input id="addsongcurrplaylist" type="button" value="Add Song..."></input>
                    <input id="${
                      creating ? 'createplaylist' : 'savecurrplaylist'
                    }" type="button" value="Save..."></input>`
    : `
                    <input id="editcurrplaylist" type="button" value="Edit..."></input>`

  return `
            <div class="row mt-1">
                <div class="col-sm-4">
                    <input id="deletecurrplaylist" type="button" value="Delete..."></input>
                </div>
                <div class="offset-sm-4 col-sm-4">
                    <input id="playcurrplaylist" type="button" value="Play"></input>
                    <input id="createcurrplaylist" type="button" value="New..."></input>
                    ${variableBtns}
                </div>
            </div>
    `
}

function saveNewPlaylist(parentElement) {
  const body = {
    title: $('#title').val()
  }
  state.playlists.create(body).then(playlist => {
    displayPlaylistDetails(playlist.id, parentElement)
    state.writeToState('currID', playlist.id)
    playlistDataChanged()
  })
}

function savePlaylistDetails(playlistID, parentElement) {
  state.playlists
    .get(playlistID, FULL)
    .then(playlist => {
      const body = Object.assign(playlist, {
        title: $('#title').val()
      })

      delete body.createdAt
      delete body.modifiedAt

      return body
    })
    .then(state.playlists.update)
    .then(playlist => {
      displayPlaylistDetails(playlistID, parentElement)
      playlistDataChanged()
    })
}

function deletePlaylist(playlistID, parentElement) {
  if (confirm('Are you sure?')) {
    state.playlists.delete(playlistID).then(() => {
      playlistDataChanged()
      parentElement.html('')
    })
  }
}
