const formatSongListItem = song => {
  state.artists.get(song.relationships.artist.id, COMPACT).then(artist => {
    $(`a[data-songid="${song.id}"]`).html(`${song.title} (${artist.name})`)
  })

  return `
        <a data-songid='${song.id}' class="nav-link" href="#">
            ${song.title} (...)
        </a>`
}

function displaySongs(parentElement, searchTerm) {
  state.songs.search(searchTerm).then(songs => {
    parentElement.html(
      Object.values(songs)
        .map(
          song => `
                    <li class="nav-item">
                        ${formatSongListItem(song)}
                    </li>`
        )
        .join('\n')
    )
  })
}

const renderSongRow = (song, icon) => `
    <tr>
        <td>
            ${song.title}
        </td>
        <td data-artistid="${song.artistId}">
            ${asyncFormatter(
              state.artists.get(song.relationships.artist.id, MEDIUM),
              artist => artist.name
            )}
        </td>
        <td>
            ${formatDuration(song.duration * 1000)}
        </td>
        <td>
            <button class="playlist-${icon} icon-btn" data-songID="${
  song.id
}" ${isSubscribed() ? '' : 'disabled'}>
                <svg class="icon">
                  <use xlink:href="open-iconic.svg#${icon}" class="${icon}"></use>
                </svg>
            </button>
        </td>
    </tr>
`

function displaySongTable(songSet, icon) {
  return Promise.all(
    songSet.map(songId => state.songs.get(songId, MEDIUM))
  ).then(songs =>
    $('#songs-table').html(
      songs.map(song => renderSongRow(song, icon)).join('\n')
    )
  )
}

function renderSongTable() {
  return `
        <table class="table">
            <thead>
                <th>Name</th>
                <th>Artist</th>
                <th>Duration</th>
            </thead>
            <tbody id="songs-table">
            </tbody>
        </table>
    `
}

function formatSongDetailActions(song, editing = false) {
  const playingSong = state.getFromState('playingSong')
  return `
            <div class="row mt-1">
                <div class="col-sm-4">
                    <input id="deletecurrsong" type="button" value="Delete..."></input>
                </div>
                <div class="offset-sm-4 col-sm-4">
                    <input
                      id="${
                        song.id === playingSong
                          ? 'stopcurrsong'
                          : 'playcurrsong'
                      }"
                      type="button"
                      value="${song.id === playingSong ? 'Stop' : 'Play'}"
                      data-songid=${song.id}
                      ${!song.subscribeOnly || isSubscribed() ? '' : 'disabled'}
                    ></input>
                    ${
                      isAdmin()
                        ? editing
                          ? '<input id="savecurrsong" type="button" value="Save"></input>'
                          : '<input id="editcurrsong" type="button" value="Edit..."></input>'
                        : ''
                    }
                </div>
            </div>
    `
}

function displaySongDetails(songID, parentElement) {
  state.songs.get(songID, MEDIUM).then(song => {
    const songDetails = formatSongDetails(song, formatROSongAttrs)
    const actions = formatSongDetailActions(song, false)
    parentElement.html(songDetails + '\n' + actions)
  })
}

function editSongDetails(songID, parentElement) {
  state.songs.get(songID, MEDIUM).then(song => {
    const songDetails = formatSongDetails(song, formatRWSongAttrs)
    const actions = formatSongDetailActions(song, true)
    parentElement.html(songDetails + '\n' + actions)
  })
}

function saveSongDetails(songID, parentElement) {
  state.songs
    .get(songID, FULL)
    .then(song =>
      Object.assign(song, {
        title: $('#title').val(),
        artist: $('#artist').val(),
        lyrics: {
          lyrics: $('#lyrics_lyrics').val()
        }
      })
    )
    .then(state.songs.update)
    .then(() => {
      displaySongDetails(songID, parentElement)
      songDataChanged()
    })
}

function formatROSongAttrs(song, attr) {
  const roRenderers = {
    title: song => renderROText(song, 'title', 'h1'),
    artist: song =>
      asyncFormatter(
        state.artists.get(song.relationships.artist.id, COMPACT),
        renderROTxtAttrWLbl,
        'name',
        'By'
      ),
    duration: song => renderROSecsAttrWLbl(song, 'duration', 'Duration'),
    bitrate: song => renderROTxtAttrWLbl(song, 'bitrate', 'Bit Rate'),
    'lyrics.lyrics': song => renderROLgTxt(song, 'lyrics.lyrics')
  }

  return roRenderers[attr](song)
}

function formatRWSongAttrs(song, attr) {
  const rwRenderers = {
    title: song => renderRWText(song, 'title', 'h1'),
    artist: song =>
      asyncFormatter(
        state.artists.get(song.relationships.artist.id, COMPACT),
        renderRWTxtAttrWLbl,
        'artist',
        'By'
      ),
    duration: song => renderROSecsAttrWLbl(song, 'duration', 'Duration'),
    bitrate: song => renderROTxtAttrWLbl(song, 'bitrate', 'Bit Rate'),
    'lyrics.lyrics': song => renderRWLgTxt(song, 'lyrics.lyrics')
  }

  return rwRenderers[attr](song)
}

function formatSongDetails(song, fieldFormatter) {
  return `
            <form>
                <div class="row">
                    ${fieldFormatter(song, 'title')}
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        ${fieldFormatter(song, 'artist')}
                    </div>
                    <div class="col-sm-4">
                        ${fieldFormatter(song, 'duration')}
                    </div>
                    <div class="col-sm-4">
                        ${fieldFormatter(song, 'bitrate')}
                    </div>
                </div>
                <div class="row mt-3">
                    <hr />
                    <div class="col-sm-12">
                        <h3>Lyrics</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        ${fieldFormatter(song, 'lyrics.lyrics')}
                    </div>
                </div>
            </form>
`
}

function playSong(songID) {
  state.writeToState('playingSong', songID)

  state.songs.get(songID, FULL).then(song => {
    state.writeToState('playStartTime', Date.now())
    $('#playingtitle').html(song.title)
    $('#timeremaining').html(formatDuration(song.duration * 1000))
    $('#progress').removeClass('w-0 w-25 w-50 w-75 w-100')
    songDataChanged()

    state.writeToState('intervalID', setInterval(updateSongProgress, 200, song))

    state.artists.get(song.relationships.artist.id).then(artist => {
      $('#playingartist').html(artist.name)
    })
  })
}

function updateSongProgress(song) {
  const timeElapsed = (Date.now() - state.getFromState('playStartTime')) / 1000

  if (timeElapsed >= song.duration) {
    stopPlaying()
  } else {
    $('#progress').css('width', `${(timeElapsed / song.duration) * 100}%`)
    $('#timeremaining').html(
      formatDuration((song.duration - timeElapsed) * 1000, ['millisecond'])
    )
  }
}

function stopPlaying() {
  clearInterval(state.getFromState('intervalID'))
  state.writeToState('playStartTime', undefined)
  state.writeToState('playingSong', undefined)
  $('#playingtitle').html('')
  $('#timeremaining').html('')
  $('#playingartist').html('')
  $('#progress').css('width', '0')
  songDataChanged()
}
