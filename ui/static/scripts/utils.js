function convertArrayToMap(arr, keyPath) {
  return arr.reduce((acc, item) => {
    const key = getByPath(item, keyPath)
    if (key) {
      acc[key] = item
    }
    return acc
  }, {})
}

function getByPath(obj, path) {
  return path.split('.').reduce((acc, attr) => acc && acc[attr], obj)
}

function formatDuration(ms, excludeUnits = []) {
  if (ms < 0) ms = -ms
  const time = {
    day: Math.floor(ms / 86400000),
    hour: Math.floor(ms / 3600000) % 24,
    minute: Math.floor(ms / 60000) % 60,
    second: Math.floor(ms / 1000) % 60,
    millisecond: Math.floor(ms) % 1000
  }

  return Object.entries(time)
    .filter(val => val[1] !== 0 && !excludeUnits.includes(val[0]))
    .map(val => val[1] + ' ' + (val[1] !== 1 ? val[0] + 's' : val[0]))
    .join(' ')
}

const getRelIDs = (obj, type) => obj.relationships[type].map(relationship => relationship.id)

const escapeHTMLID = id => id.replace(/\./g, '_')

const renderAttrLbl = (lbl, tag = 'h4') => `
                    <${tag} class="attrlbl">${lbl}</${tag}>
`

const renderROText = (obj, attr, tag = 'p') => `
                    <${tag}
                      id="${escapeHTMLID(attr)}"
                      class="attr"
                    >
                      ${getByPath(obj, attr) || ''}
                    </${tag}>
`

const renderRWText = (obj, attr, tag = 'p') => `
                    <input
                      class="form-control"
                      id="${escapeHTMLID(attr)}"
                      type="text"
                      value="${getByPath(obj, attr)}"
                    >
                    </input>
`

const renderROSeconds = (obj, attr, tag = 'p') => `
                    <${tag}
                      class=""
                      id="${escapeHTMLID(attr)}"
                    >
                      ${formatDuration(getByPath(obj, attr) * 1000)}
                    </${tag}>
`

const renderRWSeconds = renderRWText

const renderROLgTxt = renderROText

const renderRWLgTxt = (obj, attr) => `
                    <textarea
                      class="h-100 form-control form-control-lg"
                      id="${escapeHTMLID(attr)}"
                    >
                      ${getByPath(obj, attr)}
                    </textarea>
`

const renderROTxtAttrWLbl = (obj, attr, lbl) => {
  return renderAttrLbl(lbl) + '\n' + renderROText(obj, attr)
}

const renderRWTxtAttrWLbl = (obj, attr, lbl) => {
  return renderAttrLbl(lbl) + '\n' + renderRWText(obj, attr)
}

const renderROSecsAttrWLbl = (obj, attr, lbl) => {
  return renderAttrLbl(lbl) + '\n' + renderROSeconds(obj, attr)
}

const renderRWSecsAttrWLbl = (obj, attr, lbl) => {
  return renderAttrLbl(lbl) + '\n' + renderROSeconds(obj, attr)
}

function uuidv4() {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (c ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))).toString(16)
  )
}

function ensureArray(value) {
  return Array.isArray(value) ? value : value ? [value] : []
}

function returnResolved(data) {
  return new Promise(resolve => resolve(data))
}

function asyncFormatter(promise, formatter, ...args) {
  const uniqueID = uuidv4()

  promise.then(obj => {
    $(`#async-${uniqueID}`).replaceWith(formatter(obj, ...args))
  })
  return `<span class="loader" id="async-${uniqueID}">...</span>`
}

function isAuthenticated() {
  return keycloak ? (keycloak.authenticated && !keycloak.isTokenExpired()) : authenticated;
}

function isAdmin() {
  return keycloak && isAuthenticated && keycloak.hasRealmRole('ADMIN')
}

function isSubscribed() {
  const account = state.getFromState('account')
  return (isAuthenticated && account && account.isSubscribed) || false
}

function isLocalhost() {
  return state.getFromState('useLocalhost') === 'true'
}
